<?php
	Class Paper extends CI_Controller {
		function __construct(){
			parent::__construct();
			$this->load->model("model_paper");
		}
		function dashboard(){
			if($this->session->userdata('status') == "masuk"){
				
			}else{
				redirect("login/login");
			}
			$data['title'] 	= "Paperless Exam";
			$data['menu'] 	= "main";
			$data['content'] = "dashboard";
			$this->load->view("dashboard",$data);
		}
		function paket(){
			$data['title'] 	= "Paperless Exam";
			$data['menu'] 	= "main";
			$data['content'] = "paket";
			$this->load->view("dashboard",$data);
		}
		function soal(){
			if($this->session->userdata('status') == "masuk"){
				
			}else{
				redirect("login/login");
			}
			$nis = $this->session->userdata("nis");
			$id_mapel = $this->session->userdata("id_mapel");
			$id_jenis = $this->session->userdata("id_jenis");
			$qwnil = $this->model_paper->qw("nilai","WHERE nis='$nis' AND id_mapel = '$id_mapel' AND id_jenis = '$id_jenis'")->num_rows();
			if($qwnil == 0){
				$data['title'] 	= "Paperless Exam";
				$data['menu'] 	= "main";
				$data['content'] = "kerja_soal";
				$nis = $this->session->userdata("nis");
				$this->db->where("nis", $nis);
				$this->db->update("siswa",array("status"=>"on"));
				$this->load->view("dashboard",$data);
			}else{
				redirect('paper/nilai');
			}
			
		}
		function soal_try(){
			if($this->session->userdata('status') == "masuk"){
				
			}else{
				redirect("login/login");
			}
			$nis = $this->session->userdata("nis");
			$id_mapel = $this->session->userdata("id_mapel");
			$paket = $this->session->userdata("paket");
			$latihan = $this->session->userdata("latihan");
			$qwnil = $this->model_paper->qw("nilai_tryout","WHERE nis='$nis' AND id_mapel = '$id_mapel' AND paket = '$paket' AND status = '$latihan'")->num_rows();
			if($qwnil == 0){
				$data['title'] 	= "Paperless Exam";
				$data['menu'] 	= "main";
				$data['content'] = "kerja_soal_try";
				$nis = $this->session->userdata("nis");
				$this->db->where("nis", $nis);
				$this->db->update("siswa",array("status"=>"on"));
				$this->load->view("dashboard",$data);
			}else{
				redirect('paper/nilai');
			}
			
		}
		function jawab_sis(){
			$id_jawaban = $this->input->post('id_j');
			$jawaban = $this->input->post('jwb');
			$id_mapel = $this->session->userdata('id_mapel');
			$qam = $this->model_paper->qw("jawaban","WHERE id_jawaban = '$id_jawaban'")->row_array();
			if($qam['kunci'] == $jawaban){
				$em = $this->model_paper->qw("mapel","WHERE id_mapel ='$id_mapel'")->row_array();
				$poin = 100 / $em['jumlah_soal'];
			}else{
				$poin=0;
			}
			$this->db->where("id_jawaban",$id_jawaban);
			$this->db->update('jawaban',array("jawaban"=>$jawaban, "poin"=>$poin));
			$id_mapel = $this->session->userdata("id_mapel");
			$mapel = $this->model_paper->qw("mapel","WHERE id_mapel = '$id_mapel'")->row_array();
			$no = $this->input->post("no");
			if($no == $mapel['jumlah_soal']){
				echo $no;
			}else{
				echo $no+1;
			}
		}
		function jawab_rag(){
			$id_jawaban = $this->input->post('id_j');
			$qam = $this->model_paper->qw("jawaban","WHERE id_jawaban = '$id_jawaban'")->row_array();
			if($qam['sts_jwb'] == "ragu"){
				$this->db->where("id_jawaban",$id_jawaban);
				$this->db->update("jawaban",array("sts_jwb"=>""));
			}else{
				$this->db->where("id_jawaban",$id_jawaban);
				$this->db->update("jawaban",array("sts_jwb"=>"ragu"));
			}
			echo "berhasil";
		}
		function jawab_sis_try(){
			$id_jawaban = $this->input->post('id_j');
			$jawaban = $this->input->post('jwb');
			$id_mapel = $this->session->userdata('id_mapel');
			$qam = $this->model_paper->qw("jawaban_tryout","WHERE id_jawaban = '$id_jawaban'")->row_array();
			if($qam['kunci'] == $jawaban){
				$em = $this->model_paper->qw("mapel_try","WHERE id_try ='$id_mapel'")->row_array();
				$poin = 100 / $em['jumlah_soal'];
			}else{
				$poin=0;
			}
			$this->db->where("id_jawaban",$id_jawaban);
			$this->db->update('jawaban_tryout',array("jawaban"=>$jawaban, "poin"=>$poin));
			$id_mapel = $this->session->userdata("id_mapel");
			$mapel = $this->model_paper->qw("mapel_try","WHERE id_try = '$id_mapel'")->row_array();
			$no = $this->input->post("no");
			if($no == $mapel['jumlah_soal']){
				echo $no;
			}else{
				echo $no+1;
			}
		}
		function wkt(){
			$jam = $this->input->post("jm");
			$menit = $this->input->post("mn");
			$detik = $this->input->post("wk");
			$nis = $this->session->userdata('nis');
			$id_mapel = $this->session->userdata('id_mapel');
			$id_jenis = $this->session->userdata('id_jenis');
			$wkt = $this->model_paper->qw("waktu","WHERE nis = '$nis' AND id_mapel = '$id_mapel' AND id_jenis = '$id_jenis'");
			$wktc = $wkt->num_rows();
			$dtw = $wkt->row_array();
			$aris = array(
				"jam" => $jam,
				"menit"=> $menit,
				"detik"=>$detik,
				"nis"=>$nis,
				"id_mapel"=>$id_mapel,
				"id_jenis"=>$id_jenis
			);
			if($wktc == 0){
				$this->db->insert("waktu",$aris);
			}else{
				$this->db->where("id_waktu", $dtw['id_waktu']);
				$this->db->update("waktu",$aris);
			}
			$data_session = array(
					"jam" => $jam,
					"menit" => $menit,
					"detik"=> $detik
			);
			$this->session->set_userdata($data_session);
			echo json_encode($data_session);
		}
		function wkt_try(){
			$jam = $this->input->post("jm");
			$menit = $this->input->post("mn");
			$detik = $this->input->post("wk");
			$nis = $this->session->userdata('nis');
			$id_mapel = $this->session->userdata('id_mapel');
			$paket = $this->session->userdata('paket');
			$latihan = $this->session->userdata('latihan');
			$wkt = $this->model_paper->qw("waktu_tryout","WHERE nis = '$nis' AND id_mapel = '$id_mapel' AND paket = '$paket' AND status = '$latihan'");
			$wktc = $wkt->num_rows();
			$dtw = $wkt->row_array();
			$aris = array(
				"jam" => $jam,
				"menit"=> $menit,
				"detik"=>$detik,
				"nis"=>$nis,
				"id_mapel"=>$id_mapel,
				"paket"=>$paket,
				'status' => $latihan
			);
			if($wktc == 0){
				$this->db->insert("waktu_tryout",$aris);
			}else{
				$this->db->where("id_waktu", $dtw['id_waktu']);
				$this->db->update("waktu_tryout",$aris);
			}
			$data_session = array(
					"jam" => $jam,
					"menit" => $menit,
					"detik"=> $detik
			);
			$this->session->set_userdata($data_session);
			echo json_encode($data_session);
		}
		function listen(){
			$this->db->where("id_jawaban",$this->input->post('id_j'));
			$this->db->update("jawaban",array("sts_listening"=>"No"));
		}
		function listen_try(){
			$this->db->where("id_jawaban",$this->input->post('id_j'));
			$this->db->update("jawaban_tryout",array("sts_listening"=>"No"));
		}
		function selesai(){
			$nis = $this->session->userdata("nis");
			$id_mapel = $this->session->userdata("id_mapel");
			$id_jenis = $this->session->userdata("id_jenis");
			$jjj = $this->model_paper->qw("jawaban","WHERE nis ='$nis' AND id_mapel = '$id_mapel' AND id_jenis = '$id_jenis' AND sts_jwb = 'ragu'")->num_rows();
			if($jjj == 0){
				$sis = $this->model_paper->qw("siswa","WHERE nis = '$nis'")->row_array();
				$mapel = $this->model_paper->qw("mapel","WHERE id_mapel = '$id_mapel'")->row_array();
				$nilai = 0;
				for ($i=1; $i <= $mapel['jumlah_soal'] ; $i++) { 
					$jwbn = $this->model_paper->qw("jawaban","WHERE nis = '$nis' AND id_mapel = '$id_mapel' AND id_jenis = '$id_jenis' AND nomor = '$i'")->row_array();
					$nilai = $nilai+$jwbn['poin'];
				}
				if($sis['kelas'] == "X"){
				 	$mapelkkm= $mapel['kkm1'];
				}elseif($sis['kelas'] == "XI"){
				 	$mapelkkm= $mapel['kkm2'];
				}else{
				 	$mapelkkm= $mapel['kkm3'];
				}
				if($nilai >= $mapelkkm){
					$stat = "Kompeten";
				}else{
					$stat = "Belum Kompeten";
				}
				$this->db->where(array("nis"=>$nis, "id_jenis"=>$id_jenis, "id_mapel" => $id_mapel));
				$this->db->delete("absen");
				$this->db->where(array("nis"=>$nis, "id_jenis"=>$id_jenis, "id_mapel" => $id_mapel));
				$this->db->delete("rekomendasi");
				
				$arnil = array(
					"nilai" => $nilai,
					"tgl_kerja" => date("Y-m-d"),
					"nis" => $nis,
					"id_mapel" => $id_mapel,
					"id_jenis" => $id_jenis,
					"kelas" => $sis['kelas'],
					"id_rombel" => $sis['id_rombel'],
					"kkm" => $mapelkkm,
					"status" => $stat
				);
				$this->db->insert("nilai",$arnil);
				$nis = $this->session->userdata("nis");
				$this->db->where("nis", $nis);
				$this->db->update("siswa",array("status"=>"off"));
				echo "Berhasil";
			}else{
				echo "blm";
			}
		}
		function selesai_try(){
			$nis = $this->session->userdata("nis");
			$id_mapel = $this->session->userdata("id_mapel");
			$paket = $this->session->userdata("paket");
			$status = $this->session->userdata("latihan");
			$sis = $this->model_paper->qw("siswa","WHERE nis = '$nis'")->row_array();
			$mapel = $this->model_paper->qw("mapel_try","WHERE id_try = '$id_mapel'")->row_array();
			$nilai = 0;
			for ($i=1; $i <= $mapel['jumlah_soal'] ; $i++) { 
				$jwbn = $this->model_paper->qw("jawaban_tryout","WHERE nis = '$nis' AND id_mapel = '$id_mapel' AND paket = '$paket' AND status='$status' AND nomor = '$i'")->row_array();
				$nilai = $nilai+$jwbn['poin'];
			}
			if($nilai >= $mapel['kkm']){
				$stat = "Kompeten";
			}else{
				$stat = "Belum Kompeten";
			}
			$arnil = array(
				"nilai" => $nilai,
				"tgl_kerja" => date("Y-m-d"),
				"nis" => $nis,
				"id_mapel" => $id_mapel,
				"paket" => $paket,
				"id_rombel" => $sis['id_rombel'],
				"kkm" => $mapel['kkm'],
				"status" => $stat,
				"status_tes"=> $status
			);
			$this->db->insert("nilai_tryout",$arnil);
			$this->db->where("nis", $nis);
			$this->db->update("siswa",array("status"=>"off"));
			echo "Berhasil";
		}
		function nilai(){
			$nis = $this->session->userdata("nis");
			$id_mapel=$this->session->userdata("id_mapel");
			$id_jenis=$this->session->userdata("id_jenis");
			$mapel = $this->model_paper->qw("mapel","WHERE id_mapel = '$id_mapel'")->row_array();
			$data['salah'] = 0;
			$data['benar'] = 0;
			for ($i=1; $i <= $mapel['jumlah_soal'] ; $i++) { 
				$jwbn = $this->model_paper->qw("jawaban","WHERE nis = '$nis' AND id_mapel = '$id_mapel' AND id_jenis = '$id_jenis' AND nomor = '$i'")->row_array();
				if($jwbn['poin'] == 0){
					$data['salah'] = $data['salah']+1; 
				}else{
					$data['benar'] = $data['benar']+1;
				}
			}
			$nil = $this->model_paper->qw("nilai","WHERE nis = '$nis' AND id_mapel = '$id_mapel' AND id_jenis = '$id_jenis'")->row_array();
			$sisw = $this->model_paper->qw("siswa","WHERE nis = '$nis'")->row_array();
			$data['kelas'] = $sisw['kelas'];
			$data['kkm1'] = $mapel['kkm1'];
			$data['kkm2'] = $mapel['kkm2'];
			$data['kkm3'] = $mapel['kkm3'];
			$data['nilai'] = $nil['nilai'];
			$data['jumlah_soal'] = $mapel['jumlah_soal'];
			$data['status'] = $nil['status'];
			$data['title'] 	= "Paperless Exam";
			$this->load->view("nilai",$data);
		}
		function nilai_try(){
			$nis = $this->session->userdata("nis");
			$id_mapel=$this->session->userdata("id_mapel");
			$paket=$this->session->userdata("paket");
			$status = $this->session->userdata("latihan");
			$mapel = $this->model_paper->qw("mapel_try","WHERE id_try = '$id_mapel'")->row_array();
			$data['salah'] = 0;
			$data['benar'] = 0;
			for ($i=1; $i <= $mapel['jumlah_soal'] ; $i++) { 
				$jwbn = $this->model_paper->qw("jawaban_tryout","WHERE nis = '$nis' AND id_mapel = '$id_mapel' AND paket = '$paket' AND status = '$status' AND nomor = '$i'")->row_array();
				if($jwbn['poin'] == 0){
					$data['salah'] = $data['salah']+1; 
				}else{
					$data['benar'] = $data['benar']+1;
				}
			}
			$nil = $this->model_paper->qw("nilai_tryout","WHERE nis = '$nis' AND id_mapel = '$id_mapel' AND paket = '$paket' AND status_tes = '$status'")->row_array();
			$data['kkm'] = $mapel['kkm'];
			$data['nilai'] = $nil['nilai'];
			$data['jumlah_soal'] = $mapel['jumlah_soal'];
			$data['status'] = $nil['status'];
			$data['title'] 	= "Paperless Exam";
			$this->load->view("nilai_try",$data);
		}
		function kerjasoal(){
			$nis = $this->session->userdata('nis');
			$id_mapel = $this->input->post('mapel');
			$id_jenis = $this->input->post('jenis');
			$kelas = $this->input->post('kelas');
			$qwtes = $this->model_paper->qw("jenis_tes","WHERE id_tes = '$id_jenis'")->row_array();
			$dt = date("Y-m-d");
			$wktc = $this->model_paper->qw("waktu","WHERE nis = '$nis' AND id_mapel = '$id_mapel' AND id_jenis = '$id_jenis'");
			$wkt = $wktc->row_array();
			$jmlw = $wktc->num_rows();
			if($jmlw == 0){
				$jns = $this->model_paper->qw("jenis_tes","WHERE id_tes = '$id_jenis'")->row_array();
				$data_session = array(
					"jam" => $jns['jam'],
					"menit" => $jns['menit'],
					"detik"=> 1,
					"id_mapel" => $id_mapel,
					"id_jenis" => $id_jenis
				);
				$this->session->set_userdata($data_session);
			}else{
				$data_session = array(
					"jam" => $wkt['jam'],
					"menit" => $wkt['menit'],
					"detik"=> $wkt['detik'],
					"id_mapel" => $id_mapel,
					"id_jenis" => $id_jenis
				);
				$this->session->set_userdata($data_session);
			}

			// if //

			if($qwtes['tanggal_awal'] > $dt || $qwtes['tanggal_akhir'] < $dt){
				echo "Tanggal Tes Telah Berakhir";
			}else{
				$qwrek = $this->model_paper->qw("pengaturan","WHERE id_peng = '1'")->row_array();
				if($qwrek['status'] == "on"){
					$sts_rekom = $this->model_paper->qw("rekomendasi","WHERE nis = '$nis' AND id_mapel = '$id_mapel' AND id_jenis = '$id_jenis'")->num_rows();
					if($sts_rekom == 0){
						echo "Anda Belum dapat rekomendasi";
					}else{
						$sts_abs = $this->model_paper->qw("absen","WHERE nis = '$nis' AND id_mapel = '$id_mapel' AND id_jenis = '$id_jenis'")->num_rows();
						if($sts_abs == 0){
							echo "Anda Belum dapat Absen";
						}else{
							$qwm=$this->model_paper->qw("mapel","WHERE id_mapel = '$id_mapel'")->row_array();
							$qwsol=$this->model_paper->qw("soal","WHERE id_mapel = '$id_mapel' AND jenis_tes = '$id_jenis' AND kelas = '$kelas'")->num_rows();
							if($qwsol < $qwm['jumlah_soal'] ){
								echo "Jumlah Soal Kurang";
							}else{
								$qwjmlo = $this->model_paper->qw("mapel","WHERE id_mapel = '$id_mapel'")->row_array();
								$jmlj = $this->model_paper->qw("jawaban","WHERE nis = '$nis' AND id_mapel='$id_mapel' AND id_jenis ='$id_jenis'")->num_rows();
								if($jmlj >= $qwjmlo['jumlah_soal']){
									echo "Berhasil";
								}else{
									$sol = $this->model_paper->qw("soal","WHERE kelas='$kelas' AND id_mapel = '$id_mapel' AND jenis_tes = '$id_jenis' ORDER BY id_soal ASC")->result();
									$x=0;
									$nomor = 1;
									foreach ($sol as $datasol) { 
										if(empty($datasol->listening)){
											$arid[] = $datasol->id_soal;
											$x++;
										}else{
											$id_soal = $datasol->id_soal;
											$qwjwb = $this->model_paper->qw("jawaban","WHERE nis = '$nis' AND id_mapel='$id_mapel' AND id_soal = '$id_soal' AND id_jenis ='$id_jenis'")->num_rows();
											if($qwjwb == 0){
												$ar = array(
													"id_jawaban" => '',
													"nis" => $nis,
													"id_mapel"=>$id_mapel,
													"id_soal"=>$datasol->id_soal,
													"kunci"=>$datasol->kunci,
													"id_jenis"=>$id_jenis,
													"sts_listening"=> "Yes",
													"nomor"=> $nomor++
												);
												$this->db->insert("jawaban",$ar);
											}else{

											}
										}
									}
									shuffle($arid);
									$x = $x-1;
									for($b=0;$b<=$x;$b++){
										$id_soal = $arid[$b];
										$dsl = $this->model_paper->qw("soal","WHERE id_soal = '$id_soal' ORDER BY id_soal ASC")->row_array();
										$qwjwb = $this->model_paper->qw("jawaban","WHERE nis = '$nis' AND id_mapel='$id_mapel' AND id_soal = '$id_soal' AND id_jenis ='$id_jenis'")->num_rows();
										if($qwjwb == 0){
											$ar = array(
												"id_jawaban" => '',
												"nis" => $nis,
												"id_mapel"=>$id_mapel,
												"id_soal"=>$id_soal,
												"kunci"=>$dsl['kunci'],
												"id_jenis"=>$id_jenis,
												"sts_listening"=> "No",
												"nomor"=>$nomor++
											);
											$this->db->insert("jawaban",$ar);
										}else{
											$b = $b-1;
										}
										if($nomor > $qwm['jumlah_soal']){
											break;
										}else{

										}
									}
									$qwjmloc = $this->model_paper->qw("mapel","WHERE id_mapel = '$id_mapel'")->row_array();
									$jmljc = $this->model_paper->qw("jawaban","WHERE nis = '$nis' AND id_mapel='$id_mapel' AND id_jenis ='$id_jenis'")->num_rows();
									if($jmljc >= $qwjmloc['jumlah_soal']){
										echo "Berhasil";
									}else{
										echo "Gagal";	
									}
								}
							}
						}
					}
				}else{
					$sts_abs = $this->model_paper->qw("absen","WHERE nis = '$nis' AND id_mapel = '$id_mapel' AND id_jenis = '$id_jenis'")->num_rows();
					if($sts_abs == 0){
						echo "Anda Belum dapat Absen";
					}else{
						$qwm=$this->model_paper->qw("mapel","WHERE id_mapel = '$id_mapel'")->row_array();
						$qwsol=$this->model_paper->qw("soal","WHERE id_mapel = '$id_mapel' AND jenis_tes = '$id_jenis' AND kelas = '$kelas'")->num_rows();
						if($qwsol < $qwm['jumlah_soal'] ){
							echo "Jumlah Soal Kurang";
						}else{
							$qwjmlo = $this->model_paper->qw("mapel","WHERE id_mapel = '$id_mapel'")->row_array();
							$jmlj = $this->model_paper->qw("jawaban","WHERE nis = '$nis' AND id_mapel='$id_mapel' AND id_jenis ='$id_jenis'")->num_rows();
							if($jmlj >= $qwjmlo['jumlah_soal']){
								echo "Berhasil";
							}else{
								$sol = $this->model_paper->qw("soal","WHERE kelas='$kelas' AND id_mapel = '$id_mapel' AND jenis_tes = '$id_jenis' ORDER BY id_soal ASC")->result();
								$x=0;
								$nomor = 1;
								foreach ($sol as $datasol) { 
									if(empty($datasol->listening)){
										$arid[] = $datasol->id_soal;
										$x++;
									}else{
										$id_soal = $datasol->id_soal;
										$qwjwb = $this->model_paper->qw("jawaban","WHERE nis = '$nis' AND id_mapel='$id_mapel' AND id_soal = '$id_soal' AND id_jenis ='$id_jenis'")->num_rows();
										if($qwjwb == 0){
											$ar = array(
												"id_jawaban" => '',
												"nis" => $nis,
												"id_mapel"=>$id_mapel,
												"id_soal"=>$datasol->id_soal,
												"kunci"=>$datasol->kunci,
												"id_jenis"=>$id_jenis,
												"sts_listening"=> "Yes",
												"nomor"=> $nomor++
											);
											$this->db->insert("jawaban",$ar);
										}else{

										}
									}
								}
								shuffle($arid);
								$x = $x-1;
								for($b=0;$b<=$x;$b++){
									$id_soal = $arid[$b];
									$dsl = $this->model_paper->qw("soal","WHERE id_soal = '$id_soal' ORDER BY id_soal ASC")->row_array();
									$qwjwb = $this->model_paper->qw("jawaban","WHERE nis = '$nis' AND id_mapel='$id_mapel' AND id_soal = '$id_soal' AND id_jenis ='$id_jenis'")->num_rows();
									if($qwjwb == 0){
										$ar = array(
											"id_jawaban" => '',
											"nis" => $nis,
											"id_mapel"=>$id_mapel,
											"id_soal"=>$id_soal,
											"kunci"=>$dsl['kunci'],
											"id_jenis"=>$id_jenis,
											"sts_listening"=> "No",
											"nomor"=>$nomor++
										);
										$this->db->insert("jawaban",$ar);
									}else{
										$b = $b-1;
									}
									if($nomor > $qwm['jumlah_soal']){
										break;
									}else{

									}
								}
								$qwjmloc = $this->model_paper->qw("mapel","WHERE id_mapel = '$id_mapel'")->row_array();
								$jmljc = $this->model_paper->qw("jawaban","WHERE nis = '$nis' AND id_mapel='$id_mapel' AND id_jenis ='$id_jenis'")->num_rows();
								if($jmljc >= $qwjmloc['jumlah_soal']){
									echo "Berhasil";
								}else{
									echo "Gagal";	
								}
							}
						}
					}
				}
		
			// } if //
		}
	}
	function kerjasoal_try(){
			$nis = $this->session->userdata('nis');
			$id_mapel = $this->input->post('mapel');
			$paket = $this->input->post('paket');
			$peng = $this->model_paper->qw("pengaturan","WHERE id_peng = '2'")->row_array();
			
			if($peng['status'] == "off"){
				$latihan = '';
				echo "Tryout Belum di Aktifkan";
			}else{
				if($paket == "Latihan"){
					$soal_lat = $this->model_paper->qw("jawaban_tryout","WHERE nis = '$nis' AND id_mapel = '$id_mapel' AND paket = 'Latihan' ORDER BY id_jawaban DESC")->row_array();
					if(empty($soal_lat['status'])){
						$latihanv = 1;
					}else{
						$latihanv = $soal_lat['status'];
					}
					
					$sel = $this->model_paper->qw("nilai_tryout","WHERE nis = '$nis' AND id_mapel = '$id_mapel' AND paket = '$paket' AND status_tes = '$latihanv'")->num_rows();
					if($sel == 0){
						$latihan = $latihanv;
					}else{
						$latihan = $latihanv+1;
					}
					$jsoal = $this->model_paper->qw("mapel_try","WHERE id_try = '$id_mapel'")->row_array();
					$jmapel = $this->model_paper->qw("soal_tryout","WHERE id_mapel = '$id_mapel' AND paket = '$paket'")->num_rows();
					if($jsoal['jumlah_soal'] < $jmapel){
						echo "Soal Belum Lengkap";
					}else{
						$x=0;
						$nomor=1;
						$soal = $this->model_paper->qw("soal_tryout","WHERE id_mapel = '$id_mapel' AND paket = '$paket'")->result();
						foreach ($soal as $soal_ty) {
							if(empty($soal_ty->listening)){
								$araysoal[] = $soal_ty->id_soal;
								$x++;
							}else{
								$id_soal = $soal_ty->id_soal;
								$sl = $this->model_paper->qw("soal_tryout","WHERE id_soal = '$id_soal'")->row_array();
								$qwjwb = $this->model_paper->qw("jawaban_tryout","WHERE nis = '$nis' AND id_mapel='$id_mapel' AND id_soal = '$id_soal' AND paket ='$paket' AND status = '$latihan'")->num_rows();
								if($qwjwb == 0){
									$aray=array(
										'id_jawaban' => '',
										'nis' => $nis,
										'id_mapel'=> $id_mapel,
										'id_soal'=>$id_soal,
										'kunci'=>$sl['kunci'],
										'paket' => $paket,
										'sts_listening'=>'Yes',
										'nomor'=>$nomor++,
										'status' => $latihan
									);
									$this->db->insert("jawaban_tryout",$aray);
								}else{

								}
							}
						}
						shuffle($araysoal);
						$x = $x-1;
						for($b=0;$b<=$x;$b++){
							$id_soal = $araysoal[$b];
								$sl = $this->model_paper->qw("soal_tryout","WHERE id_soal = '$id_soal'")->row_array();
							$qwjwb = $this->model_paper->qw("jawaban_tryout","WHERE nis = '$nis' AND id_mapel='$id_mapel' AND id_soal = '$id_soal' AND paket ='$paket' AND status = '$latihan'")->num_rows();
							if($qwjwb == 0){
								$ar = array(
									"id_jawaban" => '',
									"nis" => $nis,
									"id_mapel"=>$id_mapel,
									"id_soal"=>$id_soal,
									"kunci"=>$sl['kunci'],
									"paket"=>$paket,
									"sts_listening"=> "No",
									"nomor"=>$nomor++,
									'status`' => $latihan
								);
								$this->db->insert("jawaban_tryout",$ar);
							}else{
								
							}
							if($nomor > $jsoal['jumlah_soal']){
								break;
							}else{

							}
						}
						echo "Berhasil";
					}
				}else{ 
					$latihan = '';
					$jsoal = $this->model_paper->qw("mapel_try","WHERE id_try = '$id_mapel'")->row_array();
					$jmapel = $this->model_paper->qw("soal_tryout","WHERE id_mapel = '$id_mapel' AND paket = '$paket'")->num_rows();
					$nia = $this->model_paper->qw("nilai_tryout","WHERE nis = '$nis' AND id_mapel = '$id_mapel' AND paket = '$paket' AND status_tes = ''")->num_rows();
					if($nia <> 0){
						echo "Anda Sudah Mengerjakan Paket Ini";
					}else{
						if($jsoal['jumlah_soal'] < $jmapel){
							echo "Soal Belum Lengkap";
						}else{
							$x=0;
							$nomor=1;
							$soal = $this->model_paper->qw("soal_tryout","WHERE id_mapel = '$id_mapel' AND paket = '$paket'")->result();
							foreach ($soal as $soal_ty) {
								if(empty($soal_ty->listening)){
									$araysoal[] = $soal_ty->id_soal;
									$x++;
								}else{
									$id_soal = $soal_ty->id_soal;
									$qwjwb = $this->model_paper->qw("jawaban_tryout","WHERE nis = '$nis' AND id_mapel='$id_mapel' AND id_soal = '$id_soal' AND paket ='$paket' AND status = '$latihan'")->num_rows();
									if($qwjwb == 0){
										$aray=array(
											'id_jawaban' => '',
											'nis' => $nis,
											'id_mapel'=> $id_mapel,
											'id_soal'=>$id_soal,
											'kunci'=>$soal_ty->kunci,
											'paket' => $paket,
											'sts_listening'=>'Yes',
											'nomor'=>$nomor++,
											'status' => $latihan
										);
										$this->db->insert("jawaban_tryout",$aray);
									}else{

									}
								}
							}
							shuffle($araysoal);
							$x = $x-1;
							for($b=0;$b<=$x;$b++){
								$id_soal = $araysoal[$b];
								$qwjwb = $this->model_paper->qw("jawaban_tryout","WHERE nis = '$nis' AND id_mapel='$id_mapel' AND id_soal = '$id_soal' AND paket ='$paket' AND status = '$latihan'")->num_rows();
								if($qwjwb == 0){
									$ar = array(
										"id_jawaban" => '',
										"nis" => $nis,
										"id_mapel"=>$id_mapel,
										"id_soal"=>$id_soal,
										"kunci"=>$soal_ty->kunci,
										"paket"=>$paket,
										"sts_listening"=> "No",
										"nomor"=>$nomor++,
										'status`' => $latihan
									);
									$this->db->insert("jawaban_tryout",$ar);
								}else{
									
								}
								if($nomor > $jsoal['jumlah_soal']){
									break;
								}else{

								}
							}
							echo "Berhasil";
						}


					}
				} 
			}

			$wktc = $this->model_paper->qw("waktu_tryout","WHERE nis = '$nis' AND id_mapel = '$id_mapel' AND paket = '$paket' AND status = '$latihan'");
			$wkt = $wktc->row_array();
			$jmlw = $wktc->num_rows();
			if($jmlw == 0){
				$data_session = array(
						"jam" => 2,
						"menit" => 0,
						"detik"=> 0,
						"id_mapel" => $id_mapel,
						"paket" => $paket,
						"latihan" => $latihan
					);
				$this->session->set_userdata($data_session);
			}else{
				$data_session = array(
						"jam" => $wkt['jam'],
						"menit" => $wkt['menit'],
						"detik"=> $wkt['detik'],
						"id_mapel" => $id_mapel,
						"paket" => $paket,
						"latihan" => $latihan
					);
				$this->session->set_userdata($data_session);
			}
	}
	function peringkat(){
		if($this->session->userdata('status') == "masuk"){
			
		}else{
			redirect("login/login");
		}
		$data['title'] 	= "Paperless Exam";
		$data['menu'] 	= "main";
		$data['judul']  = 'Peringkat Mapel';
		$data['content'] = "peringkat";
		$data['mapel'] = $this->model_paper->qw("mapel","ORDER BY mapel ASC")->result();
		$data['jenis'] = $this->model_paper->qw("jenis_tes","ORDER BY id_tes ASC")->result();
		$this->load->view("dashboard",$data);
	}
	function peringkat_try(){
		if($this->session->userdata('status') == "masuk"){
			
		}else{
			redirect("login/login");
		}
		$data['title'] 	= "Paperless Exam";
		$data['menu'] 	= "main";
		$data['judul']  = 'Peringkat Mapel Tryout';
		$data['content'] = "peringkat_try";
		$data['mapel'] = $this->model_paper->qw("mapel_try","ORDER BY mapel ASC")->result();
		$this->load->view("dashboard",$data);
	}
	function peringkat_ak(){
		if($this->session->userdata('status') == "masuk"){
			
		}else{
			redirect("login/login");
		}
		$data['title'] 	= "Paperless Exam";
		$data['menu'] 	= "main";
		$data['judul']  = 'Peringkat Akademik';
		$data['content'] = "peringkat_aka";
		$data['mapel'] = $this->model_paper->qw("mapel","ORDER BY mapel ASC")->result();
		$data['jenis'] = $this->model_paper->qw("jenis_tes","ORDER BY id_tes ASC")->result();
		$this->load->view("dashboard",$data);
	}
	function nila(){
		if($this->session->userdata('status') == "masuk"){
			
		}else{
			redirect("login/login");
		}
		$data['title'] 	= "Paperless Exam";
		$data['menu'] 	= "main";
		$data['judul']  = 'Nilai Anda';
		$data['content'] = "nila";
		$data['mapel'] = $this->model_paper->qw("mapel","ORDER BY mapel ASC")->result();
		$data['jenis'] = $this->model_paper->qw("jenis_tes","ORDER BY id_tes ASC")->result();
		$this->load->view("dashboard",$data);
	}
}

?>