<?php
	class Login extends CI_Controller {
		function __construct(){
			parent::__construct();
			$this->load->model("model_login");
		}
		function login(){
			if($this->session->userdata('status') == "masuk"){
				redirect("paper/dashboard");
			}else{

			}
			$data['title'] = "Login";
			$this->load->view("login",$data);
		}
		function proses_login(){
			$usr = $this->input->post("usr");
			$pas = $this->input->post("pas");
			$ac = $this->model_login->row_login($usr);
			$ax = $ac->row_array();
			$jml = $ac->num_rows();
			if($jml <> 0){
				$paswd = str_replace("-", "", $ax['tgl']);
				if($paswd == $pas){
					if($ax['status'] == 'on'){
						echo "<script>alert('Akun anda masih aktif')</script>";
						echo "<script>document.location='login/login'</script>";
					}else{
						$data_session = array(
							"nis" => $ax['nis'],
							"nama_siswa" => $ax['nama'],
							"kelas"=> $ax['kelas'],
							"status"=>"masuk"
						);
						$this->session->set_userdata($data_session);
						redirect("paper/dashboard");
					}
				}else{
					redirect("login/login");
				}
			}else{
				redirect("login/login");
			}
		}
		function keluar(){
			$this->db->where("nis",$this->session->userdata('nis'));
			$art = array("status"=>"off");
			$this->db->update("siswa",$art);
			session_destroy();
			redirect("login/login");
		}
	}
?>