<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title;?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/login.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/responsive.css">
	<link href="https://fonts.googleapis.com/css?family=Contrail+One|Kalam" rel="stylesheet">
</head>
<body>
	<div id="avg">
			<?php
				$query=$this->db->get("identitas");
				$tamp=$query->row();
			?>
			<img src="<?php echo base_url();?>/assets/logo/<?php echo $tamp->logo;?>" id="sekolah_logo">
			<div id="nms">
				<h2><?php echo $tamp->sasaran;?></h2>
				<h1><?php echo $tamp->nama_sekolah;?></h1>
				<p><?php echo $tamp->motto;?></p>
			</div>
			<img src="<?php echo base_url();?>assets/icon/logo-afas.png" id="icon">
	</div>
	<div id="login">
		<?php echo form_open("login/proses_login");?>
			<div id="icon-text">
				<img src="<?php echo base_url();?>assets/icon/usrkecil.svg">
			</div>
			<input type="text" name="usr" placeholder="Username" required>
			<div id="icon-textp">
				<img src="<?php echo base_url();?>assets/icon/key.svg">
			</div>
			<input type="password" name="pas" placeholder="YYYYMMDD" required>
			<input type="submit" name="login" value="LOGIN">
		<?php echo form_close();?>
	</div>
	<div class="pesan">
		<span class="merah">Selamat Datang</span>
		<div class="icon">
			<img src="<?php echo base_url();?>assets/icon/orang.png">
		</div> 
		<div class="logon">
			<img src="<?php echo base_url();?>assets/icon/logo-afas.png" id="icon">
		</div>
		<div class="icon1">
			<img src="<?php echo base_url();?>assets/icon/orang1.png">
		</div>
		<div class="bawah-logo">
			<h1>SMK Wikrama 1 Jepara</h1>
			<span class="sub-pesan">Selamat Mengerjakan!!</span>
			<span class="subpesan1">Belajar + Jujur + Do'a = SUKSES</span>
		</div>
	</div>	
</body>
</html>

	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
	<script type="text/javascript">
	
		$(document).ready(function(){
			$("#tengah-login").css({"transform":"scale(1,1)"});
			$("#usr").css({"margin-left":"0px"});
			$("#pas").css({"margin-left":"0px"});
			$("#sub").css({"margin-top":"10px"});
			
		});
		/*
		 function toggleFullScreen() {
  if ((document.fullScreenElement && document.fullScreenElement !== null) ||   
   (!document.mozFullScreen && !document.webkitIsFullScreen)) {
    if (document.documentElement.requestFullScreen) { 
      document.documentElement.requestFullScreen(); 
    } else if (document.documentElement.mozRequestFullScreen) { 
      document.documentElement.mozRequestFullScreen(); 
    } else if (document.documentElement.webkitRequestFullScreen) { 
      document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT); 
    } 
  } else { 
    if (document.cancelFullScreen) { 
      document.cancelFullScreen(); 
    } else if (document.mozCancelFullScreen) { 
      document.mozCancelFullScreen(); 
    } else if (document.webkitCancelFullScreen) { 
      document.webkitCancelFullScreen(); 
    } 
  } 
}
/*
		/*
		function toggleFullScreen(elem) {
    // ## The below if statement seems to work better ## if ((document.fullScreenElement && document.fullScreenElement !== null) || (document.msfullscreenElement && document.msfullscreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
    if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
        if (elem.requestFullScreen) {
            elem.requestFullScreen();
        } else if (elem.mozRequestFullScreen) {
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullScreen) {
            elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        } else if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen();
        }
    } else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    }
}
*/
	</script>
	<script language=JavaScript>
<!--

//Disable right mouse click Script
//By Maximus (maximus@nsimail.com) w/ mods by DynamicDrive
//For full source code, visit http://www.dynamicdrive.com

var message="Function Disabled!";

///////////////////////////////////
function clickIE4(){
if (event.button==2){
alert(message);
return false;
}
}

function clickNS4(e){
if (document.layers||document.getElementById&&!document.all){
if (e.which==2||e.which==3){
alert(message);
return false;
}
}
}

if (document.layers){
document.captureEvents(Event.MOUSEDOWN);
document.onmousedown=clickNS4;
}
else if (document.all&&!document.getElementById){
document.onmousedown=clickIE4;
}

document.oncontextmenu=new Function("return false")

// -->

shortcut={all_shortcuts:{},add:function(a,b,c){var d={type:"keydown",propagate:!1,disable_in_input:!1,target:document,keycode:!1};if(c)for(var e in d)"undefined"==typeof c[e]&&(c[e]=d[e]);else c=d;d=c.target,"string"==typeof c.target&&(d=document.getElementById(c.target)),a=a.toLowerCase(),e=function(d){d=d||window.event;if(c.disable_in_input){var e;d.target?e=d.target:d.srcElement&&(e=d.srcElement),3==e.nodeType&&(e=e.parentNode);if("INPUT"==e.tagName||"TEXTAREA"==e.tagName)return}d.keyCode?code=d.keyCode:d.which&&(code=d.which),e=String.fromCharCode(code).toLowerCase(),188==code&&(e=","),190==code&&(e=".");var f=a.split("+"),g=0,h={"`":"~",1:"!",2:"@",3:"#",4:"$",5:"%",6:"^",7:"&",8:"*",9:"(",0:")","-":"_","=":"+",";":":","'":'"',",":"<",".":">","/":"?","\\":"|"},i={esc:27,escape:27,tab:9,space:32,"return":13,enter:13,backspace:8,scrolllock:145,scroll_lock:145,scroll:145,capslock:20,caps_lock:20,caps:20,numlock:144,num_lock:144,num:144,pause:19,"break":19,insert:45,home:36,"delete":46,end:35,pageup:33,page_up:33,pu:33,pagedown:34,page_down:34,pd:34,left:37,up:38,right:39,down:40,f1:112,f2:113,f3:114,f4:115,f5:116,f6:117,f7:118,f8:119,f9:120,f10:121,f11:122,f12:123},j=!1,l=!1,m=!1,n=!1,o=!1,p=!1,q=!1,r=!1;d.ctrlKey&&(n=!0),d.shiftKey&&(l=!0),d.altKey&&(p=!0),d.metaKey&&(r=!0);for(var s=0;k=f[s],s<f.length;s++)"ctrl"==k||"control"==k?(g++,m=!0):"shift"==k?(g++,j=!0):"alt"==k?(g++,o=!0):"meta"==k?(g++,q=!0):1<k.length?i[k]==code&&g++:c.keycode?c.keycode==code&&g++:e==k?g++:h[e]&&d.shiftKey&&(e=h[e],e==k&&g++);if(g==f.length&&n==m&&l==j&&p==o&&r==q&&(b(d),!c.propagate))return d.cancelBubble=!0,d.returnValue=!1,d.stopPropagation&&(d.stopPropagation(),d.preventDefault()),!1},this.all_shortcuts[a]={callback:e,target:d,event:c.type},d.addEventListener?d.addEventListener(c.type,e,!1):d.attachEvent?d.attachEvent("on"+c.type,e):d["on"+c.type]=e},remove:function(a){var a=a.toLowerCase(),b=this.all_shortcuts[a];delete this.all_shortcuts[a];if(b){var a=b.event,c=b.target,b=b.callback;c.detachEvent?c.detachEvent("on"+a,b):c.removeEventListener?c.removeEventListener(a,b,!1):c["on"+a]=!1}}},shortcut.add("Ctrl+U",function(){alert('Jangan Tekan Ctrl+U :)');});

function disableSelection(e){if(typeof e.onselectstart!="undefined")e.onselectstart=function(){return false};else if(typeof e.style.MozUserSelect!="undefined")e.style.MozUserSelect="none";else e.onmousedown=function(){return false};e.style.cursor="default"}window.onload=function(){disableSelection(document.body)}

</script>

	
