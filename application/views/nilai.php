<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title;?></title>
	<script src="<?php echo base_url();?>/assets/js/waktu.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/nilai.css">
	<script type="text/javascript">
        $(document).ready(function() {
              var detik = 05;
              var menit = 0;
              var jam   = 0;
            function hitung() {
                setTimeout(hitung,1000); 
              $('#timer').html(
                      '<h1 align="center" style="display:none;">' + jam + ' jam : ' + menit + ' menit : ' + detik + ' detik</h1><hr>'
                );
              if (detik == 0) {
              		document.location = 'dashboard';
              }else{
                detik --;
                if(detik < 0) {
                    detik = 59;
                    menit --;
 
                    if(menit < 0) {
                        menit = 59;
                        clearInterval();  
                    } 
                } 
            }
            }     
            hitung();
      }); 
    </script>
</head>
<body>
	<div id='timer'></div>
	<h1 id="judul">Nilai Anda</h1>
		<div id="axe">
			<div id="kon">
				<h1 id="h1">Nilai</h1>
			<?php
				if($status == "Belum Kompeten"){
					$col = "red";
				}else{
					$col = "#22313F";
				}
			?>
			<h1 style="color:<?php echo $col;?>" class="h1"><?php echo $nilai;?></h1>
			</div>
			<div id="kon">
				<h1 id="h1">KKM</h1>
				<h1 class="h1" style="color:#22313F;">
					<?php
						 if($kelas == "X"){
						 	echo $kkm1;
						 }elseif($kelas == "XI"){
						 	echo $kkm2;
						 }else{
						 	echo $kkm3;
						 }
					?></h1>
			</div>
		</div>
	<center>
		<h2 style="color:<?php echo $col;?>"><?php echo $status;?></h2>
	</center>
	<table width="500px">
		<tr>
			<td width="120px">Salah</td>
			<td width="20px">:</td>
			<td><?php echo $salah;?></td>
		</tr>
		<tr>
			<td width="120px">Benar</td>
			<td width="20px">:</td>
			<td><?php echo $benar;?></td>
		</tr>
		<tr>
			<td width="120px">Jumlah Soal</td>
			<td width="20px">:</td>
			<td><?php echo $jumlah_soal;?></td>
		</tr>
	</table>
	<a href="<?php echo site_url('login/keluar');?>" style="text-decoration:none;">
	<div id="keluar">
		<h1>Keluar</h1>
	</div>
	</a>
</body>
</html>