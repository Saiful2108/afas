<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title;?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/home.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/responsive.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/dist/sweetalert.css">
</head>
<body>
	<?php
		include "menu/".$menu.".php";
	?>
	<div id="kon">
		<?php
			include "content/".$content.".php";
		?>
	</div>
	<div id="bgbox" class="test">
	<div id="frme">
		<div id="jdlf">
			<h1>Pilih Matapelajaran !</h1>
		</div>
		<p>Nama : </p>
		<p><input type="text" id="nama_t" value="<?php echo $this->session->userdata('nama_siswa');?>" disabled></p>
		<p>Kelas : </p>
		<p><input type="text" id="kls_t" value="<?php echo $this->session->userdata('kelas');?>" disabled></p>
		<p>Mapel : </p>
		<p>
			<select id="mapel_t" >
				<?php
					$kelas= $this->session->userdata('kelas');
					$mapel = $this->model_paper->qw("soal_aktif","WHERE kelas = '$kelas' GROUP BY id_mapel")->result();
					foreach ($mapel as $val) {
						$id_mapel = $val->id_mapel;
						$mapele = $this->model_paper->qw("mapel","WHERE id_mapel = '$id_mapel'")->row_array();
				?>
					<option value="<?php echo $val->id_mapel;?>"><?php echo $mapele['mapel'];?></option>
				<?php } ?>
			</select>
		</p>
		<p>Jenis Tes : </p>
		<p>
			<select id="jenis_tes_t" disabled="">
				<?php
					$jenis = $this->model_paper->qw("soal_aktif","WHERE kelas = '$kelas' GROUP BY jenis_tes")->result();
					foreach ($jenis as $val) {
						$id_jenis = $val->jenis_tes;
						$jnise = $this->model_paper->qw("jenis_tes","WHERE id_tes = '$id_jenis'")->row_array();
				?>
					<option value="<?php echo $val->jenis_tes;?>"><?php echo $jnise['jenis_tes'];?></option>
				<?php } ?>
			</select>
		</p>
		<button id="lnjt" onClick="kerjasoal()">
			<p>Lanjutkan !</p>
		</button>
		<button id="btl" onClick="btl('test')">
			<p>Batal</p>
		</button>
	</div>
</div>
<div id="bgbox" class="try">
	<div id="frme1">
		<div id="jdlf">
			<h1>Pilih Matapelajaran !</h1>
		</div>
		<p>Nama : </p>
		<p><input type="text" id="nama" value="<?php echo $this->session->userdata('nama_siswa');?>" disabled></p>
		<p>Kelas : </p>
		<p><input type="text" id="kls" value="<?php echo $this->session->userdata('kelas');?>" disabled></p>
		<p>Mapel : </p>
		<p>
			<select id="mapel">
				<?php
					$mapel_try = $this->model_paper->qw("mapel_try","ORDER BY mapel ASC")->result();
					foreach ($mapel_try as $val) {
				?>
					<option value="<?php echo $val->id_try;?>"><?php echo $val->mapel;?></option>
				<?php } ?>
			</select>
		</p>
		<button id="lnjt" onClick="xcv('mapel')">
			<p>Lanjutkan !</p>
		</button>
		<button id="btl" onClick="btl('try')">
			<p>Batal</p>
		</button>
	</div>
</div>
<div id="loading">
	<h1 id="h1lad">Harap tunggu sedang memproses !</h1>
	<div id="tenga">
		<div id="load">
			<div id="load2">
				<div id="load3"></div>
			</div>
		</div>
	</div>
	<h1 id="hilo">Loading...</h1>
	<h1 id="nom"></h1>
</div>
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/dist/sweetalert.min.js"></script>
	<script type="text/javascript">
		function xcv(cv){
			var t = $("#"+cv).val();
			document.location = '<?php echo site_url('paper/paket');?>/'+t;
		}
	</script>
	<?php
		if($this->session->userdata('kelas') == "XII"){
	?>
	<script type="text/javascript">
	function bn(){
			var k = document.getElementById("menu").style.height;
			if(k == "410px"){
				$("#menu").css({"height":"0px"});
				$("#menuic").css({"backgroundColor":"transparent"});
				$("#menust1").css({"transform":"rotate(0deg)","margin":"20px auto 0px auto","backgroundColor":"#3F3D3D"});
				$("#menust3").css({"transform":"rotate(0deg)","margin":"7px auto","backgroundColor":"#3F3D3D"});
				$("#menust2").fadeIn(1);
			}else{
				$("#menuic").css({"backgroundColor":"#2d2828"});
				$("#menust1").css({"transform":"rotate(45deg)","margin":"30px 22px","backgroundColor":"#fff"});
				$("#menust3").css({"transform":"rotate(-45deg)","margin":"-32px 22px","backgroundColor":"#fff"});
				$("#menust2").fadeOut(1);
				$("#menu").css({"height":"410px"});
			}
		}
		function hrf(link){
			document.location=link;
		}
	</script>
	<?php
		}else{
	?>
	<script type="text/javascript">
	function bn(){
			var k = document.getElementById("menu").style.height;
			if(k == "210px"){
				$("#menu").css({"height":"0px"});
				$("#menuic").css({"backgroundColor":"transparent"});
				$("#menust1").css({"transform":"rotate(0deg)","margin":"20px auto 0px auto","backgroundColor":"#3F3D3D"});
				$("#menust3").css({"transform":"rotate(0deg)","margin":"7px auto","backgroundColor":"#3F3D3D"});
				$("#menust2").fadeIn(1);
			}else{
				$("#menuic").css({"backgroundColor":"#2d2828"});
				$("#menust1").css({"transform":"rotate(45deg)","margin":"30px 22px","backgroundColor":"#fff"});
				$("#menust3").css({"transform":"rotate(-45deg)","margin":"-32px 22px","backgroundColor":"#fff"});
				$("#menust2").fadeOut(1);
				$("#menu").css({"height":"210px"});
			}
		}
		function hrf(link){
			document.location=link;
		}
	</script>
	<?php } ?>
	<script type="text/javascript">
		 
		function kerjasoal(){
			$("#loading").fadeIn(100);
			$.ajax({
				url:'<?php echo site_url('paper/kerjasoal');?>',
				type:'POST',
				data:{
					kelas : $("#kls_t").val(),
					mapel : $("#mapel_t").val(),
					jenis : $("#jenis_tes_t").val()
				},
				success:function(data){
					if(data == "Berhasil"){
						setInterval("pp()",1000);
					}else{
						var t = data;
						swal("Gagal", t, "error");
						$("#loading").fadeOut(1);
					}
				}
			});
		}
		var x = 3;
		function pp(){
			$("#nom").html(x);
			if(x == 0){
				document.location = '<?php echo site_url('paper/soal');?>';
			}else{
				x--;
			}
		}
		
		function btl(c){
			$("."+c).fadeOut(100);
		}
		function bx(c){
			$("."+c).fadeIn(100);
		}
	</script>
</body>
</html>