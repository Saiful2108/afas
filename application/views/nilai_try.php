<style type="text/css">
	*{
		margin: 0px;
		padding: 0px;
		font-family: Segoe UI;
	}
	#judul{
		width: 100%;
		text-align: center;
		margin: 20px 0px;
		font-size: 40px;
		color: #22313F;
	}
	#table{
		width: 500px;
		margin: 10px auto;
	}
	table{
		margin: 10px auto;
		font-size: 20px;
	}
	body{
		background-color: #ddd;
	}
	.h1{
		font-size: 120px;
	}
	table td span{
		font-size: 40px;
		color: #22313F;
	}
	h2{
		font-size: 40px;
		font-weight: normal;
	}
	#keluar{
		width: 120px;
		height: 40px;
		margin: 20px auto;
		cursor: pointer;
		background-color: #513ebc;
	}
	#keluar h1{
		font-size: 25px;
		font-weight: normal;
		border-radius: 2px;
		cursor: pointer;
		text-align: center;
		color: #fff;
	}
</style>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title;?></title>
</head>
<body>
	<h1 id="judul">Nilai Anda</h1>
	<table border="1px" id="table">
		<tr>
			<td align="center"><span>Nilai</span></td>
			<td align="center"><span>KKM</span></td>
		</tr>
		<tr>
			<td align="center">
			<?php
				if($status == "Belum Kompeten"){
					$col = "red";
				}else{
					$col = "#22313F";
				}
			?>
				<h1 style="color:<?php echo $col;?>" class="h1"><?php echo $nilai;?></h1>
			</td>
			<td align="center">
				<h1 class="h1" style="color:#22313F;"><?php echo $kkm;?></h1>
			</td>
		</tr>
	</table>
	<center>
		<h2 style="color:<?php echo $col;?>"><?php echo $status;?></h2>
	</center>
	<table width="500px">
		<tr>
			<td width="120px">Salah</td>
			<td width="20px">:</td>
			<td><?php echo $salah;?></td>
		</tr>
		<tr>
			<td width="120px">Benar</td>
			<td width="20px">:</td>
			<td><?php echo $benar;?></td>
		</tr>
		<tr>
			<td width="120px">Jumlah Soal</td>
			<td width="20px">:</td>
			<td><?php echo $jumlah_soal;?></td>
		</tr>
	</table>
	<a href="<?php echo site_url('login/keluar');?>" style="text-decoration:none;">
	<div id="keluar">
		<h1>Keluar</h1>
	</div>
	</a>
</body>
</html>