<div id="smn">
	
	<h1><?php echo $judul;?></h1>
	<?php echo form_open();?>
	<table cellspacing="10">
		<tr>
			<td width="200px">Mapel :</td>
			<td width="200px">Paket :</td>
			<td></td>
		</tr>
		<tr>
			<td>
				<select name="mapel">
					<?php
						foreach ($mapel as $dmapel) {
					?>
						<option value="<?php echo $dmapel->id_try;?>" <?php if($this->input->post("mapel") == $dmapel->id_try){echo "selected";}?>><?php echo $dmapel->mapel;?></option>
					<?php } ?>
				</select>
			</td>
			<td>
				<select name="jenis">
					<?php
						for ($x=1;$x<=4;$x++) {
					?>
						<option value="<?php echo $x;?>" <?php if($this->input->post("jenis") == $x){echo "selected";}?>><?php echo $x;?></option>
					<?php } ?>
				</select>
			</td>
			<td>
				<button type="submit" id="cmt"><p>Tampilkan</p></button>
			</td>
		</tr>
	</table>
	<?php echo form_close();?>
	<table cellspacing="0" id="table">
		<tr>
			<th>Peringkat</th>
			<th>Nama</th>
			<th>Nilai</th>
			<th>Rombel</th>
			<th>Tanggal Megerjakan</th>
		</tr>
		<?php
			$id_mapel = $this->input->post("mapel");
			$id_jenis = $this->input->post("jenis");
			$kelas = $this->input->post("kelas");
			if(!empty($id_mapel)){
				$x=0;
				$sis = $this->model_paper->qw("nilai_tryout,siswa,mapel_try","WHERE nilai_tryout.nis = siswa.nis AND nilai_tryout.id_mapel = mapel_try.id_try AND nilai_tryout.id_mapel = '$id_mapel' AND nilai_tryout.paket = '$id_jenis' ORDER BY nilai_tryout.nilai DESC")->result();
				foreach ($sis as $data_siswa) { $x++;
		?>
		<?php
			if($data_siswa->nis == $this->session->userdata("nis")){
				$bg = "#9f85db";
				$col = "#fff";
			}else{
				$bg ="";
				$col = "";
			}
		?>
		<tr>
			<td bgcolor="<?php echo $bg;?>" style="color:<?php echo $col;?>"><?php echo $x;?></td>
			<td bgcolor="<?php echo $bg;?>" style="color:<?php echo $col;?>"><?php echo $data_siswa->nama;?></td>
			<td bgcolor="<?php echo $bg;?>" style="color:<?php echo $col;?>"><?php echo $data_siswa->nilai;?></td>
			<td bgcolor="<?php echo $bg;?>" style="color:<?php echo $col;?>">
				<?php
					$vm = $data_siswa->id_rombel;
					$dr = $this->model_paper->qw("rombel","WHERE id_rombel = '$vm'")->row_array();
					echo $dr['rombel'];
				?>
			</td>
			<td bgcolor="<?php echo $bg;?>" style="color:<?php echo $col;?>"><?php echo $data_siswa->tgl_kerja;?></td>
		</tr>
		<?php 
				}
			}
		?>
	</table>
</div>
</style>