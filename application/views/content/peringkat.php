<div id="smn">
	<div id="menu-peringkat">
		<div id="per">
			<p>Peringkat Mapel</p>
		</div>
		<a href="<?php echo site_url('paper/peringkat_ak');?>">
		<div id="pera">
			<p>Peringkat Akademik</p>
		</div>
		</a>
		<a href="<?php echo site_url('paper/nila');?>">
		<div id="nil">
			<p>Nilai Anda</p>
		</div>
		</a>
	</div>
	<h1><?php echo $judul;?></h1>
	<?php echo form_open();?>
	<table cellspacing="10" id="tam">
		<tr>
			<td width="200px">Mapel :</td>
			<td width="200px">Jenis Tes :</td>
			<td width="200px">Kelas :</td>
			<td></td>
		</tr>
		<tr>
			<td>
				<select name="mapel">
					<?php
						foreach ($mapel as $dmapel) {
					?>
						<option value="<?php echo $dmapel->id_mapel;?>" <?php if($this->input->post("mapel") == $dmapel->id_mapel){echo "selected";}?>><?php echo $dmapel->mapel;?></option>
					<?php } ?>
				</select>
			</td>
			<td>
				<select name="jenis">
					<?php
						foreach ($jenis as $djenis) {
					?>
						<option value="<?php echo $djenis->id_tes;?>" <?php if($this->input->post("jenis") == $djenis->id_tes){echo "selected";}?>><?php echo $djenis->jenis_tes;?></option>
					<?php } ?>
				</select>
			</td>
			<td>
				<select name="kelas">
						<option value="X" <?php if($this->input->post("kelas") == "X"){echo "selected";}?>>X</option>
						<option value="XI" <?php if($this->input->post("kelas") == "XI"){echo "selected";}?>>XI</option>
						<option value="XII" <?php if($this->input->post("kelas") == "XII"){echo "selected";}?>>XII</option>
				</select>
			</td>
			<td>
				<button type="submit" id="cmt"><p>Tampilkan</p></button>
			</td>
		</tr>
	</table>
	<?php echo form_close();?>
	<?php echo form_open();?>
	<table cellspacing="10px" id="hiden" width="100%">
		<tr>
			<td width="10%">
			<p>Mapel : </p>
				<select name="mapel" id="aaa">
					<?php
						foreach ($mapel as $dmapel) {
					?>
						<option value="<?php echo $dmapel->id_mapel;?>" <?php if($this->input->post("mapel") == $dmapel->id_mapel){echo "selected";}?>><?php echo $dmapel->mapel;?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
			<p>Jenis Tes : </p>
				<select name="jenis">
					<?php
						foreach ($jenis as $djenis) {
					?>
						<option value="<?php echo $djenis->id_tes;?>" <?php if($this->input->post("jenis") == $djenis->id_tes){echo "selected";}?>><?php echo $djenis->jenis_tes;?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
			<p>Kelas</p>
				<select name="kelas">
						<option value="X" <?php if($this->input->post("kelas") == "X"){echo "selected";}?>>X</option>
						<option value="XI" <?php if($this->input->post("kelas") == "XI"){echo "selected";}?>>XI</option>
						<option value="XII" <?php if($this->input->post("kelas") == "XII"){echo "selected";}?>>XII</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<button type="submit" id="cmt"><p>Tampilkan</p></button>
			</td>
		</tr>
	</table>
	<?php echo form_close();?>
	<table cellspacing="0" id="table">
		<tr>
			<th>Peringkat</th>
			<th>Nama</th>
			<th>Nilai</th>
			<th>Rombel</th>
			<th>Tanggal Megerjakan</th>
		</tr>
		<?php
			$id_mapel = $this->input->post("mapel");
			$id_jenis = $this->input->post("jenis");
			$kelas = $this->input->post("kelas");
			if(!empty($id_mapel)){
				$x=0;
				$sis = $this->model_paper->qw("nilai,siswa,mapel,jenis_tes","WHERE nilai.nis = siswa.nis AND nilai.id_mapel = mapel.id_mapel AND nilai.id_jenis = jenis_tes.id_tes AND nilai.id_mapel = '$id_mapel' AND nilai.id_jenis = '$id_jenis' AND nilai.kelas = '$kelas' ORDER BY nilai.nilai DESC")->result();
				foreach ($sis as $data_siswa) { $x++;
		?>
		<?php
			if($data_siswa->nis == $this->session->userdata("nis")){
				$bg = "#9f85db";
				$col = "#fff";
			}else{
				$bg ="";
				$col = "";
			}
		?>
		<tr>
			<td bgcolor="<?php echo $bg;?>" style="color:<?php echo $col;?>"><?php echo $x;?></td>
			<td bgcolor="<?php echo $bg;?>" style="color:<?php echo $col;?>"><?php echo $data_siswa->nama;?></td>
			<td bgcolor="<?php echo $bg;?>" style="color:<?php echo $col;?>"><?php echo $data_siswa->nilai;?></td>
			<td bgcolor="<?php echo $bg;?>" style="color:<?php echo $col;?>">
				<?php
					$dr = $this->model_paper->qw("rombel","WHERE id_rombel = '$data_siswa->id_rombel'")->row_array();
					echo $dr['rombel'];
				?>
			</td>
			<td bgcolor="<?php echo $bg;?>" style="color:<?php echo $col;?>"><?php echo $data_siswa->tgl_kerja;?></td>
		</tr>
		<?php 
				}
			}
		?>
	</table>
</div>
</style>