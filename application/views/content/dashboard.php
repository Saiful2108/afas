<div id="logo">
	<img src="<?php echo base_url();?>/assets/icon/logo-afas.png">
</div>
<div id="tkbh">
	<div id="ltk" onClick="bx('test')">
		<h2>Lakukan Test ! </h2>
		<center>
		<img src="<?php echo base_url();?>/assets/icon/pencil.png">
		</center>
		<p>Kerjakan Test yang anda inginkan dan raihlah peringkat teratas di mapel yang anda kerjakan.</p>
	</div>
	<a href="<?php echo site_url('paper/peringkat');?>" style="text-decoration:none;">
	<div id="ltk">
		<h2>Lihat Nilai ! </h2>
		<center>
		<img src="<?php echo base_url();?>/assets/icon/peringkat.png">
		</center>
		<p>Lihat peringkat anda dalam mata pelajaran yang telah anda kerjakan.</p>
	</div>
	</a>
	<div id="ltk" onClick="bx('try')" <?php echo $hid;?>>
		<h2>Lakukan Try Out ! </h2>
		<center>
		<img src="<?php echo base_url();?>/assets/icon/try.svg">
		</center>
		<p>Kerjakan Try Out untuk melatih anda agar terbiasa dengan Ujian Nasional.</p>
	</div>
	<a href="<?php echo site_url('paper/peringkat_try');?>" style="text-decoration:none;">
	<div id="ltk" <?php echo $hid;?>>
		<h2>Hasil Try Out ! </h2>
		<center>
		<img src="<?php echo base_url();?>/assets/icon/peringkat.png">
		</center>
		<p>Lihat hasil Try Out anda agar bisa tau status nilai anda dan meningkatkan kemampuan anda.</p>
	</div>
	</a>
</div>