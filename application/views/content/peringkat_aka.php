<div id="smn">
	<div id="menu-peringkat">
		<a href="<?php echo site_url('paper/peringkat');?>">
		<div id="per">
			<p>Peringkat Mapel</p>
		</div>
		</a>
		<div id="pera">
			<p>Peringkat Akademik</p>
		</div>
		<a href="<?php echo site_url('paper/nila');?>">
		<div id="nil">
			<p>Nilai Anda</p>
		</div>
		</a>
	</div>

	<h1><?php echo $judul;?></h1>
	<?php echo form_open();?>
	<table cellspacing="10">
		<tr>
			<td width="200px">Jenis Tes :</td>
			<td width="200px">Kelas :</td>
			<td></td>
		</tr>
		<tr>
			<td>
				<select name="jenis">
					<?php
						foreach ($jenis as $djenis) {
					?>
						<option value="<?php echo $djenis->id_tes;?>" <?php if($this->input->post("jenis") == $djenis->id_tes){echo "selected";}?>><?php echo $djenis->jenis_tes;?></option>
					<?php } ?>
				</select>
			</td>
			<td>
				<select name="kelas">
						<option value="X" <?php if($this->input->post("kelas") == "X"){echo "selected";}?>>X</option>
						<option value="XI" <?php if($this->input->post("kelas") == "XI"){echo "selected";}?>>XI</option>
						<option value="XII" <?php if($this->input->post("kelas") == "XII"){echo "selected";}?>>XII</option>
				</select>
			</td>
			<td>
				<button type="submit" id="cmt"><p>Tampilkan</p></button>
			</td>
		</tr>
	</table>
	<?php echo form_close();?>
	<table cellspacing="0" id="table">
		<tr>
			<th>Peringkat</th>
			<th>Nama</th>
			<th>Rombel</th>
			<th>Jumlah Mapel</th>
			<th>Rata- Rata</th>
		</tr>
		<?php
			$id_jenis = $this->input->post("jenis");
			$kelas = $this->input->post("kelas");
			if(!empty($id_jenis)){
				$x=0;
				$sis = $this->db->query("SELECT siswa.nis, siswa.nama, nilai.nilai, nilai.id_rombel FROM nilai,siswa,mapel,jenis_tes 
										WHERE nilai.nis = siswa.nis AND 
											  nilai.id_mapel = mapel.id_mapel AND
											  nilai.id_jenis = jenis_tes.id_tes AND  
											  nilai.id_jenis = '$id_jenis' AND 
											  nilai.kelas = '$kelas' GROUP BY nis ORDER BY sum(nilai.nilai) DESC")->result();
				foreach ($sis as $data_siswa) { $x++;
		?>
		<?php
			if($data_siswa->nis == $this->session->userdata("nis")){
				$bg = "#9f85db";
				$col = "#fff";
			}else{
				$bg ="";
				$col= "";
			}
		?>
		<tr>
			<td bgcolor="<?php echo $bg;?>" style="color:<?php echo $col;?>"><?php echo $x;?></td>
			<td bgcolor="<?php echo $bg;?>" style="color:<?php echo $col;?>"><?php echo $data_siswa->nama;?></td>
			<td bgcolor="<?php echo $bg;?>" style="color:<?php echo $col;?>">
				<?php
					$id_rom = $data_siswa->id_rombel;
					$selr = $this->model_paper->qw("rombel","WHERE id_rombel = '$id_rom'")->row_array();
					echo $selr['rombel'];
				?>
			</td>
			<td bgcolor="<?php echo $bg;?>" style="color:<?php echo $col;?>">
				<?php
					$ns = $data_siswa->nis;
					$jml = $this->model_paper->qw("nilai","WHERE id_jenis = '$id_jenis' AND kelas='$kelas' AND nis = '$ns'");
					$jnil = $jml->num_rows();
					$nlrat = 0;
					foreach ($jml->result() as $rt) {
						$nlrat = $nlrat+$rt->nilai;
					}
					echo $jnil;
				?>
			</td>
			<td bgcolor="<?php echo $bg;?>" style="color:<?php echo $col;?>"><?php echo ceil($nlrat / $jnil);;?></td>
		</tr>
		<?php 
				}
			}
		?>
	</table>
</div>
</style>