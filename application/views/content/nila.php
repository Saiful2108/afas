<div id="smn">
	<div id="menu-peringkat">
		<a href="<?php echo site_url('paper/peringkat');?>">
		<div id="per">
			<p>Peringkat Mapel</p>
		</div>
		</a>
		<a href="<?php echo site_url('paper/peringkat_ak');?>">
		<div id="pera">
			<p>Peringkat Akademik</p>
		</div>
		</a>
		<div id="nil">
			<p>Nilai Anda</p>
		</div>
	</div>

	<h1><?php echo $judul;?></h1>
	<?php echo form_open();?>
	<table cellspacing="10">
		<tr>
			<td width="200px">Jenis Tes :</td>
			<td></td>
		</tr>
		<tr>
			<td>
				<select name="jenis">
					<?php
						foreach ($jenis as $djenis) {
					?>
						<option value="<?php echo $djenis->id_tes;?>" <?php if($this->input->post("jenis") == $djenis->id_tes){echo "selected";}?>><?php echo $djenis->jenis_tes;?></option>
					<?php } ?>
				</select>
			</td>
			<td>
				<button type="submit" id="cmt"><p>Tampilkan</p></button>
			</td>
		</tr>
	</table>
	<?php echo form_close();?>
	<table cellspacing="0" id="table">
		<tr>
			<th>No</th>
			<th>Mapel</th>
			<th>Nilai</th>
			<th>Rombel</th>
			<th>Tanggal Megerjakan</th>
		</tr>
		<?php
			$id_jenis = $this->input->post("jenis");
			$nis = $this->session->userdata("nis");
			if(!empty($id_jenis)){
				$x=0;
				$sis = $this->model_paper->qw("nilai,siswa,mapel,jenis_tes","WHERE nilai.nis = siswa.nis AND nilai.id_mapel = mapel.id_mapel AND nilai.id_jenis = jenis_tes.id_tes AND nilai.id_jenis = '$id_jenis' AND nilai.nis = '$nis'")->result();
				foreach ($sis as $data_siswa) { $x++;
		?>
		<tr>
			<td><?php echo $x;?></td>
			<td><?php echo $data_siswa->mapel;?></td>
			<td><?php echo $data_siswa->nilai;?></td>
			<td>
				<?php
					$dr = $this->model_paper->qw("rombel","WHERE id_rombel = '$data_siswa->id_rombel'")->row_array();
					echo $dr['rombel'];
				?>
			</td>
			<td><?php echo $data_siswa->tgl_kerja;?></td>
		</tr>
		<?php 
				}
			}
		?>
	</table>
</div>
</style>