<style type="text/css">
	*{
		font-family: Segoe UI;
	}
	select{
		width: 200px;
		padding: 7px 0px;
		border:1px solid #ddd;
		text-indent: 5px;
		font-size: 16px;
		border-radius: 2px;
	}
	input[type="submit"]{
		background-color: #4b4ba3;
		border: 1px solid #4b4ba3;
		color: #fff;
		padding: 10px 25px;
		border-radius: 2px;
		cursor: pointer;
	}
	.xdc{
		width: 300px;
		height: 200px;
	}
	.h1{
		width: 100%;
		text-align: center;
		font-size: 30px;
		color: #22313F;
		margin: 0px auto;
	}
	.p{
		width: 100%;
		text-align: center;
		color: #22313F;
		font-weight: bolder;
	}
	.table{
		width: 100%;
	}
	.table th{
		height: 40px;
		background-color: #6553db;
		color: #fff;
	}
	.table td{
		border-bottom: 1px solid #ddd;
		height: 30px;
		font-size: 13px;
	}
</style>
<!DOCTYPE html>
<html>
<head>
	<title>Lihat Nilai</title>
</head>
<body onload="<?php echo $print;?>">
	<h1 class="h1">Data Nilai <?php echo $pel;?></h1>
	<?php
		$xx= $id_jenis = $this->input->post("jen");
		$v = $this->model_admin->qw("jenis_tes","WHERE id_tes = '$xx'")->row_array();
	?>
	<p class="p"><?php echo $v['jenis_tes'];?></p>
	<?php echo form_open("admin/lihat_nilai/cari");?>
		<table cellspacing="5" <?php echo $hid;?>>
			<tr>
				<td><p>Mata Pelajaran :</p></td>
				<td><p>Jenis Tes :</p></td>
				<td><p>Rombel :</p></td>
			</tr>
			<tr>
				<td>
					<select name="pelajaran">
						<?php
							foreach ($var as $dtd) {
						?>
						<option value="<?php echo $dtd->id_mapel;?>" <?php if($this->input->post("pelajaran") == $dtd->id_mapel){echo "selected";};?>><?php echo $dtd->mapel;?></option>

						<?php } ?>
					</select>
				</td>
				<td>
					<select name="jen">
						<?php
							foreach ($jns as $dtd) {
						?>
						<option value="<?php echo $dtd->id_tes;?>" <?php if($this->input->post("jen") == $dtd->id_tes){echo "selected";};?>><?php echo $dtd->jenis_tes;?></option>

						<?php } ?>
					</select>
				</td>
				<td>
					<select name="rombel">
					<?php
						foreach ($rombel as $datrom) {
					?>
						<option value="<?php echo $datrom->id_rombel;?>" <?php if($this->input->post('rombel') == $datrom->id_rombel){ echo "selected";};?>><?php echo $datrom->rombel;?></option>
					<?php } ?>
					</select>
				</td>
				<td>
					<input type="submit" name="cari" value="PRINT">
				</td>
			</tr>
		</table>
	<?php echo form_close();?>
	<table width="100%" cellspacing="0px" cellpadding="10px" class="table">
			<th align="left" width="20px">No</th>
			<th align="left">NIS</th>
			<th align="left">Nama</th>
			<th align="left">Rombel</th>
			<th align="left">Nilai</th>
		</tr>
		<?php
			$urt = $this->uri->segment(3);
			if(empty($urt)){

			}else{
				$id_mapel = $this->input->post("pelajaran");
				$id_jenis = $this->input->post("jen");
				$id_rombel = $this->input->post("rombel");
				$qw= $this->model_admin->qw("nilai,siswa,rombel","WHERE siswa.nis = nilai.nis AND rombel.id_rombel = nilai.id_rombel AND nilai.id_mapel = '$id_mapel' AND nilai.id_jenis = '$id_jenis' AND nilai.id_rombel = '$id_rombel'")->result();
				$x = 0;
				foreach ($qw as $dtnil) { $x++;
		?>
		<tr>
			<td><?php echo $x;?></td>
			<td><?php echo $dtnil->nis;?></td>
			<td><?php echo $dtnil->nama;?></td>
			<td><?php echo $dtnil->rombel;?></td>
			<td><?php echo $dtnil->nilai;?></td>
		</tr>
		<?php } }?>
	</table>
</body>
</html>