<style type="text/css">
    #container{
         position: fixed;
        width: 100%;
        height: 100%;
        background-color: rgba(0,0,0,0.7);
        left: 0px;
        bottom: 0px;
    }
</style>
<script type="text/javascript" src="<?php echo base_url();?>../assets/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>../assets/js/highcharts.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>../assets/js/exporting.js"></script>
    <script type="text/javascript">
        var chart1; 
        $(document).ready(function() {
              chart1 = new Highcharts.Chart({
                 chart: {
                    renderTo: 'gr',
                    type: 'column'
                 },   
                 title: {
                    text: 'Grafik Nilai '
                 },
                 subtitle:{
                  text : 'TAHUN 2015 - 2016'
                 },
                 xAxis: {
                    categories: ['Mapel'],
                    align: 'right',
                    style: {
                      fontSize: '10px',
                      fontFamily: 'Verdana, sans-serif'
                     }
                 },
                 yAxis: {
                    title: {
                       text: 'Nilai Rata - Rata Siswa'
                    }
                 },
                      series:             
                    [
                    <?php 
                    $sql   = $this->model_admin->qw("mapel","ORDER BY id_mapel ASC")->result();
                    foreach( $sql as $ret){
                      $jumlah = 0;
                      $nn=0;
                        $merek=$ret->id_mapel;
                        $mapel=$ret->mapel;                     
                         $sql_jumlah   = $this->model_admin->qw("nilai","WHERE id_mapel='$merek' AND id_jenis = 'IT001'")->result();       
                         $x=0;
                         foreach( $sql_jumlah as $data){ $x++;
                            $jumlah = $jumlah + $data->nilai;
                            $nn = $jumlah/$x;
                          }
                                       
                          ?>
                          {
                              name: '<?php echo $mapel; ?>',
                              data: [<?php echo ceil($nn); ?>]
                          },
                          <?php } ?>
                    ]
              });
           });  
    </script>
        <div id='gr'>
        </div>  