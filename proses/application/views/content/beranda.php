<script type="text/javascript" src="<?php echo base_url();?>../assets/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>../assets/js/highcharts.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>../assets/js/exporting.js"></script>
<div class="top">
	<div class="kiri"><p>Beranda</p></div>
	<div id="clear"></div>
</div>
<div class="bawah">
	<div id="beranda" style="background-color:#00C0EF;">
		<div class="ata">
			<h1><?php echo $jmlsiswa;?></h1>
			<p>Jumlah Siswa</p>
			<img src="<?php echo base_url();?>../assets/icon/men1.svg">
		</div>
		<div class="baw" style="background-color:#00ACD7"><h6>More Info ></h6></div>
	</div>
	<div id="beranda" style="background-color:#F39C12">
		<div class="ata">
			<h1><?php echo $jmlguru;?></h1>
			<p>Jumlah Guru</p>
			<img src="<?php echo base_url();?>../assets/icon/men2.svg">
		</div>
		<div class="baw" style="background-color:#DA8C10"><h6>More Info ></h6></div>
	</div>
	<div id="beranda" style="background-color:#F56954">
		<div class="ata">
			<h1><?php echo $jmljurusan;?></h1>
			<p>Jumlah Jurusan</p>
			<img src="<?php echo base_url();?>../assets/icon/men3.svg">
		</div>
		<div class="baw" style="background-color:#DC5E4B"><h6>More Info ></h6></div>
	</div>
	<div id="beranda" style="background-color:#85144B">
		<div class="ata">
			<h1><?php echo $jmlrom;?></h1>
			<p>Jumlah Rombel</p>
			<img src="<?php echo base_url();?>../assets/icon/men4.svg">
		</div>
		<div class="baw" style="background-color:#771243"><h6>More Info ></h6></div>
	</div>
	<?php 
		if($this->input->post('ber') == "id_jenis"){
			$xxpl = "Jenis Tes : ";
		}elseif($this->input->post('ber') == "id_rombel"){
			$xxpl = "Rombel : ";
		}elseif($this->input->post('ber') == "kelas"){
			$xxpl = "Kelas : ";
		}else{
			$xxpl = "Semua Kategori";
		}
	?>
	<script type="text/javascript">
		var chart1; 
		$(document).ready(function() {
		      chart1 = new Highcharts.Chart({
		         chart: {
		            renderTo: 'gr',
		            type: 'column'
		         },   
		         title: {
		            text: 'Grafik Nilai '
		         },
		         subtitle:{
		          text : '<?php echo $xxpl;?><?php echo $this->input->post('cri');?>'
		         },
		         xAxis: {
		            categories: ['Mapel'],
		            align: 'right',
		            style: {
		              fontSize: '10px',
		              fontFamily: 'Verdana, sans-serif'
		             }
		         },
		         yAxis: {
		            title: {
		               text: 'Nilai Rata - Rata Siswa'
		            }
		         },
		              series:             
		            [
		            <?php 
		        	$sql   = $this->model_admin->qw("mapel","ORDER BY id_mapel ASC")->result();
		            foreach( $sql as $ret){
		              $jumlah = 0;
		              $nn=0;
		            	$merek=$ret->id_mapel;
		              	$mapel=$ret->mapel;
		              	 $bxcv = $this->input->post('ber');
		              	 $dxcv = $this->input->post('cri');
		              	 if(empty($bxcv)){
		              	 	$sql_jumlah   = $this->model_admin->qw("nilai","WHERE id_mapel='$merek'")->result();	
		              	 }else{
		              	 	if($bxcv == "id_jenis"){
		              	 		$qwjen = $this->model_admin->qw("jenis_tes","WHERE jenis_tes = '$dxcv'")->row_array();
		              	 		$dxcv = $qwjen['id_tes'];	
		              	 	}elseif($bxcv == "id_rombel"){
		              	 		$qwjen = $this->model_admin->qw("rombel","WHERE rombel = '$dxcv'")->row_array();
		              	 		$dxcv = $qwjen['id_rombel'];
		              	 	}else{

		              	 	}
		              	 	$sql_jumlah   = $this->model_admin->qw("nilai","WHERE id_mapel='$merek' AND $bxcv = '$dxcv'")->result();
		              	 }
		                 $x=0;
		                 foreach( $sql_jumlah as $data){ $x++;
		                    $jumlah = $jumlah + $data->nilai;
		                    $nn = $jumlah/$x;
		                  }
		                               
		                  ?>
		                  {
		                      name: '<?php echo $mapel; ?>',
		                      data: [<?php echo ceil($nn); ?>]
		                  },
		                  <?php } ?>
		            ]
		      });
		   });	
	</script>
		<div id='gr'>

    	</div>
    	<div id="pencarian-grafik">
    		<div id="jdl-gr">
    			<img src="<?php echo base_url();?>../assets/icon/grafik.svg">
    			<p>Pencarian Grafik</p>
    		</div>
    		<form action="" method="POST">
    		<div id="dlm-gr">
    			<p>Berdasarkan : </p>
    			<p>	
    				<select name="ber">
    					<option value="id_jenis" <?php if($this->input->post('ber') == 'id_jenis'){ echo "selected"; }else{} ?>>Jenis Tes</option>
    					<option value="kelas" <?php if($this->input->post('ber') == 'kelas'){ echo "selected"; }else{} ?>>Kelas</option>
    					<option value="id_rombel" <?php if($this->input->post('ber') == 'id_rombel'){ echo "selected"; }else{} ?>>Rombel</option>
    				</select>
    			</p>
    			<p>
    				<input type="text" name="cri" placeholder="Pencarian" required value="<?php echo $this->input->post('cri');?>">
    			</p>
    			<p><input type="submit" name="cari" value="Cari"></p>
    		</form>
    		</div>
    		
    	</div>		
 </div>