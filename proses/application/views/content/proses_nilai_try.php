<div id="tb">
	<table>
		<tr>
			<td width="190px">Mapel : 
				<select id="mapelID">
					<?php
						$sl = $this->db->get("mapel_try")->result();
						foreach ($sl as $mpl) {
					?>
						<option value="<?php echo $mpl->id_try;?>"><?php echo $mpl->mapel;?></option>
					<?php
						}
					?>
				</select>
			</td>
			<td width="90px">Paket : 
				<select id="paket">
					<option>Latihan</option>
					<option>1</option>
					<option>2</option>
					<option>3</option>
					<option>4</option>
				</select>
			</td>
			<td>
				<button class="proses" onclick="proses_nilai()" style="cursor:pointer">
					<p>Proses Nilai</p>
				</button>
			</td>
			<td></td>
		</tr>
	</table>
	<div id="pmbr"></div>
	<div id="pem" hidden><p>Data telah berhasil di simpan.</p></div>
	<center>
		<button class="backup" onclick="coba()" hidden><p>BackUp</p></button>
	</center>
</div>
<div class="bg-lg" hidden>
	<div id="tenga">
		<div id="load">
			<div id="load2">
				<div id="load3"></div>
			</div>
		</div>
		<p>Memproses data ... </p>
	</div> 
</div>
<script type="text/javascript">
	
	function coba(){
		$(".bg-lg").fadeIn(100);
		$.ajax({
			url:'<?php echo site_url('nilait/sim_nilai');?>',
			type:"POST",
			data:{
				
			},
			success:function(data){
				if(data == "Berhasil"){
					$("#pem").fadeIn(1);
					$(".bg-lg").fadeOut(1);
					$(".backup").fadeOut(1);
					$(".hps").fadeIn(1);
				}else{
					alert("Gagal BackUp Data");
					$(".bg-lg").fadeIn(100);
				}
			}
		})
	}
	function proses_nilai(){
		$.ajax({
			url:"../../../admin/tryo/proses_nilai",
			type:"POST",
			data:{
				mapelID : $("#mapelID").val(),
				paket : $("#paket").val()
			},
			success:function(data){
				if(data == 0){
					$(".backup").fadeOut(10);
				}else{
					$(".backup").fadeIn(10);

				}
				$("#pmbr").html("<h1>Jumlah data :" +data+"</h1>");
			}
		})
	}
</script>