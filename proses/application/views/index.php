<?php
	if($this->session->userdata('level') == "Guru"){
		$gur = "";
		$ad  = "hidden";
		$pen = "hidden";
		$id_gur = $this->session->userdata("id");
		$qwyi = $this->model_admin->qw("tryout_guru","WHERE id_guru = '$id_gur'")->num_rows();
		if($qwyi == 0){
			$maptry = "hidden";
		}else{
			$maptry="";
		}
	}elseif($this->session->userdata('level') == "Pengawas"){
		$gur = "hidden";
		$ad  = "hidden";
		$pen = "";
		$maptry = "hidden";
	}else{
		$gur = "hidden";
		$ad  = "";
		$pen = "hidden";
		$maptry = "hidden";
	}
?>
<?php
	$tyu = $this->model_admin->qw("pengaturan","WHERE jenis = 'Rekomendasi'")->row_array();
	if($tyu['status'] == "off"){
		$rekom = "hidden";
	}else{
		$rekom = "";
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title;?></title>
</head>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>../assets/css/admin/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>../assets/css/admin/backup.css">
<body>
<div id="header">
	<div id="kanan">
		<p><?php echo $this->session->userdata("nama");?></p>
		<a href="<?php echo site_url('../admin/login/keluar');?>" title="Keluar">
			<img src="<?php echo base_url();?>../assets/icon/power.svg">
		</a>
	</div>
</div>
<div id="menu">
	<div id="logo">
		<img src="<?php echo base_url();?>../assets/icon/logo-afas.png">
	</div>
	<div id="userlogin">
		<img src="<?php echo base_url();?>../assets/icon/user.svg">
		<h2><?php echo $this->session->userdata('nama');?></h2>
		<p><?php echo $this->session->userdata('level');?></p>
	</div>
	<ul>
		<a href="<?php echo base_url();?>../admin/admin/halad/beranda" title="Beranda"><li>Beranda</li></a>
		<a href="<?php echo base_url();?>proses/halad/proses_nilai" title="Proses Data Nilai"><li>Proses Data Nilai</li></a>
		<a href="<?php echo base_url();?>proses/halad/proses_nilai_try" title="Proses Data Nilai Tryout"><li>Proses Data Nilai Tryout</li></a>
	</ul>

</div>
<div id="content">
	<?php
		include('content/'.$lod.'.php');
	?>
</div>
<script type="text/javascript" src="<?php echo base_url();?>../assets/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>../assets/ckeditor/ckeditor.js"></script>
<?php include "../assets/js/function.php";?>

</body>
</html>