<?php
	Class Admin extends CI_COntroller{
		function __construct(){
			parent::__construct();
			$this->load->model("model_admin");
        	$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
			if($this->session->userdata("status_ad") == "login"){
				
			}else{
				redirect("login/form_login");
			}
		}
		function grafik(){
			$data['report'] = $this->model_admin->report();
        	$this->load->view('grafik', $data);
		}
		function semua_analisis_tryout(){
			$kl = $this->session->userdata('id');
			$data['var'] = $this->model_admin->qw("tryout_guru,mapel_try","WHERE mapel_try.id_try = tryout_guru.id_mapel AND tryout_guru.id_guru = '$kl'")->result();
			$data['rbl'] = $this->model_admin->tampil_data("rombel")->result();
			$this->load->view("semua_analisis_tryout",$data);
		}
		function semua_analisis(){
			$kl = $this->session->userdata('id');
			$data['var'] = $this->model_admin->qw("mapel_guru,mapel","WHERE mapel.id_mapel = mapel_guru.id_mapel AND mapel_guru.id_guru = '$kl'")->result();
			$data['jns'] = $this->model_admin->tampil_data("jenis_tes")->result();
			$data['rbl'] = $this->model_admin->tampil_data("rombel")->result();
			$this->load->view("semua_analisis",$data);
		}
		function lihat_soal(){
			$urs = $this->uri->segment(3);
			if(empty($urs)){
				$data['pel'] = "";
				$kl = $this->session->userdata('id');
				$data['var'] = $this->model_admin->qw("mapel_guru,mapel","WHERE mapel.id_mapel = mapel_guru.id_mapel AND mapel_guru.id_guru = '$kl'")->result();
				$data['jns'] = $this->model_admin->tampil_data("jenis_tes")->result();
				$data['soal'] = "";
				$this->load->view("lihat_soal", $data);
			}else{
				$id_map = $this->input->post("pelajaran");
				$kelas = $this->input->post("kelas");
				$jenis = $this->input->post("jen");
				$dtm = $this->model_admin->qw("mapel","WHERE id_mapel = '$id_map'")->row_array();
				$data['pel'] = $dtm['mapel'];
				$kl = $this->session->userdata('id');
				$data['var'] = $this->model_admin->qw("mapel_guru,mapel","WHERE mapel.id_mapel = mapel_guru.id_mapel AND mapel_guru.id_guru = '$kl'")->result();
				$data['jns'] = $this->model_admin->tampil_data("jenis_tes")->result();
				$data['soal'] = $this->model_admin->qw("soal","WHERE kelas = '$kelas' AND id_mapel = '$id_map' AND jenis_tes = '$jenis'")->result();
				$this->load->view("lihat_soal", $data);
			}
		}
		function lihat_soala(){
			$urs = $this->uri->segment(3);
			if(empty($urs)){
				$data['pel'] = "";
				$kl = $this->session->userdata('id');
				$data['var'] = $this->model_admin->qw("mapel_guru,mapel","WHERE mapel.id_mapel = mapel_guru.id_mapel AND mapel_guru.id_guru = '$kl'")->result();
				$data['vara'] = $this->db->get("mapel")->result();
				$data['jns'] = $this->model_admin->tampil_data("jenis_tes")->result();
				$data['soal'] = "";
				$this->load->view("lihat_soala", $data);
			}else{
				$id_map = $this->input->post("pelajaran");
				$kelas = $this->input->post("kelas");
				$jenis = $this->input->post("jen");
				$dtm = $this->model_admin->qw("mapel","WHERE id_mapel = '$id_map'")->row_array();
				$data['pel'] = $dtm['mapel'];
				$kl = $this->session->userdata('id');
				$data['var'] = $this->model_admin->qw("mapel_guru,mapel","WHERE mapel.id_mapel = mapel_guru.id_mapel AND mapel_guru.id_guru = '$kl'")->result();
				$data['vara'] = $this->db->get("mapel")->result();
				$data['jns'] = $this->model_admin->tampil_data("jenis_tes")->result();
				$data['soal'] = $this->model_admin->qw("soal","WHERE kelas = '$kelas' AND id_mapel = '$id_map' AND jenis_tes = '$jenis'")->result();
				$this->load->view("lihat_soala", $data);
			}
		}
		function lihat_soal_tryout(){
			$urs = $this->uri->segment(3);
			if(empty($urs)){
				$data['pel'] = "";
				$kl = $this->session->userdata('id');
				$data['var'] = $this->model_admin->qw("tryout_guru,mapel_try","WHERE mapel_try.id_try = tryout_guru.id_mapel AND tryout_guru.id_guru = '$kl'")->result();
				$data['soal'] = "";
				$this->load->view("lihat_soal_try", $data);
			}else{
				$id_map = $this->input->post("pelajaran");
				$paket = $this->input->post("jen");
				$dtm = $this->model_admin->qw("mapel_try","WHERE id_try = '$id_map'")->row_array();
				$data['pel'] = $dtm['mapel'];
				$kl = $this->session->userdata('id');
				$data['var'] = $this->model_admin->qw("tryout_guru,mapel_try","WHERE mapel_try.id_try = tryout_guru.id_mapel AND tryout_guru.id_guru = '$kl'")->result();
				$data['soal'] = $this->model_admin->qw("soal_tryout","WHERE id_mapel = '$id_map' AND paket = '$paket'")->result();
				$this->load->view("lihat_soal_try", $data);
			}
		}
		function lihatdetail(){
			$nis = $this->uri->segment(3);
			$id_mapel = $this->uri->segment(4);
			$id_tes = $this->uri->segment(5);
			$data['soal'] = $this->model_admin->qw("jawaban2,soal","WHERE jawaban2.nis = '$nis' AND jawaban2.id_mapel = '$id_mapel' AND jawaban2.id_jenis = '$id_tes' AND jawaban2.id_soal = soal.id_soal")->result();
			$this->load->view("detail_nilai",$data);
		}
		function lihatdetail_tryout(){
			$nis = $this->uri->segment(3);
			$id_mapel = $this->uri->segment(4);
			$id_tes = $this->uri->segment(5);
			$data['soal'] = $this->model_admin->qw("jawaban_tryout,soal_tryout","WHERE jawaban_tryout.nis = '$nis' AND jawaban_tryout.id_mapel = '$id_mapel' AND jawaban_tryout.paket = '$id_tes' AND jawaban_tryout.id_soal = soal_tryout.id_soal")->result();
			$this->load->view("detail_nilai_tryout",$data);
		}
		function halad($hal){
			if($hal == "siswa"){
				$data['title'] 	= "Welcome";
				$data['lod']	= $hal;
				$urs = $this->uri->segment(4);
				$x = str_replace("%20", " ", $urs);
				$sel = $this->model_admin->qw("rombel","WHERE rombel = '$x'")->row_array();
				$id_rom = $sel['id_rombel'];
				if(empty($urs)){
					$data['hasil']	= $this->model_admin->qw("siswa,jurusan, rombel","WHERE siswa.id_jurusan = jurusan.id_jurusan AND siswa.id_rombel = rombel.id_rombel AND siswa.kelas = 'X'")->result();
				}else{
					$data['hasil']	= $this->model_admin->qw("siswa,jurusan,rombel","WHERE nis like '%$urs%' AND siswa.id_jurusan = jurusan.id_jurusan AND siswa.id_rombel = rombel.id_rombel OR nama like '%$urs%' AND siswa.id_jurusan = jurusan.id_jurusan AND siswa.id_rombel = rombel.id_rombel OR alamat like '%$urs%' AND siswa.id_jurusan = jurusan.id_jurusan AND siswa.id_rombel = rombel.id_rombel OR kelas like '%$urs%' AND siswa.id_jurusan = jurusan.id_jurusan AND siswa.id_rombel = rombel.id_rombel OR siswa.id_rombel = '$id_rom' AND siswa.id_jurusan = jurusan.id_jurusan AND siswa.id_rombel = rombel.id_rombel GROUP BY nis")->result();
				}
				$this->load->view("index",$data);
			}elseif($hal == "i_siswa"){
				$data['title'] 	= "Welcome";
				$data['lod']	= $hal;
				$xx	= $this->uri->segment(4);
				if(empty($xx)){
					$data['status'] = "simpan";
					$data['open'] = "admin/simpan_siswa";
					$data['value'] = "Simpan";
					$data['jurusan'] = $this->model_admin->tampil_data("jurusan")->result();
					$data['rombel'] = $this->model_admin->tampil_data("rombel")->result();
				}else{
					$data['status'] = "edit";
					$data['value'] = "Edit";
					$data['open'] = "admin/edit_siswa";
					$data['rombel'] = $this->model_admin->tampil_data("rombel")->result();
					$data['hsl'] = $this->model_admin->qw("siswa,jurusan","WHERE siswa.id_jurusan = jurusan.id_jurusan AND siswa.nis = '$xx'");
					$data['jurusan'] = $this->model_admin->tampil_data("jurusan")->result();
				}
				$this->load->view("index",$data);
			}elseif($hal == "beranda"){
				$data['title'] 	= "Welcome";
				$data['lod']	= $hal;
				$data['jmlsiswa']= $this->model_admin->tampil_data("siswa")->num_rows();
				$data['jmljurusan']= $this->model_admin->tampil_data("jurusan")->num_rows();
				$data['jmlrom']= $this->model_admin->tampil_data("rombel")->num_rows();
				$data['jmlguru']= $this->model_admin->qw("guru","WHERE level = 'Guru'")->num_rows();
				$data['qw_jen'] = $this->model_admin->tampil_data("jenis_tes")->result();
				$data['qw_mapel'] = $this->model_admin->tampil_data("mapel")->result();
				$this->load->view("index",$data);
			}elseif($hal == "guru_try"){
					$data['guru'] = $this->model_admin->qw("tryout_guru,guru,mapel_try","WHERE tryout_guru.id_guru = guru.id_guru AND tryout_guru.id_mapel = mapel_try.id_try AND guru.level = 'Guru'")->result();
					$data['title'] 	= "Welcome";
					$data['lod']	= $hal;
					$this->load->view("index",$data);
			}elseif($hal == "guru"){
					$data['guru'] = $this->model_admin->qw("guru","WHERE level = 'Guru'")->result();
					$data['title'] 	= "Welcome";
					$data['lod']	= $hal;
					$this->load->view("index",$data);
			}elseif($hal == "jenis_tes"){
					$data['jns'] = $this->model_admin->tampil_data("jenis_tes")->result();
					$data['title'] 	= "Welcome";
					$data['lod']	= $hal;
					$this->load->view("index",$data);
			}elseif($hal == "jurusan"){
					$data['jns'] = $this->model_admin->tampil_data("jurusan")->result();
					$data['title'] 	= "Welcome";
					$data['lod']	= $hal;
					$this->load->view("index",$data);
			}elseif($hal == "rombel"){
					$data['jns'] = $this->model_admin->tampil_data("rombel")->result();
					$data['title'] 	= "Welcome";
					$data['lod']	= $hal;
					$this->load->view("index",$data);
			}elseif($hal == "mapel"){
					$data['jns'] = $this->model_admin->tampil_data("mapel")->result();
					$data['title'] 	= "Welcome";
					$data['lod']	= $hal;
					$this->load->view("index",$data);
			}elseif($hal == "identitas"){
					$data['identitas'] = $this->model_admin->tampil_data("identitas")->result();
					$data['title'] 	= "Welcome";
					$data['lod']	= $hal;
					$this->load->view("index",$data);
			}elseif($hal == "mapel_tryout"){
					$data['jns'] = $this->model_admin->tampil_data("mapel_try")->result();
					$data['title'] 	= "Welcome";
					$data['lod']	= $hal;
					$this->load->view("index",$data);
			}elseif($hal == "soal"){
					$im = $this->input->post('id_mapel');
					$jt = $this->input->post('id_tes');
					$kls = $this->input->post('kelas');
					if(empty($im)){
						$data['jns'] = $this->model_admin->qw("soal,mapel,jenis_tes","WHERE soal.id_mapel = mapel.id_mapel AND soal.jenis_tes = jenis_tes.id_tes AND soal.id_mapel = '$im'")->result();
						$kl = $this->session->userdata('id');
						$data['mapel'] = $this->model_admin->qw("mapel_guru,mapel","WHERE mapel.id_mapel = mapel_guru.id_mapel AND mapel_guru.id_guru = '$kl'")->result();
					}else{
						$data['jns'] = $this->model_admin->qw("soal,mapel,jenis_tes","WHERE soal.id_mapel = mapel.id_mapel AND soal.jenis_tes = jenis_tes.id_tes AND soal.id_mapel='$im' AND soal.jenis_tes = '$jt' AND soal.kelas= '$kls'")->result();
						$kl = $this->session->userdata('id');
						$data['mapel'] = $this->model_admin->qw("mapel_guru,mapel","WHERE mapel.id_mapel = mapel_guru.id_mapel AND mapel_guru.id_guru = '$kl'")->result();
					}
					$data['jnsx'] = $this->model_admin->tampil_data("jenis_tes")->result();
					$data['title'] 	= "Welcome";
					$data['lod']	= $hal;
					$this->load->view("index",$data);
			}elseif($hal == "soal_try"){
					$im = $this->input->post('id_mapel');
					$jt = $this->input->post('paket');
					if(empty($im)){
						$data['jns'] = $this->model_admin->qw("soal_tryout,mapel_try","WHERE soal_tryout.id_mapel = mapel_try.id_try AND soal_tryout.id_mapel = '$im'")->result();
						$kl = $this->session->userdata('id');
						$data['mapel'] = $this->model_admin->qw("tryout_guru,mapel_try","WHERE mapel_try.id_try = tryout_guru.id_mapel AND tryout_guru.id_guru = '$kl'")->result();
					}else{
						$data['jns'] = $this->model_admin->qw("soal_tryout,mapel_try","WHERE soal_tryout.id_mapel = mapel_try.id_try AND soal_tryout.id_mapel='$im' AND soal_tryout.paket = '$jt'")->result();
						$kl = $this->session->userdata('id');
						$data['mapel'] = $this->model_admin->qw("tryout_guru,mapel_try","WHERE mapel_try.id_try = tryout_guru.id_mapel AND tryout_guru.id_guru = '$kl'")->result();
					}
					$data['jnsx'] = $this->model_admin->tampil_data("jenis_tes")->result();
					$data['title'] 	= "Welcome";
					$data['lod']	= $hal;
					$this->load->view("index",$data);
			}elseif($hal == "i_guru"){
				$data['title'] 	= "Welcome";
				$data['lod']	= $hal;
				$xx	= $this->uri->segment(4);
				if(empty($xx)){
					$dta = $this->model_admin->qw("guru","ORDER BY id_guru DESC")->row_array();
					if(empty($dta['id_guru'])){
						$data['id'] = 1;
					}else{
						$data['id'] = $dta['id_guru']+1;
					}
					$data['status'] = "simpan";
					$data['open'] = "admin/simpan_guru";
					$data['value'] = "Simpan";
					$data['mapel'] = $this->model_admin->tampil_data("mapel")->result();
					$data['tmapel'] = $this->model_admin->qw("mapel_guru,mapel,guru","WHERE guru.id_guru = mapel_guru.id_guru AND mapel.id_mapel = mapel_guru.id_mapel AND guru.id_guru = '$data[id]'")->result();
				}else{
					$data['status'] = "edit";
					$data['value'] = "Edit";
					$data['id'] = $xx;
					$data['open'] = "admin/edit_guru";
					$data['hsl'] = $this->model_admin->qw("guru","WHERE id_guru = '$xx'");
					$data['mapel'] = $this->model_admin->tampil_data("mapel")->result();
					$data['tmapel'] = $this->model_admin->qw("mapel_guru,mapel,guru","WHERE guru.id_guru = mapel_guru.id_guru AND mapel.id_mapel = mapel_guru.id_mapel AND guru.id_guru = '$xx'")->result();
				}
				$this->session->set_userdata(array("id_guru"=>$data['id']));
				$this->load->view("index",$data);
			}elseif($hal == "edit_guru"){
				$data['title'] 	= "Welcome";
				$data['lod']	= $hal;
				$xx	= $this->uri->segment(4);
					$data['status'] = "edit";
					$data['value'] = "Edit";
					$data['id'] = $xx;
					$data['open'] = "admin/editg";
					$data['hsl'] = $this->model_admin->qw("guru","WHERE id_guru = '$xx'");
				$this->session->set_userdata(array("id_guru"=>$data['id']));
				$this->load->view("index",$data);
			}elseif($hal == "i_guru_try"){
				$data['title'] 	= "Welcome";
				$data['lod']	= $hal;
				$xx	= $this->uri->segment(4);
				if(empty($xx)){
					$data['guru'] = $this->model_admin->qw("guru","WHERE level = 'Guru'")->result();
					$data['mapel'] = $this->model_admin->qw("mapel_try","")->result();
					$data['status'] = "simpan";
					$data['open'] = "admin/sim_ad_try";
					$data['value'] = "Simpan";
					$data['id'] = "";
				}else{
					$data['status'] = "edit";
					$data['value'] = "Edit";
					$data['id'] = $xx;
					$data['open'] = "admin/edit_guru_try";
					$data['guru'] = $this->model_admin->qw("guru","WHERE level = 'Guru'")->result();
					$data['mapel'] = $this->model_admin->qw("mapel_try","")->result();
					$data['hsl'] = $this->model_admin->qw("tryout_guru","WHERE id_tguru = '$xx'");
				}
				$this->load->view("index",$data);
			}elseif($hal == "i_jenis_tes"){
				$data['title'] 	= "Welcome";
				$data['lod']	= $hal;
				$xx	= $this->uri->segment(4);
				if(empty($xx)){
					$data['status'] = "simpan";
					$data['open'] = "admin/sim_jenis";
					$data['value'] = "Simpan";
				}else{
					$data['status'] = "edit";
					$data['value'] = "Edit";
					$data['open'] = "admin/edit_jenis";
					$data['hsl'] = $this->model_admin->qw("jenis_tes","WHERE id_tes = '$xx'");
				}
				$this->load->view("index",$data);
			}elseif($hal == "i_soal"){
				$data['title'] 	= "Welcome";
				$data['lod']	= $hal;
				$xx	= $this->uri->segment(4);
				if(empty($xx)){
					$data['status'] = "simpan";
					$data['open'] = "admin/sim_soal";
					$data['value'] = "Simpan";
				}else{
					$data['status'] = "edit";
					$data['value'] = "Edit";
					$data['open'] = "admin/edit_soal";
					$data['hsl'] = $this->model_admin->qw("soal","WHERE id_soal = '$xx'");
				}
				$this->load->view("index",$data);
			}elseif($hal == "i_soal_try"){
				$data['title'] 	= "Welcome";
				$data['lod']	= $hal;
				$xx	= $this->uri->segment(4);
				if(empty($xx)){
					$data['status'] = "simpan";
					$data['open'] = "admin/sim_soal_try";
					$data['value'] = "Simpan";
				}else{
					$data['status'] = "edit";
					$data['value'] = "Edit";
					$data['open'] = "admin/edit_soal_try";
					$data['hsl'] = $this->model_admin->qw("soal_tryout","WHERE id_soal = '$xx'");
				}
				$this->load->view("index",$data);
			}elseif($hal == "i_jurusan"){
				$data['title'] 	= "Welcome";
				$data['lod']	= $hal;
				$xx	= $this->uri->segment(4);
				if(empty($xx)){
					$data['status'] = "simpan";
					$data['open'] = "admin/sim_jurusan";
					$data['value'] = "Simpan";
				}else{
					$data['status'] = "edit";
					$data['value'] = "Edit";
					$data['open'] = "admin/edit_jurusan";
					$data['hsl'] = $this->model_admin->qw("jurusan","WHERE id_jurusan = '$xx'");
				}
				$this->load->view("index",$data);
			}elseif($hal == "i_rombel"){
				$data['title'] 	= "Welcome";
				$data['lod']	= $hal;
				$xx	= $this->uri->segment(4);
				if(empty($xx)){
					$data['status'] = "simpan";
					$data['open'] = "admin/sim_rombel";
					$data['value'] = "Simpan";
				}else{
					$data['status'] = "edit";
					$data['value'] = "Edit";
					$data['open'] = "admin/edit_rombel";
					$data['hsl'] = $this->model_admin->qw("rombel","WHERE id_rombel = '$xx'");
				}
				$this->load->view("index",$data);
			}elseif($hal == "i_mapel"){
				$data['title'] 	= "Welcome";
				$data['lod']	= $hal;
				$xx	= $this->uri->segment(4);
				if(empty($xx)){
					$data['status'] = "simpan";
					$data['open'] = "admin/sim_mapel";
					$data['value'] = "Simpan";
				}else{
					$data['status'] = "edit";
					$data['value'] = "Edit";
					$data['open'] = "admin/edit_mapel";
					$data['hsl'] = $this->model_admin->qw("mapel","WHERE id_mapel = '$xx'");
				}
				$this->load->view("index",$data);
			}elseif($hal == "i_mapel_try"){
				$data['title'] 	= "Welcome";
				$data['lod']	= $hal;
				$xx	= $this->uri->segment(4);
				if(empty($xx)){
					$data['status'] = "simpan";
					$data['open'] = "admin/sim_mapel_try";
					$data['value'] = "Simpan";
				}else{
					$data['status'] = "edit";
					$data['value'] = "Edit";
					$data['open'] = "admin/edit_mapel_try";
					$data['hsl'] = $this->model_admin->qw("mapel_try","WHERE id_try = '$xx'");
				}
				$this->load->view("index",$data);
			}elseif($hal == "i_mapel_try"){
				$data['title'] 	= "Welcome";
				$data['lod']	= $hal;
				$xx	= $this->uri->segment(4);
				if(empty($xx)){
					$data['status'] = "simpan";
					$data['open'] = "admin/sim_mapel_try";
					$data['value'] = "Simpan";
				}else{
					$data['status'] = "edit";
					$data['value'] = "Edit";
					$data['open'] = "admin/edit_mapel_try";
					$data['hsl'] = $this->model_admin->qw("mapel_try","WHERE id_try = '$xx'");
				}
				$this->load->view("index",$data);
			}elseif($hal == "i_identitas"){
				$data['title'] 	= "Welcome";
				$data['lod']	= $hal;
				$xx	= $this->uri->segment(4);
				if(empty($xx)){
					$data['status'] = "simpan";
					$data['open'] = "admin/sim_identitas";
					$data['value'] = "Simpan";
				}else{
					$data['status'] = "edit";
					$data['value'] = "Edit";
					$data['open'] = "admin/edit_identitas";
					$data['hsl'] = $this->model_admin->qw("identitas","WHERE id_identitas = '$xx'");
				}
				$this->load->view("index",$data);
			}elseif($hal == "status"){
				$data['title'] 	= "Welcome";
				$data['lod']	= $hal;
				$urs = $this->uri->segment(4);
				$x = str_replace("%20", " ", $urs);
				$sel = $this->model_admin->qw("rombel","WHERE rombel = '$x'")->row_array();
				$id_rom = $sel['id_rombel'];
				if(empty($urs)){
					$data['hasil']	= $this->model_admin->qw("siswa,jurusan,rombel","WHERE siswa.id_jurusan = jurusan.id_jurusan AND siswa.id_rombel = rombel.id_rombel AND siswa.kelas = 'X'")->result();
				}else{
					$data['hasil']	= $this->model_admin->qw("siswa,jurusan,rombel","WHERE nis like '%$urs%' AND siswa.id_jurusan = jurusan.id_jurusan AND siswa.id_rombel = rombel.id_rombel OR nama like '%$urs%' AND siswa.id_jurusan = jurusan.id_jurusan AND siswa.id_rombel = rombel.id_rombel OR alamat like '%$urs%' AND siswa.id_jurusan = jurusan.id_jurusan AND siswa.id_rombel = rombel.id_rombel OR kelas like '%$urs%' AND siswa.id_jurusan = jurusan.id_jurusan AND siswa.id_rombel = rombel.id_rombel OR siswa.id_rombel = '$id_rom' AND siswa.id_jurusan = jurusan.id_jurusan AND siswa.id_rombel = rombel.id_rombel GROUP BY nis")->result();
				}
				$this->load->view("index",$data);
			}elseif($hal == "rekomen"){
				$data['title'] 	= "Welcome";
				$data['lod']	= $hal;
				$urs = $this->input->post("id_rombel");
				if(empty($urs)){
					$data['hasil']	= $this->model_admin->qw("siswa,jurusan, rombel","WHERE siswa.id_jurusan = jurusan.id_jurusan AND siswa.id_rombel = rombel.id_rombel AND siswa.kelas = 'X'")->result();
				}else{
					$data['hasil']	= $this->model_admin->qw("siswa,jurusan,rombel","WHERE siswa.id_jurusan = jurusan.id_jurusan AND siswa.id_rombel = rombel.id_rombel AND siswa.id_rombel = '$urs' GROUP BY nis")->result();
				}
				$kl = $this->session->userdata('id');
				$data['rombel'] = $this->model_admin->qw("rombel","ORDER BY rombel ASC")->result();
				$data['mapel'] = $this->model_admin->qw("mapel_guru,mapel","WHERE mapel.id_mapel = mapel_guru.id_mapel AND mapel_guru.id_guru = '$kl'")->result();
				$data['jns'] = $this->model_admin->tampil_data("jenis_tes")->result(); 
				$this->load->view("index",$data);
			}elseif($hal == "cek_rekom"){
				$data['title'] 	= "Welcome";
				$data['lod']	= $hal;
				$urs = $this->uri->segment(4);
				$x = str_replace("%20", " ", $urs);
				$sel = $this->model_admin->qw("rombel","WHERE rombel = '$x'")->row_array();
				$id_rom = $sel['id_rombel'];
				if(empty($urs)){
					$data['hasil']	= $this->model_admin->qw("siswa,jurusan,rombel","WHERE siswa.id_jurusan = jurusan.id_jurusan AND siswa.id_rombel = rombel.id_rombel")->result();
				}else{
					$data['hasil']	= $this->model_admin->qw("siswa,jurusan,rombel","WHERE nis like '%$urs%' AND siswa.id_jurusan = jurusan.id_jurusan AND siswa.id_rombel = rombel.id_rombel OR nama like '%$urs%' AND siswa.id_jurusan = jurusan.id_jurusan AND siswa.id_rombel = rombel.id_rombel OR alamat like '%$urs%' AND siswa.id_jurusan = jurusan.id_jurusan AND siswa.id_rombel = rombel.id_rombel OR kelas like '%$urs%' AND siswa.id_jurusan = jurusan.id_jurusan AND siswa.id_rombel = rombel.id_rombel OR siswa.id_rombel = '$id_rom' AND siswa.id_jurusan = jurusan.id_jurusan AND siswa.id_rombel = rombel.id_rombel GROUP BY nis")->result();
				}
				$kl = $this->session->userdata('id');
				$data['rmbl'] = $this->model_admin->tampil_data("rombel")->result();
				$data['mapel'] = $this->model_admin->tampil_data("mapel")->result();
				$data['jns'] = $this->model_admin->tampil_data("jenis_tes")->result(); 
				$this->load->view("index",$data);
			}elseif($hal == "absen"){
				$data['title'] 	= "Welcome";
				$data['lod']	= $hal;
				$urs = $this->input->post("id_rombel");
				if(empty($urs)){
					$data['hasil']	= $this->model_admin->qw("siswa,jurusan, rombel","WHERE siswa.id_jurusan = jurusan.id_jurusan AND siswa.id_rombel = rombel.id_rombel AND siswa.kelas = 'X'")->result();
				}else{
					$data['hasil']	= $this->model_admin->qw("siswa,jurusan,rombel","WHERE siswa.id_jurusan = jurusan.id_jurusan AND siswa.id_rombel = rombel.id_rombel AND siswa.id_rombel = '$urs' GROUP BY nis")->result();
				}
				$kl = $this->session->userdata('id');
				$data['rombel'] = $this->model_admin->qw("rombel","ORDER BY rombel ASC")->result();
				$data['mapel'] = $this->model_admin->tampil_data("mapel")->result();
				$data['jns'] = $this->model_admin->tampil_data("jenis_tes")->result(); 
				$this->load->view("index",$data);
			}elseif($hal == "pengaturan"){
				$data['title'] 	= "Welcome";
				$data['lod']	= $hal;
				$data['png'] = $this->model_admin->tampil_data("pengaturan")->result();
				$data['jenis'] = $this->model_admin->tampil_data("jenis_tes")->result();
				$data['mapel'] = $this->model_admin->tampil_data("mapel")->result();
				$this->load->view("index",$data);
			}elseif($hal == "p_mapel"){
				$im = $this->input->post('id_mapel');
				$jt = $this->input->post('id_tes');
				$kls = $this->input->post('kelas');
				if(empty($im)){
					$data['jns'] = $this->model_admin->qw("nilai,siswa,rombel","WHERE nilai.id_mapel = '$im' AND nilai.id_jenis = '$jt' AND nilai.kelas = '$kls' ORDER BY nilai.nilai DESC")->result();
					$kl = $this->session->userdata('id');
					$data['mapel'] = $this->model_admin->qw("mapel_guru,mapel","WHERE mapel.id_mapel = mapel_guru.id_mapel AND mapel_guru.id_guru = '$kl'")->result();
				}else{
					$data['jns'] = $this->model_admin->qw("nilai,siswa,rombel","WHERE nilai.id_mapel = '$im' AND nilai.id_jenis = '$jt' AND nilai.kelas = '$kls' AND nilai.nis = siswa.nis AND nilai.id_rombel = rombel.id_rombel ORDER BY nilai.nilai DESC")->result();
					$kl = $this->session->userdata('id');
					$data['mapel'] = $this->model_admin->qw("mapel_guru,mapel","WHERE mapel.id_mapel = mapel_guru.id_mapel AND mapel_guru.id_guru = '$kl'")->result();
				}
				$data['jnsx'] = $this->model_admin->tampil_data("jenis_tes")->result();
				$data['title'] 	= "Welcome";
				$data['lod']	= $hal;
				$this->load->view("index",$data);
			}elseif($hal == "k_nil"){
				$im = $this->input->post('id_mapel');
				$jt = $this->input->post('id_tes');
				$kls = $this->input->post('kelas');
				if(empty($im)){
					$cc = $this->model_admin->qw("nilai,siswa,rombel","WHERE nilai.id_mapel = '$im' AND nilai.id_jenis = '$jt' AND nilai.kelas = '$kls'");
					$data['jns'] = $cc->result();
					$data['jml'] = $cc->num_rows();
					$kl = $this->session->userdata('id');
					$data['mapel'] = $this->model_admin->qw("mapel_guru,mapel","WHERE mapel.id_mapel = mapel_guru.id_mapel AND mapel_guru.id_guru = '$kl'")->result();
				}else{
					$cc = $this->model_admin->qw("nilai,siswa,rombel","WHERE nilai.id_mapel = '$im' AND nilai.id_jenis = '$jt' AND nilai.id_rombel = '$kls' AND nilai.nis = siswa.nis AND nilai.id_rombel = rombel.id_rombel ORDER BY siswa.nama ASC");
					$data['jns'] = $cc->result();
					$data['jml'] = $cc->num_rows();
					$kl = $this->session->userdata('id');
					$data['mapel'] = $this->model_admin->qw("mapel_guru,mapel","WHERE mapel.id_mapel = mapel_guru.id_mapel AND mapel_guru.id_guru = '$kl'")->result();
				}
				$data['jnsx'] = $this->model_admin->tampil_data("jenis_tes")->result();
				$data['rom'] = $this->model_admin->tampil_data("rombel")->result();
				$data['title'] 	= "Welcome";
				$data['lod']	= $hal;
				$this->load->view("index",$data);
			}elseif($hal == "nilai_tryout"){
				$im = $this->input->post('id_mapel');
				$pkt = $this->input->post('id_tes');
				$kls = $this->input->post('kelas');
				if(empty($im)){
					$cc = $this->model_admin->qw("nilai_tryout,siswa,rombel","WHERE nilai_tryout.id_mapel = '$im' AND nilai_tryout.paket = '$pkt'");
					$data['jns'] = $cc->result();
					$data['jml'] = $cc->num_rows();
					$kl = $this->session->userdata('id');
					$data['mapel'] = $this->model_admin->qw("tryout_guru,mapel_try","WHERE mapel_try.id_try = tryout_guru.id_mapel AND tryout_guru.id_guru = '$kl'")->result();
				}else{
					$cc = $this->model_admin->qw("nilai_tryout,siswa,rombel","WHERE nilai_tryout.id_mapel = '$im' AND nilai_tryout.paket = '$pkt' AND nilai_tryout.id_rombel = '$kls' AND nilai_tryout.nis = siswa.nis AND nilai_tryout.id_rombel = rombel.id_rombel");
					$data['jns'] = $cc->result();
					$data['jml'] = $cc->num_rows();
					$kl = $this->session->userdata('id');
					$data['mapel'] = $this->model_admin->qw("tryout_guru,mapel_try","WHERE mapel_try.id_try = tryout_guru.id_mapel AND tryout_guru.id_guru = '$kl'")->result();
				}
				$data['jnsx'] = $this->model_admin->tampil_data("jenis_tes")->result();
				$data['rom'] = $this->model_admin->tampil_data("rombel")->result();
				$data['title'] 	= "Welcome";
				$data['lod']	= $hal;
				$this->load->view("index",$data);
			}else{
				$data['title'] 	= "Welcome";
				$data['lod']	= $hal;
				$this->load->view("index",$data);
			}
		}
		function simpan_guru(){
			$dta = $this->model_admin->qw("guru","ORDER BY id_guru DESC")->row_array();
			if(empty($dta['id_guru'])){
				$id = 1;
			}else{
				$id = $dta['id_guru']+1;
			}
			$ar = array(
				"id_guru" => $id,
				"nama"	  => $this->input->post("nama"),
				"pas"	  => base64_encode($this->input->post("pas")),
				"alamat"  => $this->input->post("alamat"),
				"level"	  => "Guru"
			);
			$this->db->insert("guru",$ar);
			redirect("admin/halad/guru");
		}
		function edit_guru(){
			$ar = array(
				"nama"	  => $this->input->post("nama"),
				"pas"	  => base64_encode($this->input->post("pas")),
				"alamat"  => $this->input->post("alamat"),
				"level"	  => "Guru"
			);
			$id = $this->input->post("id");
			$this->db->where("id_guru",$id);
			$this->db->update("guru", $ar);
			redirect("admin/halad/guru");
		}
		function edit_guru_try(){
			$ar = array(
				"id_guru"	  => $this->input->post("id_guru"),
				"id_mapel"	  => $this->input->post("id_mapel")
			);
			$id = $this->input->post("id");
			$this->db->where("id_tguru",$id);
			$this->db->update("tryout_guru", $ar);
			redirect("admin/halad/guru_try");
		}
		function editg(){
			$ar = array(
				"nama"	  => $this->input->post("nama"),
				"pas"	  => base64_encode($this->input->post("pas")),
				"alamat"  => $this->input->post("alamat"),
				"level"	  => "Guru"
			);
			$id = $this->input->post("id");
			$this->db->where("id_guru",$id);
			$this->db->update("guru", $ar);
			redirect("admin/halad/edit_guru/".$this->session->userdata('id'));
		}
		function tam_map(){
			$this->load->view("gru.php");
		}
		function tam_map_try(){
			$this->load->view("gru_try.php");
		}
		function sim_ad(){
			$id_guru = $this->input->post("id_guru");
			$id_mapel = $this->input->post("id_mapel");
			$sim = array(
				"id_mpg" => "",
				"id_guru"=> $id_guru,
				"id_mapel"=> $id_mapel
			);
			$this->session->set_userdata(array("id_guru"=>$id_guru));
			$this->db->insert("mapel_guru", $sim);
		}
		function sim_ad_try(){
			$id_guru = $this->input->post("id_guru");
			$id_mapel = $this->input->post("id_mapel");
			$sim = array(
				"id_tguru" => "",
				"id_guru"=> $id_guru,
				"id_mapel"=> $id_mapel
			);
			$this->session->set_userdata(array("id_guru"=>$id_guru));
			$this->session->set_userdata(array("id_guru"=>$id_guru));
			$this->db->insert("tryout_guru", $sim);
			redirect("admin/halad/guru_try");
		}
		function sim_hap(){
			$id_mpg = $this->input->post("id_mpg");
			$this->db->where("id_mpg",$id_mpg);
			$this->db->delete("mapel_guru");
		}
		function sim_hap_try($id_mpg){
			$this->db->where("id_tguru",$id_mpg);
			$this->db->delete("tryout_guru");
			redirect("admin/halad/guru_try");
		}
		function simpan_identitas(){
			$frm = array(
				"logo"=>$this->input->post('ogo'),
				"nama"=>$this->input->post('nama'),
				"alamat"=>$this->input->post('alamat'),
				"tgl"=>$this->input->post('tgl'),
				"kelas"=>$this->input->post('kelas'),
				"id_rombel"=>$this->input->post('id_rombel'),
				"id_jurusan"=>$this->input->post('id_jurusan'),
				"status"=>"off"
			);
			$this->db->insert("siswa",$frm);
			redirect("admin/halad/siswa");
		}
		function simpan_siswa(){
			$frm = array(
				"nis"=>$this->input->post('nis'),
				"nama"=>$this->input->post('nama'),
				"alamat"=>$this->input->post('alamat'),
				"tgl"=>$this->input->post('tgl'),
				"kelas"=>$this->input->post('kelas'),
				"id_rombel"=>$this->input->post('id_rombel'),
				"id_jurusan"=>$this->input->post('id_jurusan'),
				"status"=>"off"
			);
			$this->db->insert("siswa",$frm);
			redirect("admin/halad/siswa");
		}
		function edit_siswa(){
			$frm = array(
				"nis"=>$this->input->post('nis'),
				"nama"=>$this->input->post('nama'),
				"alamat"=>$this->input->post('alamat'),
				"tgl"=>$this->input->post('tgl'),
				"kelas"=>$this->input->post('kelas'),
				"id_rombel"=>$this->input->post('id_rombel'),
				"id_jurusan"=>$this->input->post('id_jurusan'),
				"status"=>"off"
			);
			$id=$this->input->post('id');
			$this->db->where("nis",$id);
			$this->db->update("siswa",$frm);
			redirect("admin/halad/siswa");
		}
		function hapus_siswa($nis){
			$this->db->where("nis",$nis);
			$this->db->delete("siswa");
			redirect("admin/halad/siswa");
		}
		function hapus_identitas($id){
			$this->db->where('id_identitas',$id);
			$query=$this->db->query("SELECT * FROM identitas");
			$row=$query->row();
			unlink("../assets/logo/$row->logo");
			$this->db->delete("identitas");
			redirect("admin/halad/identitas");
		}
		function hapus_guru($id_gur){
			$this->db->where("id_guru",$id_gur);
			$this->db->delete("guru");
			$this->db->where("id_guru",$id_gur);
			$this->db->delete("mapel_guru");
			redirect("admin/halad/guru");
		}
		function sim_jenis(){
			$ambl = $this->model_admin->qw("jenis_tes","ORDER BY id_tes DESC")->row_array();
			if(empty($ambl['id_tes'])){
				$id_tes = "IT001";
			}else{
				$xx = substr($ambl['id_tes'], 4, 20) + 1;
				$id_tes = "IT00".$xx;
			}
			$frm = array(
				"id_tes" => $id_tes,
				"jenis_tes" => $this->input->post("jenis"),
				"tanggal_awal" => $this->input->post("tanggal_mulai"),
				"tanggal_akhir" => $this->input->post("tanggal_selesai"),
				"jam" => $this->input->post("jam"),
				"menit" => $this->input->post("menit")
			);
			$this->db->insert("jenis_tes",$frm);
			redirect("admin/halad/jenis_tes");
		}
		function edit_jenis(){
			$frm = array(
				"jenis_tes" => $this->input->post("jenis"),
				"tanggal_awal" => $this->input->post("tanggal_mulai"),
				"tanggal_akhir" => $this->input->post("tanggal_selesai"),
				"jam" => $this->input->post("jam"),
				"menit" => $this->input->post("menit")
			);
			$id = $this->input->post("id_jenis");
			$this->db->where("id_tes",$id);
			$this->db->update("jenis_tes",$frm);
			redirect("admin/halad/jenis_tes");
		}
		function hapus_jenis($aa){
			$this->db->where("id_tes", $aa);
			$this->db->delete("jenis_tes");
			redirect("admin/halad/jenis_tes");
		}
		function sim_jurusan(){
			$ambl = $this->model_admin->qw("jurusan","ORDER BY id_jurusan DESC")->row_array();
			if(empty($ambl['id_jurusan'])){
				$id_tes = "IJ001";
			}else{
				$xx = substr($ambl['id_jurusan'], 3, 20) + 1;
				if($xx >= 10){
					$id_tes = "IJ0".$xx;
				}else{
					$id_tes = "IJ00".$xx;	
				}
				
			}
			$frm = array(
				"id_jurusan" => $id_tes,
				"jurusan" => $this->input->post("jurusan")
			);
			$this->db->insert("jurusan",$frm);
			redirect("admin/halad/jurusan");
		}
		function sim_rombel(){
			$ambl = $this->model_admin->qw("rombel","ORDER BY id_rombel DESC")->row_array();
			if(empty($ambl['id_rombel'])){
				$id_rom = "IR001";
			}else{
				$xx = substr($ambl['id_rombel'], 3, 20) + 1;
				if($xx >= 10){
					$id_rom = "IR0".$xx;
				}else{
					$id_rom = "IR00".$xx;	
				}
				
			}
			$frm = array(
				"id_rombel" => $id_rom,
				"rombel" => $this->input->post("rombel")
			);
			$this->db->insert("rombel",$frm);
			redirect("admin/halad/rombel");
		}
		function edit_jurusan(){
			$frm = array(
				"jurusan" => $this->input->post("jurusan")
			);
			$id = $this->input->post("id_jurusan");
			$this->db->where("id_jurusan",$id);
			$this->db->update("jurusan",$frm);
			redirect("admin/halad/jurusan");
		}
		function edit_rombel(){
			$frm = array(
				"rombel" => $this->input->post("rombel")
			);
			$id = $this->input->post("id_rombel");
			$this->db->where("id_rombel",$id);
			$this->db->update("rombel",$frm);
			redirect("admin/halad/rombel");
		}
		function hapus_jurusan($aa){
			$this->db->where("id_jurusan", $aa);
			$this->db->delete("jurusan");
			redirect("admin/halad/jurusan");
		}
		function hapus_rombel($aa){
			$this->db->where("id_rombel", $aa);
			$this->db->delete("rombel");
			redirect("admin/halad/rombel");
		}

		function sim_mapel(){
			$ambl = $this->model_admin->qw("mapel","ORDER BY id_mapel DESC")->row_array();
			if(empty($ambl['id_mapel'])){
				$id_tes = "IM001";
			}else{
				$xx = substr($ambl['id_mapel'], 3, 20) + 1;
				if($xx > 9){
					$id_tes = "IM0".$xx;
				}else{
					$id_tes = "IM00".$xx;
				}
			}
			$frm = array(
				"id_mapel" => $id_tes,
				"mapel" => $this->input->post("mapel"),
				"jumlah_soal" => $this->input->post("jumlah_soal"),
				"kkm1" => $this->input->post("kkm1"),
				"kkm2" => $this->input->post("kkm2"),
				"kkm3" => $this->input->post("kkm3")
			);
			$this->db->insert("mapel",$frm);
			redirect("admin/halad/mapel");
		}
		function edit_mapel(){
			$frm = array(
				"mapel" => $this->input->post("mapel"),
				"jumlah_soal" => $this->input->post("jumlah_soal"),
				"kkm1" => $this->input->post("kkm1"),
				"kkm2" => $this->input->post("kkm2"),
				"kkm3" => $this->input->post("kkm3")
			);
			$id = $this->input->post("id_mapel");
			$this->db->where("id_mapel",$id);
			$this->db->update("mapel",$frm);
			redirect("admin/halad/mapel");
		}
		function hapus_mapel($aa){
			$this->db->where("id_mapel", $aa);
			$this->db->delete("mapel");
			redirect("admin/halad/mapel");
		}
		function sim_mapel_try(){
			$ambl = $this->model_admin->qw("mapel_try","ORDER BY id_try DESC")->row_array();
			if(empty($ambl['id_try'])){
				$id_tes = "MT001";
			}else{
				$xx = substr($ambl['id_try'], 3, 20) + 1;
				if($xx > 9){
					$id_tes = "MT0".$xx;
				}else{
					$id_tes = "MT00".$xx;
				}
			}
			$frm = array(
				"id_try" => $id_tes,
				"mapel" => $this->input->post("mapel"),
				"jumlah_soal" => $this->input->post("jumlah_soal"),
				"kkm" => $this->input->post("kkm")
			);
			$this->db->insert("mapel_try",$frm);
			redirect("admin/halad/mapel_tryout");
		}
		function edit_mapel_try(){
			$frm = array(
				"mapel" => $this->input->post("mapel"),
				"jumlah_soal" => $this->input->post("jumlah_soal"),
				"kkm" => $this->input->post("kkm")
			);
			$id = $this->input->post("id_mapel");
			$this->db->where("id_try",$id);
			$this->db->update("mapel_try",$frm);
			redirect("admin/halad/mapel_tryout");
		}
		function hapus_mapel_try($aa){
			$this->db->where("id_try", $aa);
			$this->db->delete("mapel_try");
			redirect("admin/halad/mapel_tryout");
		}
		function sim_soal(){
			$filename = 'list';
			$config['upload_path'] = '../assets/listening/';
			$config['allowed_types'] = 'mp3|wav';
			$config['max_size'] = '1000';
			$this->load->library('upload',$config);

			$data['listening'] = array('file_name' => '');


			if( ! $this->upload->do_upload($filename)){ $this->upload->display_errors(); }else{ $data['listening'] = $this->upload->data();}

			$ary = array(
				"id_soal" => "",
				"soal"    => $this->input->post("soal"),
				"kunci"   => $this->input->post("kunci"),
				"kelas"   => $this->input->post("kelas"),
				"id_mapel"=> $this->input->post("id_mapel"),
				"jenis_tes"=> $this->input->post("jenis"),
				"listening"=> $data['listening']['file_name']
			);
			$this->db->insert("soal",$ary);
			redirect("admin/halad/soal");
		}
		function sim_soal_try(){
			$filename = 'list';
			$config['upload_path'] = '../assets/listening/';
			$config['allowed_types'] = 'mp3|wav';
			$config['max_size'] = '500';
			$this->load->library('upload',$config);

			$data['listening'] = array('file_name' => '');


			if( ! $this->upload->do_upload($filename)){ $this->upload->display_errors(); }else{ $data['listening'] = $this->upload->data();}

			$ary = array(
				"id_soal" => "",
				"soal"    => $this->input->post("soal"),
				"kunci"   => $this->input->post("kunci"),
				"id_mapel"=> $this->input->post("id_mapel"),
				"paket"=> $this->input->post("paket"),
				"listening"=> $data['listening']['file_name']
			);
			$this->db->insert("soal_tryout",$ary);
			redirect("admin/halad/soal_try");
		}
		function edit_soal(){
			$id_sol = $this->input->post("id_soal");
			$dtasol = $this->model_admin->qw("soal","WHERE id_soal = '$id_sol'")->row_array();
			$filename = 'list';
			$config['upload_path'] = '../assets/listening/';
			$config['allowed_types'] = 'mp3|wav';
			$config['max_size'] = '1000';
			$this->load->library('upload',$config);
			
			$data['listening'] = array('file_name' => '');

			if( ! $this->upload->do_upload($filename)){$this->upload->display_errors();}else{$data['listening'] = $this->upload->data();}
			if($data['listening']['file_name'] == ""){$data['listening']['file_name'] = $dtasol['listening'];}else{}
			$ary = array(
				"soal"    => $this->input->post("soal"),
				"kunci"   => $this->input->post("kunci"),
				"kelas"   => $this->input->post("kelas"),
				"id_mapel"=> $this->input->post("id_mapel"),
				"jenis_tes"=> $this->input->post("jenis"),
				"listening"=> $data['listening']['file_name']
			);
			$this->db->where("id_soal", $id_sol);
			$this->db->update("soal",$ary);
			redirect("admin/halad/soal");
		}
		function edit_soal_try(){
			$id_sol = $this->input->post("id_soal");
			$dtasol = $this->model_admin->qw("soal_tryout","WHERE id_soal = '$id_sol'")->row_array();
			$filename = 'list';
			$config['upload_path'] = '../assets/listening/';
			$config['allowed_types'] = 'mp3|wav';
			$config['max_size'] = '500';
			$this->load->library('upload',$config);
			
			$data['listening'] = array('file_name' => '');

			if( ! $this->upload->do_upload($filename)){$this->upload->display_errors();}else{$data['listening'] = $this->upload->data();}
			if($data['listening']['file_name'] == ""){$data['listening']['file_name'] = $dtasol['listening'];}else{}
			$ary = array(
				"soal"    => $this->input->post("soal"),
				"kunci"   => $this->input->post("kunci"),
				"id_mapel"=> $this->input->post("id_mapel"),
				"paket"=> $this->input->post("paket"),
				"listening"=> $data['listening']['file_name']
			);
			$this->db->where("id_soal", $id_sol);
			$this->db->update("soal_tryout",$ary);
			redirect("admin/halad/soal_try");
		}
		function sim_identitas(){
			$config['upload_path']='../assets/logo';
			$config['allowed_types']='gif|jpg|png';
			$config['max_size']='3000';
			$config['encrypt_name']=true;
			$this->load->library('upload',$config);
			$this->upload->do_upload('logo');
			$data=$this->upload->data();
			$gambar=$data['file_name'];
				$ary=array(
					'id_identitas'	=> "",
					'logo'	=> $gambar,
					'nama_sekolah'	=> $this->input->post('nama_sekolah'),
					'sasaran'	=>$this->input->post('sasaran'),
					'motto'	=>$this->input->post('motto')
				);
				$this->db->insert('identitas',$ary);
				redirect("admin/halad/identitas");
		}
		function edit_identitas(){
			$filename=$_FILES['logo']['name'];
			if(empty($filename)){
			$id=$this->input->post('id_identitas');
				$ary=array(
					'nama_sekolah'	=> $this->input->post('nama_sekolah'),
					'sasaran'	=>$this->input->post('sasaran'),
					'motto'	=>$this->input->post('motto')
				);
				$this->db->where('id_identitas',$id);
				$this->db->update('identitas',$ary);
				redirect("admin/halad/identitas");
			}else{
				$config['upload_path']='../assets/logo';
				$config['allowed_types']='gif|jpg|png';
				$config['max_size']='3000';
				$config['encrypt_name']=true;
				$this->load->library('upload',$config);
				$this->upload->do_upload('logo');
				$data=$this->upload->data();
				$gambar=$data['file_name'];
				$id=$this->input->post('id_identitas');
				$this->db->where('id_identitas',$id);
				$query=$this->db->query("SELECT * FROM identitas");
				$row=$query->row();
				unlink("../assets/logo/$row->logo");
				$ary=array(
					'logo'	=>$gambar,
					'nama_sekolah'	=> $this->input->post('nama_sekolah'),
					'sasaran'	=>$this->input->post('sasaran'),
					'motto'	=>$this->input->post('motto')
				);
				$this->db->where('id_identitas',$id);
				$this->db->update('identitas',$ary);
				redirect("admin/halad/identitas");
			}
		}
		function hapus_soal($aa){
			$this->db->where("id_soal", $aa);
			$this->db->delete("soal");
			redirect("admin/halad/soal");
		}
		function hapus_soal_try($aa){
			$this->db->where("id_soal", $aa);
			$this->db->delete("soal_tryout");
			redirect("admin/halad/soal_try");
		}
		function hapus_nilai($aa,$bb,$cc){
			$this->db->where(array("nis"=>$aa,"id_mapel"=>$bb,"id_jenis"=>$cc));
			$this->db->delete("nilai");
			$this->db->where(array("nis"=>$aa,"id_mapel"=>$bb,"id_jenis"=>$cc));
			$this->db->delete("jawaban");
			redirect("admin/halad/k_nil");
		}
		function hapus_nilai_tryout($aa,$bb,$cc){
			$this->db->where(array("nis"=>$aa,"id_mapel"=>$bb,"paket"=>$cc));
			$this->db->delete("nilai_tryout");
			$this->db->where(array("nis"=>$aa,"id_mapel"=>$bb,"paket"=>$cc));
			$this->db->delete("jawaban_tryout");
			redirect("admin/halad/nilai_tryout");
		}
		function reset($x){
			$this->db->where("nis",$x);
			$this->db->update("siswa",array("status"=>"off"));
			redirect("admin/halad/status");
		}
		function rekom(){
			$nis = $this->input->post('nis');
			$id_mapel = $this->input->post('id_map');
			$id_jenis = $this->input->post('id_jenis');
			$jml = $this->model_admin->qw("rekomendasi","WHERE nis = '$nis' AND id_mapel = '$id_mapel' AND id_jenis = '$id_jenis'")->num_rows();
			if($jml == 0){
				$dtar = array(
					"id_rekom"=>'',
					"nis" => $nis,
					"id_mapel" => $id_mapel,
					"id_jenis" => $id_jenis
				);
				$this->db->insert('rekomendasi',$dtar);
			}else{

			}
		}
		function hrekom(){
			$nis = $this->input->post('nis');
			$id_mapel = $this->input->post('id_map');
			$id_jenis = $this->input->post('id_jenis');
			$this->model_admin->hapus('rekomendasi',"nis = '$nis' AND id_mapel = '$id_mapel' AND id_jenis = '$id_jenis'");
		}
		function absen(){
			$nis = $this->input->post('nis');
			$id_mapel = $this->input->post('id_map');
			$id_jenis = $this->input->post('id_jenis');
			$nama_guru = $this->input->post('nama_guru');
			$jml = $this->model_admin->qw("absen","WHERE nis = '$nis' AND id_mapel = '$id_mapel' AND id_jenis = '$id_jenis'")->num_rows();
			if($jml == 0){
				$dtar = array(
					"no"=>'',
					"nis" => $nis,
					"id_jenis" => $id_jenis,
					"id_mapel" => $id_mapel,
					"nama_guru" => $nama_guru
				);
				$this->db->insert('absen',$dtar);
			}else{

			}
		}
		function habsen(){
			$nis = $this->input->post('nis');
			$id_mapel = $this->input->post('id_map');
			$id_jenis = $this->input->post('id_jenis');
			$this->model_admin->hapus('absen',"nis = '$nis' AND id_mapel = '$id_mapel' AND id_jenis = '$id_jenis'");
		}
		function edit_peng($x){
			$ax = $this->model_admin->qw("pengaturan","WHERE id_peng = '$x'")->row_array();
			if($ax['status'] == "on"){
				$this->db->where("id_peng","$x");
				$this->db->update("pengaturan",array("status"=>'off'));
				redirect("admin/halad/pengaturan");
			}else{
				$this->db->where("id_peng","$x");
				$this->db->update("pengaturan",array("status"=>'on'));
				redirect("admin/halad/pengaturan");
			}
		}
		function rset(){
			$qw = $this->model_admin->tampil_data("siswa")->result();
			foreach ($qw as $d) {
				$this->db->where("nis", $d->nis);
				$this->db->update("siswa",array("status"=>"off"));
			}
			redirect('admin/halad/status');
		}
		function lihat_nilai(){
			$urs = $this->uri->segment(3);
			if(empty($urs)){
				$data['pel'] = "";
				$kl = $this->session->userdata('id');
				$data['var'] = $this->model_admin->qw("mapel_guru,mapel","WHERE mapel.id_mapel = mapel_guru.id_mapel AND mapel_guru.id_guru = '$kl'")->result();
				$data['jns'] = $this->model_admin->tampil_data("jenis_tes")->result();
				$data['soal'] = "";
				$data['hid'] = "";
				$data['print'] = "";
				$data['rombel'] = $this->model_admin->qw("rombel","ORDER BY rombel ASC")->result();
				$this->load->view("lihat_nilai", $data);
			}else{
				$id_map = $this->input->post("pelajaran");
				$kelas = $this->input->post("kelas");
				$jenis = $this->input->post("jen");
				$dtm = $this->model_admin->qw("mapel","WHERE id_mapel = '$id_map'")->row_array();
				$data['pel'] = $dtm['mapel'];
				$kl = $this->session->userdata('id');
				$data['rombel'] = $this->model_admin->qw("rombel","ORDER BY rombel ASC")->result();
				$data['var'] = $this->model_admin->qw("mapel_guru,mapel","WHERE mapel.id_mapel = mapel_guru.id_mapel AND mapel_guru.id_guru = '$kl'")->result();
				$data['jns'] = $this->model_admin->tampil_data("jenis_tes")->result();
				$data['soal'] = $this->model_admin->qw("soal","WHERE kelas = '$kelas' AND id_mapel = '$id_map' AND jenis_tes = '$jenis'")->result();
				$data['hid'] = "hidden";
				$data['print'] = "window.print()";
				$this->load->view("lihat_nilai", $data);
			}
		}
		function lihat_nilai_tryout(){
			$urs = $this->uri->segment(3);
			if(empty($urs)){
				$data['pel'] = "";
				$kl = $this->session->userdata('id');
				$data['var'] = $this->model_admin->qw("tryout_guru,mapel_try","WHERE mapel_try.id_try = tryout_guru.id_mapel AND tryout_guru.id_guru = '$kl'")->result();
				$data['soal'] = "";
				$data['hid'] = "";
				$data['print'] = "";
				$data['rombel'] = $this->model_admin->qw("rombel","ORDER BY rombel ASC")->result();
				$this->load->view("lihat_nilai_tryout", $data);
			}else{
				$id_map = $this->input->post("pelajaran");
				$kelas = $this->input->post("kelas");
				$jenis = $this->input->post("jen");
				$dtm = $this->model_admin->qw("mapel_try","WHERE id_try = '$id_map'")->row_array();
				$data['pel'] = $dtm['mapel'];
				$kl = $this->session->userdata('id');
				$data['rombel'] = $this->model_admin->qw("rombel","ORDER BY rombel ASC")->result();
				$data['var'] = $this->model_admin->qw("mapel_guru,mapel","WHERE mapel.id_mapel = mapel_guru.id_mapel AND mapel_guru.id_guru = '$kl'")->result();
				$data['jns'] = $this->model_admin->tampil_data("jenis_tes")->result();
				$data['soal'] = $this->model_admin->qw("soal","WHERE kelas = '$kelas' AND id_mapel = '$id_map' AND jenis_tes = '$jenis'")->result();
				$data['hid'] = "hidden";
				$data['print'] = "window.print()";
				$this->load->view("lihat_nilai_tryout", $data);
			}
		}

		function upload_sis(){
			$fileName = $_FILES['file']['name'];
         
	        $config['upload_path'] = '../assets/'; //buat folder dengan nama assets di root folder
	        $config['file_name'] = $fileName;
	        $config['allowed_types'] = 'xls|xlsx|csv';
	        $config['max_size'] = 10000;
	         
	        $this->load->library('upload',$config);
	         
	        if(! $this->upload->do_upload('file') )
	        $this->upload->display_errors();
	             
	        $media = $this->upload->data();
	        $inputFileName = '../assets/'.$media['file_name'];
	         
	        try {
	                $inputFileType = IOFactory::identify($inputFileName);
	                $objReader = IOFactory::createReader($inputFileType);
	                $objPHPExcel = $objReader->load($inputFileName);
	            } catch(Exception $e) {
	                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
	            }
	 
	            $sheet = $objPHPExcel->getSheet(0);
	            $highestRow = $sheet->getHighestRow();
	            $highestColumn = $sheet->getHighestColumn();
	            $x= 0;
	            for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
	                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
	                                                NULL,
	                                                TRUE,
	                                                FALSE);
	                                                 
	                if(empty($rowData[0][0])){
	                	break;
	                }else{                    
	                 $data = array(
	                    "nis"=> $rowData[0][0],
	                    "nama"=> $rowData[0][1],
	                    "alamat"=> $rowData[0][2],
	                    "tgl"=> $rowData[0][3],
	                    "kelas"=> $rowData[0][4],
	                    "id_jurusan"=> $rowData[0][5],
	                    "id_rombel"=> $rowData[0][6],
	                    "status"=> $rowData[0][7]
	                );
	                $selsis = $this->model_admin->qw("siswa","WHERE nis = '$data[nis]'")->num_rows();
	                if($selsis == 0){	                 
	                	$insert = $this->db->insert("siswa",$data);
	                }else{
	                	$this->db->where("nis",$data['nis']);
	                	$update = $this->db->update("siswa", $data);
	                }
	            }
	                     
	            }
	            unlink("../assets/".$media['file_name']);
	            redirect('admin/halad/siswa');
	    	}
	    	//AfasExam Version 3.0

	    	function jumlah_soal($id_mapel){
	    		$xx = $this->uri->segment(4);
	    		if(empty($xx)){
		    		$qwne = $this->model_admin->qw("mapel","WHERE id_mapel = '$id_mapel'")->row_array();
		    		$data['pel'] = $qwne['mapel'];
		    		$data['jsoal'] = $qwne['jumlah_soal'];
		    		$data['jns'] = $this->model_admin->tampil_data("jenis_tes")->result();
		    		$this->load->view("jumlah_soal", $data);
	    		}else{
	    			$qwne = $this->model_admin->qw("mapel","WHERE id_mapel = '$id_mapel'")->row_array();
		    		$data['pel'] = $qwne['mapel'];
		    		$data['jsoal'] = $qwne['jumlah_soal'];
		    		$data['jns'] = $this->model_admin->tampil_data("jenis_tes")->result();
		    		$this->load->view("jumlah_soal", $data);
	    		}
	    	}

	    	function reset_waktu(){
	    		$jenis = $this->input->post("jenis");
	    		$mapel = $this->input->post("mapel");
	    		$nis = $this->input->post("nis");
	    		$this->db->where(array('id_jenis'=>$jenis, 'id_mapel'=>$mapel, 'nis'=>$nis));
	    		$this->db->delete("waktu");
	            redirect('admin/halad/beranda');
	    	}

	    	function aktif_soal(){
	    		$jenis = $this->input->post("jenis");
	    		$mapel = $this->input->post("mapel");
	    		$kelas = $this->input->post("kelas");
	    		$jnis_a= $this->input->post("ja");
	    		if($jnis_a == "kaktv"){
	    			$this->db->from("soal_aktif");
	    			$this->db->truncate();
	    			$qwsoal = $this->model_admin->qw("soal","WHERE jenis_tes='$jenis' AND id_mapel='$mapel' AND kelas='$kelas'")->result();
	    			foreach ($qwsoal as $dsoal) {
	    				$ary = array(
	    					"id_soal" => $dsoal->id_soal,
	    					"soal" => $dsoal->soal,
	    					"kunci"=> $dsoal->kunci,
	    					"kelas"=> $dsoal->kelas,
	    					"id_mapel"=> $dsoal->id_mapel,
	    					"jenis_tes"=> $dsoal->jenis_tes,
	    					"listening"=> $dsoal->listening
	    				);
	    				$this->db->insert("soal_aktif",$ary);
	    			}
	    			redirect('admin/halad/pengaturan');
	    		}elseif($jnis_a == "aktv"){
	    			$qwsoal = $this->model_admin->qw("soal","WHERE jenis_tes='$jenis' AND id_mapel='$mapel' AND kelas='$kelas'")->result();
	    			foreach ($qwsoal as $dsoal) {
	    				$ary = array(
	    					"id_soal" => $dsoal->id_soal,
	    					"soal" => $dsoal->soal,
	    					"kunci"=> $dsoal->kunci,
	    					"kelas"=> $dsoal->kelas,
	    					"id_mapel"=> $dsoal->id_mapel,
	    					"jenis_tes"=> $dsoal->jenis_tes,
	    					"listening"=> $dsoal->listening
	    				);
	    				$this->db->insert("soal_aktif",$ary);
	    			}
	    			redirect('admin/halad/pengaturan');
	    		}
	    	}

	    	function soal_aktiv(){
	    		$this->load->view("soal_aktiv");
	    	}
		}
?>