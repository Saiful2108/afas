<?php
	Class Export extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model("model_admin");
		}
		function siswa(){
			$data['sis'] = $this->model_admin->qw("siswa","ORDER BY nis ASC")->result();
			$this->load->view("export", $data);
		}
		function rombel(){
			$im = $this->uri->segment(3);
			$jt = $this->uri->segment(4);
			$kls = $this->uri->segment(5);
			$cc = $this->model_admin->qw("nilai,siswa,rombel","WHERE nilai.id_mapel = '$im' AND nilai.id_jenis = '$jt' AND nilai.id_rombel = '$kls' AND nilai.nis = siswa.nis AND nilai.id_rombel = rombel.id_rombel ORDER BY siswa.nama ASC");
			$data['jns'] = $cc->result();
			$this->db->select("rombel");
			$this->db->where("id_rombel",$kls);
			$rombel = $this->db->get("rombel")->row_array();
			$data['rombel'] = $rombel['rombel'];
			$this->load->view("export_nilai",$data);
		}
		function nilai_tryout(){
			$im = $this->uri->segment(3);
			$pkt = $this->uri->segment(4);
			$kls = $this->uri->segment(5);
			$cc = $this->model_admin->qw("nilai_tryout,siswa,rombel","WHERE nilai_tryout.id_mapel = '$im' AND nilai_tryout.paket = '$pkt' AND nilai_tryout.id_rombel = '$kls' AND nilai_tryout.nis = siswa.nis AND nilai_tryout.id_rombel = rombel.id_rombel ORDER BY siswa.nama ASC");
			$data['jns'] = $cc->result();
			$this->db->select("rombel");
			$this->db->where("id_rombel",$kls);
			$rombel = $this->db->get("rombel")->row_array();
			$data['rombel'] = $rombel['rombel'];
			$this->load->view("export_tryout",$data);
		}
	}

?>