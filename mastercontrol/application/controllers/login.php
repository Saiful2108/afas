<?php	
	Class Login extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model("model_login");
		}
		function form_login(){
			if($this->session->userdata("status_ad") == "login"){
				redirect("admin/halad/beranda");
			}else{

			}
			$data['title'] = "Login Administrator";
			$this->load->view("login",$data);
		}
		function proses_login(){
			$usr = $this->input->post('usr');
			$pas = $this->input->post('pass');
			$ax = $this->model_login->row_login($usr,$pas);
			$jml = $ax->num_rows();
			$hsl = $ax->row_array();
			if($jml == 0){
				redirect("login/form_login");
			}else{
				$dt = array(
					"id" => $hsl['id_guru'],
					"nama"=> $hsl['nama'],
					"level"=> $hsl['level'],
					"status_ad"=>"login"
				);
				$this->session->set_userdata($dt);
				redirect("admin/halad/beranda");
			}
		}
		function keluar(){
			session_destroy();
			redirect("login/form_login");
		}
	}

?>