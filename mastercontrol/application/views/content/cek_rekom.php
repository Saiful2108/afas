<div class="top">
	<div class="kiri"><p>Data Rekomendasi Siswa</p></div> 
	<?php
		$is = $this->uri->segment(4);
		$isi = str_replace("%20", " ", $is)
	?>
	<div class="kanan"><input type="text" placeholder="Search . . ." id="cek_rekom" value="<?php echo $isi;?>" onkeyup="car('cek_rekom')"></div>
	<div id="clear"></div>
</div>
<div class="bawah">
	<table>
		<tr>
			<td style="border-bottom:none;" width="200px">Mata Pelajaran :</td>
			<td style="border-bottom:none;" width="200px">Jenis Tes:</td>
			<td style="border-bottom:none;" width="200px">Rombel:</td>
			<td style="border-bottom:none;"></td>
		</tr>
		<tr>
			<?php echo form_open();?>
			<td style="border-bottom:none;">
				<select name="id_mapel" id="id_mapel">
					<?php
						foreach ($mapel as $dtng) {
					?>
						<option value="<?php echo $dtng->id_mapel;?>" <?php if($this->input->post('id_mapel') == $dtng->id_mapel){ echo "selected";}?>><?php echo $dtng->mapel;?></option>
					<?php } ?>
				</select>
			</td>
			<td style="border-bottom:none;">
				<select name="id_tes" id="id_tes">
					<?php
						foreach ($jns as $dtng) {
					?>
						<option value="<?php echo $dtng->id_tes;?>" <?php if($this->input->post('id_tes') == $dtng->id_tes){ echo "selected";}?>><?php echo $dtng->jenis_tes;?></option>
					<?php } ?>
				</select>
			</td>
			<td style="border-bottom:none;">
				<input type="submit" name="cari" id="car" value="Tampilkan">
			</td>
			<?php echo form_close();?>
		</tr>
	</table>
	<div id="tabel">	
		<table>
			<tr>
				<th>NIS</th>
				<th>Nama</th>
				<th>Alamat</th>
				<th>Tanggal Lahir</th>
				<th>Kelas</th>
				<th>Jurusan</th>
				<th>Rombel</th>
				<th>Status</th>
			</tr>
			<?php
				$x = 0;
				foreach ($hasil as $val) { $x++;
			?>
			<tr>
				<td><?php echo $val->nis;?></td>
				<td><?php echo $val->nama;?></td>
				<td><?php echo $val->alamat;?></td>
				<td><?php echo $val->tgl;?></td>
				<td><?php echo $val->kelas;?></td>
				<td><?php echo $val->jurusan;?></td>
				<td><?php echo $val->rombel;?></td>
				<td>
					<?php
						$aa = $this->input->post("id_mapel");
						$aa1 = $this->input->post("id_tes");
						$ns = $val->nis;
						if(empty($aa)){
							$tyo = "";
							$col = "red";
						}else{
							$qwjk = $this->model_admin->qw("rekomendasi","WHERE nis = '$ns' AND id_mapel = '$aa' AND id_jenis = '$aa1'")->num_rows();
							if($qwjk == 0){
								$tyo = "Belum";
								$col = "red";
							}else{
								$col = "#22313F";
								$tyo = "Sudah";
							}
						}
					?>
					<p style="color:<?php echo $col;?>;"><?php echo $tyo;?></p>
				</td>
			</tr>
			<?php } ?>
		</table>
	</div>
</div>
