<?php
		$is = $this->uri->segment(4);
		$isi = str_replace("%20", " ", $is)
?>
<div id="bxx">
	<div id="frm-im">
		<h1>Import Data Siswa</h1>
		<span onclick="cls()">X</span>
		<?php
			$open = site_url('admin/upload_sis');
		?>
		<?php echo form_open_multipart($open);?>
			<input type="file" name="file">
			<input type="submit" name="import" value="Upload File">
		<?php echo form_close();?>
	</div>
</div>
<div class="top">
	<div class="kiri"><p>Data Siswa</p> 
	<a href="<?php echo site_url('admin/halad/i_siswa');?>"><input type="button" value="+ Tambah"></a><div id="clear"></div></div> 
	<div class="kanan"><input type="text" placeholder="Search . . ." id="siswa" value="<?php echo $isi;?>" onkeyup="car('siswa')"></div>
	<div id="clear"></div>
</div>
<div class="bawah">
		<div id="exim">
			<a href="<?php echo site_url('export/siswa');?>">
			<div id="export">
				<p>Export</p>
			</div>
			</a>
			<div id="import" onclick="opn()">
				<p>Import</p>
			</div>
		</div>
	<div id="tabel">	
		<table>
			<tr>
				<th>NIS</th>
				<th>Nama</th>
				<th>Alamat</th>
				<th>Tanggal Lahir</th>
				<th>Kelas</th>
				<th>Jurusan</th>
				<th>Rombel</th>
				<th>Aksi</th>
			</tr>
			<?php
				foreach ($hasil as $val) {
			?>
			<tr>
				<td><?php echo $val->nis;?></td>
				<td><?php echo $val->nama;?></td>
				<td><?php echo $val->alamat;?></td>
				<td><?php echo $val->tgl;?></td>
				<td><?php echo $val->kelas;?></td>
				<td><?php echo $val->jurusan;?></td>
				<td><?php echo $val->rombel;?></td>
				<td>
					<a href="<?php echo site_url('admin/halad/i_siswa');?>/<?php echo $val->nis;?>" class="edit">Edit</a> | 
					<a href="../../hapus_siswa/<?php echo $val->nis;?>" class="hapus">Hapus</a>
				</td>
			</tr>
			<?php } ?>
		</table>
	</div>
</div>
<script type="text/javascript">
	function opn(){
		$("#bxx").fadeIn(100);
	}
	function cls(){
		$("#bxx").fadeOut(100);
	}
</script>