<div class="top">
	<div class="kiri"><p>Nilai Tryout</p><div id="clear"></div></div> 
	<div id="clear"></div>
</div>
<div class="bawah">
	<table>
		<tr>
			<td style="border-bottom:none;" width="200px">Mata Pelajaran :</td>
			<td style="border-bottom:none;" width="200px">Jenis Tes :</td>
			<td style="border-bottom:none;" width="200px">Kelas :</td>
			<td style="border-bottom:none;"></td>
		</tr>
		<tr>
			<?php echo form_open();?>
			<td style="border-bottom:none;">
				<select name="id_mapel" id="id_mapel">
					<?php
						foreach ($mapel as $dtng) {
					?>
						<option value="<?php echo $dtng->id_mapel;?>" <?php if($this->input->post('id_mapel') == $dtng->id_mapel){ echo "selected";}?>><?php echo $dtng->mapel;?></option>
					<?php } ?>
				</select>
			</td>
			<td style="border-bottom:none;">
				<select name="id_tes" id="id_tes">
						<?php
							for ($x=1;$x<=4;$x++) {
						?>
						<option value="<?php echo $x;?>" <?php if($this->input->post("jen") == $x){echo "selected";};?>><?php echo $x;?></option>

						<?php } ?>
				</select>
			</td>
			<td style="border-bottom:none;">
				<select name="kelas" id="kelas">
				<?php
					foreach ($rom as $darom) {
				?>
					<option value="<?php echo $darom->id_rombel;?>" <?php if($this->input->post('kelas') == $darom->id_rombel){ echo "selected";}?>><?php echo $darom->rombel;?></option>	
				<?php } ?>
				</select>
			</td>
			<td style="border-bottom:none;">
				<input type="submit" name="cari" id="car" value="Tampilkan">
			</td>
			<?php echo form_close();?>
		</tr>
	</table>
	<div id="tabel">
		<table>
			<tr>
				<th>NIS</th>
				<th>Nama</th>
				<th>Nilai</th>
				<th>Rombel</th>
				<th>Tanggal Kerja</th>
				<th>Action</th>
			</tr>
			<?php
				$x=0;
				foreach ($jns as $data) { $x++;
			?>
				<tr>
					<td><?php echo $data->nis;?></td>
					<td><?php echo $data->nama;?></td>
					<td><?php echo $data->nilai;?></td>
					<td><?php echo $data->rombel;?></td>
					<td><?php echo $data->tgl_kerja;?></td>
					<td>
						<a class="edit" href="<?php echo site_url("admin/lihatdetail_tryout");?>/<?php echo $data->nis;?>/<?php echo $this->input->post('id_mapel');?>/<?php echo $this->input->post('id_tes');?>" target="_blank">
							Lihat Detail 
						</a>
						|
						<a href="../../admin/hapus_nilai_tryout/<?php echo $data->nis;?>/<?php echo $this->input->post('id_mapel');?>/<?php echo $this->input->post('id_tes');?>" class="hapus">Hapus</a>
					</td>
				</tr>
			<?php } ?>
		</table>
	<a href="<?php echo base_url();?>admin/lihat_nilai_tryout" target="_blank">
		<button id="btn"><p>Cetak Data Nilai</p></button>
	</a>
	<?php
		if($jml > 0){
	?>
	<a href="<?php echo base_url();?>export/nilai_tryout/<?php echo $this->input->post('id_mapel');?>/<?php echo $this->input->post('id_tes');?>/<?php echo $this->input->post('kelas');?>">
		<button id="btn"><p>Cetak Nilai dengan Excel</p></button>
	</a>
	<?php }else{} ?>
	</div>
</div>