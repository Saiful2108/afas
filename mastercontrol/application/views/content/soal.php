<div class="top">
	<div class="kiri"><p>Data Soal</p> 
	<a href="<?php echo base_url();?>admin/halad/i_soal"><input type="button" value="+ Tambah"></a><div id="clear"></div></div> 
	<div id="clear"></div>
</div>
<div class="bawah">
	<table>
		<tr>
			<td style="border-bottom:none;" width="200px">Mata Pelajaran :</td>
			<td style="border-bottom:none;" width="200px">Jenis Tes :</td>
			<td style="border-bottom:none;" width="200px">Kelas :</td>
			<td style="border-bottom:none;"></td>
		</tr>
		<tr>
			<?php echo form_open();?>
			<td style="border-bottom:none;">
				<select name="id_mapel" id="id_mapel">
					<?php
						foreach ($mapel as $dtng) {
					?>
						<option value="<?php echo $dtng->id_mapel;?>" <?php if($this->input->post('id_mapel') == $dtng->id_mapel){ echo "selected";}?>><?php echo $dtng->mapel;?></option>
					<?php } ?>
				</select>
			</td>
			<td style="border-bottom:none;">
				<select name="id_tes" id="id_tes">
					<?php
						foreach ($jnsx as $dtng) {
					?>
						<option value="<?php echo $dtng->id_tes;?>" <?php if($this->input->post('id_tes') == $dtng->id_tes){ echo "selected";}?>><?php echo $dtng->jenis_tes;?></option>
					<?php } ?>
				</select>
			</td>
			<td style="border-bottom:none;">
				<select name="kelas" id="kelas">
					<option <?php if($this->input->post('kelas') == "X"){ echo "selected";}?>>X</option>
					<option <?php if($this->input->post('kelas') == "XI"){ echo "selected";}?>>XI</option>
					<option <?php if($this->input->post('kelas') == "XII"){ echo "selected";}?>>XII</option>	
				</select>
			</td>
			<td style="border-bottom:none;">
				<input type="submit" name="cari" id="car" value="Tampilkan">
			</td>
			<?php echo form_close();?>
		</tr>
	</table>
	<div id="tabel">
		<table>
			<tr>
				<th>No</th>
				<th width="500px">Soal</th>
				<th>Kelas</th>
				<th>Mapel</th>
				<th>Jenis Tes</th>
				<th>Aksi</th>
			</tr>
			<?php
				$x=0;
				foreach ($jns as $data) { $x++;
			?>
				<tr>
					<td><?php echo $x;?></td>
					<td><?php echo $data->soal;?></td>
					<td><?php echo $data->kelas;?></td>
					<td><?php echo $data->mapel;?></td>
					<td><?php echo $data->jenis_tes;?></td>
					<td>
						<a href="i_soal/<?php echo $data->id_soal;?>" class="edit">Edit</a> | 
						<a href="../../admin/hapus_soal/<?php echo $data->id_soal;?>" class="hapus">Hapus</a>
					</td>
				</tr>
			<?php } ?>
		</table>
	</div>
</div>