<div class="top">
	<div class="kiri"><p>Data Rekomendasi Siswa</p></div> 
	<?php
		$is = $this->uri->segment(4);
		$isi = str_replace("%20", " ", $is)
	?>
	<div id="clear"></div>
</div>
<div class="bawah">
	<table>
		<tr>
			<td style="border-bottom:none;" width="200px">Rombel :</td>
			<td style="border-bottom:none;" width="200px">Mata Pelajaran :</td>
			<td style="border-bottom:none;" width="200px">Jenis Tes:</td>
			<td style="border-bottom:none;"></td>
		</tr>
		<tr>
			<?php echo form_open();?>
			<td style="border-bottom:none;">
				<select name="id_rombel" id="id_romb">
					<?php
						foreach ($rombel as $dtng) {
					?>
						<option value="<?php echo $dtng->id_rombel;?>" <?php if($this->input->post('id_rombel') == $dtng->id_rombel){ echo "selected";}?>><?php echo $dtng->rombel;?></option>
					<?php } ?>
				</select>
			</td>
			<td style="border-bottom:none;">
				<select name="id_mapel" id="id_mapel">
					<?php
						foreach ($mapel as $dtng) {
					?>
						<option value="<?php echo $dtng->id_mapel;?>" <?php if($this->input->post('id_mapel') == $dtng->id_mapel){ echo "selected";}?>><?php echo $dtng->mapel;?></option>
					<?php } ?>
				</select>
			</td>
			<td style="border-bottom:none;">
				<select name="id_tes" id="id_tes">
					<?php
						foreach ($jns as $dtng) {
					?>
						<option value="<?php echo $dtng->id_tes;?>" <?php if($this->input->post('id_tes') == $dtng->id_tes){ echo "selected";}?>><?php echo $dtng->jenis_tes;?></option>
					<?php } ?>
				</select>
			</td>
			<td style="border-bottom:none;">
				<input type="submit" name="cari" id="car" value="Tampilkan">
			</td>
			<?php echo form_close();?>
		</tr>
	</table>
	<div id="tabel">	
		<table>
			<tr>
				<th>NIS</th>
				<th>Nama</th>
				<th>Alamat</th>
				<th>Tanggal Lahir</th>
				<th>Kelas</th>
				<th>Jurusan</th>
				<th>Rombel</th>
				<th><input type="checkbox" id="cxk" onclick="axc()"></th>
			</tr>
			<?php
				$x = 0;
				foreach ($hasil as $val) { $x++;
			?>
			<tr>
				<td><?php echo $val->nis;?></td>
				<td><?php echo $val->nama;?></td>
				<td><?php echo $val->alamat;?></td>
				<td><?php echo $val->tgl;?></td>
				<td><?php echo $val->kelas;?></td>
				<td><?php echo $val->jurusan;?></td>
				<td><?php echo $val->rombel;?></td>
				<td>
					<?php
						$aa = $this->input->post("id_mapel");
						$aa1 = $this->input->post("id_tes");
						$ns = $val->nis;
						if(empty($aa)){
							$tyo = "";
						}else{
							$qwjk = $this->model_admin->qw("rekomendasi","WHERE nis = '$ns' AND id_mapel = '$aa' AND id_jenis = '$aa1'")->num_rows();
							if($qwjk == 0){
								$tyo = "";
							}else{
								$tyo = "checked";
							}
						}
					?>
					<input type="checkbox" id="ckk<?php echo $x;?>" value="<?php echo $val->nis;?>" <?php echo $tyo;?>>
				</td>
			</tr>
			<?php } ?>
		</table>
	</div>
	<button onclick="sim()" id="aff">Beri Rekomendasi</button>
	<button onclick="hps()" id="afx">Hapus Rekomendasi</button>
</div>
<script type="text/javascript">
	function sim(){
		for(v = 1; v<= <?php echo $x;?>; v++){
			var b = document.getElementById('ckk'+v);
			if(b.checked == true){
				$.ajax({
					url:'<?php echo site_url('admin/rekom');?>',
					type:'POST',
					data:{
						nis: b.value,
						id_map: $("#id_mapel").val(),
						id_jenis:$("#id_tes").val()
					},
					success:function(data){
						document.location='<?php echo site_url('admin/halad/rekomen');?>';
					}
				})
			}else{

			}
		}
		alert('Berhasil Memberi Rekomendasi');
	}
	function hps(){
		for(v = 1; v<= <?php echo $x;?>; v++){
			var b = document.getElementById('ckk'+v);
			if(b.checked == false){
				$.ajax({
					url:'<?php echo site_url('admin/hrekom');?>',
					type:'POST',
					data:{
						nis: b.value,
						id_map: $("#id_mapel").val(),
						id_jenis:$("#id_tes").val()
					},
					success:function(data){
						document.location='<?php echo site_url('admin/halad/rekomen');?>';
					}
				})
			}else{

			}
		}
		alert('Berhasil Menghapus Rekomendasi');
	}
	function axc(){
		var t = document.getElementById('cxk');
		if(t.checked == true){
			for(v=1; v <= <?php echo $x;?>; v++){
				document.getElementById('ckk'+v).checked = true;
			}
		}else{
			for(v=1;v<=<?php echo $x;?>;v++){
				document.getElementById('ckk'+v).checked = false;
			}
		}
	}
</script>