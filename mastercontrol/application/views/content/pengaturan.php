<div class="top">
	<div class="kiri"><p>Pengaturan</p> </div>
	<div id="clear"></div>
</div>
<div class="bawah">
	<div id="tabel">
		<table>
			<tr>
				<th>No</th>
				<th>Jurusan</th>
				<th width="200px">Status</th>
			</tr>
			<?php
				$x=0;
				foreach ($png as $data) { $x++;
			?>
				<tr>
					<td><?php echo $x;?></td>
					<td><?php echo $data->jenis;?></td>
					<td>
						<a href="<?php echo site_url('admin/edit_peng');?>/<?php echo $data->id_peng;?>" class="hapus"><?php echo $data->status;?></a>
					</td>
				</tr>
			<?php } ?>
			<tr>
				<td>3</td>
				<td>Aktivasi Soal</td>
				<td><span class="aktv" onclick="tmpil()">Aktivasi</span> | <a href="../../admin/soal_aktiv" target="_blank"><span class="liaktv">Lihat Soal Aktif</span></a></td>
			</tr>
		</table>
	</div>
</div>
<div class="bg-all" hidden>
	<div class="bg-dlm">
		<h1>Pilih Soal Aktif !!</h1>
		<h3 onclick="cls()"> X </h3>
		<?php echo form_open('admin/aktif_soal');?>
		<p>Jenis Tes</p>
		<p>
			<select name="jenis">
				<?php
					foreach ($jenis as $dajenis) {
				?>
					<option value="<?php echo $dajenis->id_tes;?>"><?php echo $dajenis->jenis_tes;?></option>
				<?php } ?>
			</select>
		</p>
		<p>Jenis Tes</p>
		<p>
			<select name="mapel">
				<?php
					foreach ($mapel as $damapel) {
				?>
					<option value="<?php echo $damapel->id_mapel;?>"><?php echo $damapel->mapel;?></option>
				<?php } ?>
			</select>
		</p>
		<p>Kelas</p>
		<p>
			<select name="kelas">
				<option>X</option>
				<option>XI</option>
				<option>XII</option>
			</select>
		</p>
		<p>Jenis Aksi</p>
		<p>
			<select name="ja">
				<option value="aktv">Aktifkan</option>
				<option value="kaktv">Kosongkan dan Aktifkan</option>
			</select>
		</p>
		<input type="submit" value="Aktifkan" name="aktif">
		<?php form_close();?>
	</div>
</div>
<script type="text/javascript">
	function tmpil(){
		$(".bg-all").fadeIn(1);
	}
	function cls(){
		$(".bg-all").fadeOut(1);
	}
</script>