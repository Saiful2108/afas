<div class="top">
	<div class="kiri"><p>Status Siswa</p> </div>
	<?php
		$is = $this->uri->segment(4);
		$isi = str_replace("%20", " ", $is)
	?>
	<div class="kanan">
		<input type="text" placeholder="Search . . ." id="status" value="<?php echo $isi;?>" name="cari" onkeypress="car('status')">
	</div>
	<div id="clear"></div>
</div>
<div class="bawah">
	<div id="tabel">
		<table>
			<tr>
				<th>Status</th>
				<th>NIS</th>
				<th>Nama</th>
				<th>Alamat</th>
				<th>Tanggal Lahir</th>
				<th>Kelas</th>
				<th>Jurusan</th>
				<th>Rombel</th>
				<th>Aksi</th>
			</tr>
			<?php
				foreach ($hasil as $val) {
					if($val->status == "on"){
						$acv = "#FF0000";
					}else{
						$acv = "#22313F";
					}
			?>
			<tr>
				<td><h2 style="color:<?php echo $acv;?>;font-weight:normal;"><?php echo $val->status;?></h2></td>
				<td><?php echo $val->nis;?></td>
				<td><?php echo $val->nama;?></td>
				<td><?php echo $val->alamat;?></td>
				<td><?php echo $val->tgl;?></td>
				<td><?php echo $val->kelas;?></td>
				<td><?php echo $val->jurusan;?></td>
				<td><?php echo $val->rombel;?></td>
				<td>
					<a href="<?php echo base_url();?>index.php/admin/reset/<?php echo $val->nis;?>" class="hapus">Reset</a>
				</td>
			</tr>
			<?php } ?>
		</table>
	</div>
	<a href="<?php echo site_url('admin/rset');?>">
		<button id="aff">Reset Semua</button>
	</a>
</div>