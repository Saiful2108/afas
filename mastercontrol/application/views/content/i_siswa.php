<?php
	if($status == "edit"){
		$val = $hsl->row_array();
	}else{
		$val['nama'] = "";
		$val['nis'] = "";
		$val['alamat'] = "";
		$val['tgl'] = "";
		$val['kelas'] = "";
		$val['id_jurusan'] = "";
	}
?>
<div class="top">
	<div class="kiri"><p>Input Data Siswa</p>
	<a href="<?php echo base_url();?>admin/halad/siswa"><input type="button" value="X"></a><div id="clear"></div></div>
	<div id="clear"></div>
</div>
<div class="bawah">
	<?php echo form_open($open);?>
		<input type="hidden" name="id" value="<?php echo $val['nis'];?>">
		<p>NIS</p>
		<input type="text" name="nis" value="<?php echo $val['nis'];?>" class="pan">
		<p>Nama</p>
		<input type="text" name="nama" value="<?php echo $val['nama'];?>" class="pan">
		<p>Alamat Siswa</p>
		<textarea name="alamat" class="pan"><?php echo $val['alamat'];?></textarea>
		<p>Tanggal Lahir</p>
		<input type="date" name="tgl" value="<?php echo $val['tgl'];?>" class="pen">
		<p>Kelas</p>
		<select name="kelas" class="pen">
			<option value="X" <?php if($val['kelas'] == "X"){echo "selected";}?>>X</option>
			<option value="XI" <?php if($val['kelas'] == "XI"){echo "selected";}?>>XI</option>
			<option value="XII" <?php if($val['kelas'] == "XII"){echo "selected";}?>>XII</option>
		</select>
		<p>Jurusan</p>
		<select name="id_jurusan" class="pen">
			<?php
				foreach ($jurusan as $key) {
					if($key->id_jurusan == $val['id_jurusan']){
						$sel = "selected";
					}else{
						$sel = "";
					}
			?>
				<option value="<?php echo $key->id_jurusan;?>" <?php echo $sel;?>><?php echo $key->jurusan;?></option>
			<?php } ?>
		</select>
		<p>Rombel</p>
		<select name="id_rombel" class="pen">
			<?php
				foreach ($rombel as $rom) {
					if($rom->id_rombel == $val['id_rombel']){
						$sel = "selected";
					}else{
						$sel = "";
					}
			?>
				<option value="<?php echo $rom->id_rombel;?>" <?php echo $sel;?>><?php echo $rom->rombel;?></option>
			<?php } ?>
		</select>
		<p>
		<input type="submit" value="<?php echo $value;?>">
		</p>
	</form>	
</div>