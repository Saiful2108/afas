<div class="top">
	<div class="kiri"><p>Data Soal</p> 
	<a href="<?php echo base_url();?>index.php/admin/halad/i_soal_try"><input type="button" value="+ Tambah"></a><div id="clear"></div></div> 
	<div id="clear"></div>
</div>
<div class="bawah">
	<table>
		<tr>
			<td style="border-bottom:none;" width="200px">Mata Pelajaran :</td>
			<td style="border-bottom:none;" width="200px">Paket :</td>
			<td style="border-bottom:none;"></td>
		</tr>
		<tr>
			<?php echo form_open();?>
			<td style="border-bottom:none;">
				<select name="id_mapel" id="id_mapel">
					<?php
						foreach ($mapel as $dtng) {
					?>
						<option value="<?php echo $dtng->id_mapel;?>" <?php if($this->input->post('id_mapel') == $dtng->id_mapel){ echo "selected";}?>><?php echo $dtng->mapel;?></option>
					<?php } ?>
				</select>
			</td>
			<td style="border-bottom:none;">
				<select name="paket" id="kelas">
					<option <?php if($this->input->post('paket') == "Latihan"){ echo "selected";}?>>Latihan</option>
					<option <?php if($this->input->post('paket') == "1"){ echo "selected";}?>>1</option>
					<option <?php if($this->input->post('paket') == "2"){ echo "selected";}?>>2</option>
					<option <?php if($this->input->post('paket') == "3"){ echo "selected";}?>>3</option>
					<option <?php if($this->input->post('paket') == "4"){ echo "selected";}?>>4</option>	
				</select>
			</td>
			<td style="border-bottom:none;">
				<input type="submit" name="cari" id="car" value="Tampilkan">
			</td>
			<?php echo form_close();?>
		</tr>
	</table>
	<div id="tabel">
		<table>
			<tr>
				<th>No</th>
				<th>Soal</th>
				<th>Paket</th>
				<th>Mapel</th>
				<th>Aksi</th>
			</tr>
			<?php
				$x=0;
				foreach ($jns as $data) { $x++;
			?>
				<tr>
					<td><?php echo $x;?></td>
					<td><?php echo $data->soal;?></td>
					<td><?php echo $data->paket;?></td>
					<td><?php echo $data->mapel;?></td>
					<td>
						<a href="i_soal_try/<?php echo $data->id_soal;?>" class="edit">Edit</a> | 
						<a href="../../admin/hapus_soal_try/<?php echo $data->id_soal;?>" class="hapus">Hapus</a>
					</td>
				</tr>
			<?php } ?>
		</table>
	</div>
</div>