<?php
	if($status == "edit"){
		$val = $hsl->row_array();
	}else{
		$val['soal'] = "";
		$val['kunci'] = "";
		$val['kelas'] = "";
		$val['id_mapel'] = "";
		$val['jenis_tes'] = "";
	}
?>
<div class="top">
	<div class="kiri"><p>Input Data Soal</p>
	<a href="<?php echo base_url();?>admin/halad/soal"><input type="button" value="X"></a><div id="clear"></div></div>
	<div id="clear"></div>
</div>
<div class="bawah">
	<?php echo form_open_multipart($open);?>
	<input type="hidden" name="id_soal" value="<?php echo $this->uri->segment(4);?>">
	<p>Soal</p>
	<textarea name="soal" id="soal"><?php echo $val['soal'];?></textarea>
	<table width="100%">
		<tr>
			<td>
				<p>Kunci Jawaban</p>
				<p><select name="kunci" class="pen">
					<option value="A" <?php if($val['kunci'] == "A"){echo "selected";}?>>A</option>
					<option value="B" <?php if($val['kunci'] == "B"){echo "selected";}?>>B</option>
					<option value="C" <?php if($val['kunci'] == "C"){echo "selected";}?>>C</option>
					<option value="D" <?php if($val['kunci'] == "D"){echo "selected";}?>>D</option>
					<option value="E" <?php if($val['kunci'] == "E"){echo "selected";}?>>E</option>
				</select></p>
			</td>
		</tr>
		<tr>
			<td>
				<p>Kelas</p>
				<p>
					<select name="kelas" id="kelas" class="pen">
						<option value="X" <?php if($val['kelas'] == "X"){echo "selected";}?>>X</option>
						<option value="XI" <?php if($val['kelas'] == "XI"){echo "selected";}?>>XI</option>
						<option value="XII" <?php if($val['kelas'] == "XII"){echo "selected";}?>>XII</option>
					</select>
				</p>
			</td>
		</tr>
		<tr>
			<td>
			<p>Mapel</p>
			<p>
				<select name="id_mapel" id="id_mapel" class="pen">
				<?php
					$id_guru = $this->session->userdata("id");
					$qwl = $this->model_admin->qw("mapel_guru,mapel","WHERE mapel_guru.id_mapel = mapel.id_mapel AND mapel_guru.id_guru = '$id_guru'")->result();
					foreach ($qwl as $am) {
				?>
				<option value="<?php echo $am->id_mapel;?>" <?php if($val['id_mapel'] == $am->id_mapel){echo "selected";}?>><?php echo $am->mapel;?></option>
					<?php } ?>
				</select>
			</p>
			</td>
			</tr>
			<tr>
			
			<td>
				<p>Jenis Tes</p>
				<p><select name="jenis" class="pen">
					<?php 
						$qwj = $this->model_admin->tampil_data("jenis_tes")->result();
						foreach ($qwj as $am) {
					?>
					<option value="<?php echo $am->id_tes;?>" <?php if($val['jenis_tes'] == $am->id_tes){echo "selected";}?>><?php echo $am->jenis_tes;?></option>
						<?php } ?>
				</select></p>
			</td>
		</tr>
		<tr>
			<td>
				<p>Listening</p>
				<p><input type="file" name="list"></p>
			</td>
		</tr>
	</table>
	<p>
		<input type="submit" value="<?php echo $value;?>" name="simpan">
	</p>
</form>	
</div>