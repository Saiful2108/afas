<div class="top">
	<div class="kiri"><p>Data Mapel</p> 
	<a href="<?php echo base_url();?>admin/halad/i_mapel"><input type="button" value="+ Tambah"></a><div id="clear"></div></div> 
	<div class="kanan"><input type="text" placeholder="Search . . ." id="cari"></div>
	<div id="clear"></div>
</div>
<div class="bawah">
	<div id="tabel">
		<table>
			<tr>
				<th>No</th>
				<th>Mapel</th>
				<th>Jumlah Soal</th>
				<th>KKM X</th>
				<th>KKM XI</th>
				<th>KKM XII</th>
				<th width="200px">Aksi</th>
			</tr>
			<?php
				$x=0;
				foreach ($jns as $data) { $x++;
			?>
				<tr>
					<td><?php echo $x;?></td>
					<td><?php echo $data->mapel;?></td>
					<td><?php echo $data->jumlah_soal;?></td>
					<td><?php echo $data->kkm1;?></td>
					<td><?php echo $data->kkm2;?></td>
					<td><?php echo $data->kkm3;?></td>
					<td>
						<a href="i_mapel/<?php echo $data->id_mapel;?>" class="edit">Edit</a> | 
						<a href="../../admin/hapus_mapel/<?php echo $data->id_mapel;?>" class="hapus">Hapus</a> |
						<a href="../../admin/jumlah_soal/<?php echo $data->id_mapel;?>" target="_blank">Lihat Soal</a>
					</td>
				</tr>
			<?php } ?>
		</table>
	</div>
</div>