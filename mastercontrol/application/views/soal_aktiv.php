<style type="text/css">
	*{
		font-family: Segoe UI;
	}
	select{
		width: 200px;
		padding: 7px 0px;
		border:1px solid #ddd;
		text-indent: 5px;
		font-size: 16px;
		border-radius: 2px;
	}
	input[type="submit"]{
		background-color: #4b4ba3;
		border: 1px solid #4b4ba3;
		color: #fff;
		padding: 10px 25px;
		border-radius: 2px;
		cursor: pointer;
	}
	.xdc{
		width: 300px;
		height: 200px;
	}
	.h1{
		width: 100%;
		text-align: center;
		font-size: 30px;
		color: #22313F;
		margin: 0px auto;
	}
	.p{
		width: 100%;
		text-align: center;
		color: #22313F;
		font-weight: bolder;
	}
	.table{
		width: 100%;
	}
	.table th{
		height: 40px;
		background-color: #6553db;
		color: #fff;
	}
	.table td{
		border-bottom: 1px solid #ddd;
		height: 30px;
		font-size: 13px;
	}
</style>
<!DOCTYPE html>
<html>
<head>
	<title>Lihat Soal Aktif</title>
</head>
<body>
	<h1 class="h1">Data Soal Aktif</h1>
	<table width="100%" cellspacing="0px" cellpadding="10px" class="table">
		<tr>
			<th align="left" width="20px">No</th>
			<th align="left">Mapel</th>
			<th align="left">Kelas</th>
			<th align="left">Jenis Tes</th>
			<th align="left">Jumlah Soal</th>
		</tr>
		<?php
			$x=0;
			$qwsoal = $this->model_admin->qw("soal_aktif","GROUP BY id_mapel")->result();
			foreach ($qwsoal as $dasol) {
				$x++;
		?>
		<tr>
			<td><?php echo $x;?></td>
			<td>
				<?php
					$id_mapel = $dasol->id_mapel;
					$slm = $this->model_admin->qw("mapel","WHERE id_mapel = '$id_mapel'")->row_array();
					echo $slm['mapel'];
				?>
			</td>
			<td>
				<?php
					$slct = $this->model_admin->qw("soal_aktif","WHERE id_mapel = '$id_mapel' GROUP BY kelas")->result();
					foreach ($slct as $dkls) {
						echo $dkls->kelas."&nbsp;";
					}
				?>
			</td>
			<td>
				<?php
					$slctj = $this->model_admin->qw("soal_aktif","WHERE id_mapel = '$id_mapel' GROUP BY jenis_tes")->result();
					foreach ($slctj as $djenis) {
						$id_tes = $djenis->jenis_tes;
						$jenis_tes = $this->model_admin->qw("jenis_tes","WHERE id_tes = '$id_tes' GROUP BY jenis_tes")->result();
						foreach ($jenis_tes as $jts) {
							echo $jts->jenis_tes."&nbsp;";
						}
					}
				?>
			</td>
			<td>
				<?php
					$hsoal = $this->model_admin->qw("soal_aktif","WHERE id_mapel = '$id_mapel'")->num_rows();
					echo $hsoal;
				?>
			</td>
		</tr>
		<?php } ?>
	</table>
</body>
</html>