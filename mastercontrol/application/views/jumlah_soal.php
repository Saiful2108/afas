<style type="text/css">
	*{
		font-family: Segoe UI;
	}
	select{
		width: 200px;
		padding: 7px 0px;
		border:1px solid #ddd;
		text-indent: 5px;
		font-size: 16px;
		border-radius: 2px;
	}
	input[type="submit"]{
		background-color: #4b4ba3;
		border: 1px solid #4b4ba3;
		color: #fff;
		padding: 10px 25px;
		border-radius: 2px;
		cursor: pointer;
	}
	.xdc{
		width: 300px;
		height: 200px;
	}
	.h1{
		width: 100%;
		text-align: center;
		font-size: 30px;
		color: #22313F;
		margin: 0px auto;
	}
	.p{
		width: 100%;
		text-align: center;
		color: #22313F;
		font-weight: bolder;
	}
	.table{
		width: 100%;
	}
	.table th{
		height: 40px;
		background-color: #6553db;
		color: #fff;
	}
	.table td{
		border-bottom: 1px solid #ddd;
		height: 30px;
		font-size: 13px;
	}
</style>
<!DOCTYPE html>
<html>
<head>
	<title>Lihat Jumlah Soal</title>
</head>
<body>
	<h1 class="h1">Jumlah Soal <?php echo $pel;?></h1>
	
	<?php echo form_open("admin/jumlah_soal/".$this->uri->segment(3)."/cari");?>
		<table cellspacing="5">
			<tr>
				<td><p>Jenis Tes :</p></td>
			</tr>
			<tr>
				<td>
					<select name="jen">
						<?php
							foreach ($jns as $dtd) {
						?>
						<option value="<?php echo $dtd->id_tes;?>" <?php if($this->input->post("jen") == $dtd->id_tes){echo "selected";};?>><?php echo $dtd->jenis_tes;?></option>

						<?php } ?>
					</select>
				</td>
				<td>
					<input type="submit" name="cari" value="Tampilkan">
				</td>
			</tr>
		</table>
	<?php echo form_close();?>
	<table width="100%" cellspacing="0px" cellpadding="10px" class="table">
			<th align="left" width="20px">No</th>
			<th align="left">Mapel</th>
			<th align="left">Kelas</th>
			<th align="left">Jumlah Soal</th>
		</tr>
		<?php
			if(!empty($this->uri->segment(4))){
		?>
		<tr>
			<td>1</td>
			<td><?php echo $pel;?></td>
			<td> X </td>
			<td>
				<?php
					$jenis_tes = $this->input->post("jen");
					$id_mapel = $this->uri->segment(3);
					$qwjm = $this->model_admin->qw("soal","WHERE id_mapel='$id_mapel' AND jenis_tes = '$jenis_tes' AND kelas = 'X'")->num_rows();
					echo $qwjm;
				?> / <?php echo $jsoal;?>
			</td>
		</tr>
		<tr>
			<td>2</td>
			<td><?php echo $pel;?></td>
			<td> XI </td>
			<td>
				<?php
					$jenis_tes = $this->input->post("jen");
					$id_mapel = $this->uri->segment(3);
					$qwjm = $this->model_admin->qw("soal","WHERE id_mapel='$id_mapel' AND jenis_tes = '$jenis_tes' AND kelas = 'XI'")->num_rows();
					echo $qwjm;
				?> / <?php echo $jsoal;?>
			</td>
		</tr>
		<tr>
			<td>3</td>
			<td><?php echo $pel;?></td>
			<td> XII </td>
			<td>
				<?php
					$jenis_tes = $this->input->post("jen");
					$id_mapel = $this->uri->segment(3);
					$qwjm = $this->model_admin->qw("soal","WHERE id_mapel='$id_mapel' AND jenis_tes = '$jenis_tes' AND kelas = 'XII'")->num_rows();
					echo $qwjm;
				?> / <?php echo $jsoal;?>
			</td>
		</tr>
		<?php
			}else{
		?>
		<tr>
			
		</tr>
		<?php } ?>
	</table>
</body>
</html>