<style type="text/css">
	*{
		font-family: Segoe UI;
	}
	select{
		width: 200px;
		padding: 7px 0px;
		border:1px solid #ddd;
		text-indent: 5px;
		font-size: 16px;
		border-radius: 2px;
	}
	input[type="submit"]{
		background-color: #4b4ba3;
		border: 1px solid #4b4ba3;
		color: #fff;
		padding: 10px 25px;
		border-radius: 2px;
		cursor: pointer;
	}
	.xdc{
		width: 300px;
		height: 200px;
	}
	.h1{
		width: 100%;
		text-align: center;
		font-size: 30px;
		color: #22313F;
		margin: 0px auto;
	}
	.p{
		width: 100%;
		text-align: center;
		color: #22313F;
		font-weight: bolder;
	}
	.table{
		width: 100%;
	}
	.table th{
		height: 40px;
		background-color: #6553db;
		color: #fff;
	}
	.table td{
		border: 1px solid #ddd;
		height: 30px;
		font-size: 13px;
	}
</style>
<!DOCTYPE html>
<html>
<head>
	<title>Lihat Soal</title>
</head>
<body>
	<h1 class="h1">Data Soal <?php echo $pel;?></h1>
	<?php echo form_open("admin/lihat_soal_tryout/cari");?>
		<table cellspacing="10">
			<tr>
				<td><p>Mata Pelajaran :</p></td>
				<td><p>Paket :</p></td>
			</tr>
			<tr>
				<td>
					<select name="pelajaran">
						<?php
							foreach ($var as $dtd) {
						?>
						<option value="<?php echo $dtd->id_mapel;?>" <?php if($this->input->post("pelajaran") == $dtd->id_mapel){echo "selected";};?>><?php echo $dtd->mapel;?></option>

						<?php } ?>
					</select>
				</td>
				<td>
					<select name="jen">
						<option value="Latihan" <?php if($this->input->post("jen") == "Latihan"){echo "selected";};?>>Latihan</option>
						<?php
							for($x=1;$x<=4;$x++){
						?>
						<option value="<?php echo $x;?>" <?php if($this->input->post("jen") == $x){echo "selected";};?>><?php echo $x;?></option>

						<?php } ?>
					</select>
				</td>
				<td>
					<input type="submit" name="cari" value="CARI">
				</td>
			</tr>
		</table>
	<?php echo form_close();?>
	<table width="100%" border="0" cellpadding="10px" class="table">
			<th align="left">No</th>
			<th width="200px">Soal</th>
			<th>Kunci</th>
		</tr>
		<?php
			$x=0;
			if($soal == ""){

			}else{
			foreach ($soal as $datsol) {
			$x++;
		?>
		<tr>
			<td valign="top"><p class="p"><?php echo $x;?></p></td>
			<td>
				<?php
					if(empty($datsol->listening)){
						$list = "";
					}else{
						$list = base_url()."../assets/listening/".$datsol->listening;
				?>
				<audio class="audioDemo" controls preload="none"> 
				   <source src="<?php echo $list;?>" type="audio/mp3">
				</audio>
				<?php } ?>
				<br>
				<?php
					if(!empty($datsol->gambar)){
						$fg = base_url()."../assets/listening/".$datsol->gambar;
						echo "<img src='$fg' class='xdc'>";
					}else{

					}
			 		echo $datsol->soal;
			 	?>
			</td>
			<td><h1 class="h1"><?php echo $datsol->kunci;?></h1></td>
		</tr>
		<?php } ?>
		<?php } ?>
	</table>
</body>
</html>