<?php	
	$dtss="siswa".date("Y-m-d").".xls";
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename='$dtss'");
?>
<table border="1">
	<tr>
		<th>NIS</th>
		<th>Nama</th>
		<th>Alamat</th>
		<th>Tanggal Lahir</th>
		<th>Kelas</th>
		<th>JurusanID</th>
		<th>RombelID</th>
		<th>Status</th>
	</tr>
	<?php
		foreach ($sis as $dt) {
	?>
	<tr>
		<td><?php echo $dt->nis;?></td>
		<td><?php echo $dt->nama;?></td>
		<td><?php echo $dt->alamat;?></td>
		<td><?php echo $dt->tgl;?></td>
		<td><?php echo $dt->kelas;?></td>
		<td><?php echo $dt->id_jurusan;?></td>
		<td><?php echo $dt->id_rombel;?></td>
		<td><?php echo $dt->status;?></td>
	</tr>
	<?php } ?>
</table>

<br/><br/><br/>

<table border="1">
	<tr>
		<th colspan="2">Data Jurusan</th>
	</tr>
	<tr>
		<th>JurusanID</th>
		<th>Jurusan</th>
	</tr>
	<?php
		$jurusan = $this->model_admin->qw("jurusan","")->result();
		foreach ($jurusan as $dajur) {
	?>
	<tr>
		<td><?php echo $dajur->id_jurusan;?></td>
		<td><?php echo $dajur->jurusan;?></td>
	</tr>
	<?php } ?>
</table>
<br>
<table border="1">
	<tr>
		<th colspan="2">Data Rombel</th>
	</tr>
	<tr>
		<th>RombelID</th>
		<th>Rombel</th>
	</tr>
	<?php
		$rombel = $this->model_admin->qw("rombel","")->result();
		foreach ($rombel as $drom) {
	?>
	<tr>
		<td><?php echo $drom->id_rombel;?></td>
		<td><?php echo $drom->rombel;?></td>
	</tr>
	<?php } ?>
</table>