<style type="text/css">
	*{
		margin: 0px;
		padding: 0px;
		font-family: Segoe UI;
	}
	body{
		background-color: #22313F;
	}
	h1{
		text-align: center;
		color: #fff;
		margin: 150px auto 0px;
	}
	#bgtext{
		width: 400px;
		height: 100px;
		margin: 10px auto;
	}
	#bgtext input[type="text"]{
		float: left;
		width: 70px;
		height: 30px;
		text-align: center;
		font-weight: normal;
		margin: 20px 15px;
		text-transform: uppercase;
		font-size: 14px;
	}
	#bgtext input[type="submit"]{
		width: 93%;
		margin: 0px 15px;
		border: 1px solid #ccd6e2;
		background-color: #ccd6e2;
		height: 30px;
		cursor: pointer;
	}
</style>
<!DOCTYPE html>
<html>
<head>
	<title>License</title>
</head>
<body>
	<h1>Activate License !</h1>
	<div id="bgtext">
		<hr>
		<?php echo form_open('lisensi/tmbh');?>
		<?php
			for($x=1;$x<=4;$x++){
		?>
			<input type="text" required name="text<?php echo $x;?>" maxlength="5">
		<?php } ?>
		<input type="submit" name="add" value="Enter License">
		<?php echo form_close();?>
		<br>
		<hr>
	</div>
</body>
</html>