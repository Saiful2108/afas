<style type="text/css">
	*{
		font-family: Segoe UI;
	}
	select{
		width: 200px;
		padding: 7px 0px;
		border:1px solid #ddd;
		text-indent: 5px;
		font-size: 16px;
		border-radius: 2px;
	}
	input[type="submit"]{
		background-color: #4b4ba3;
		border: 1px solid #4b4ba3;
		color: #fff;
		padding: 10px 25px;
		border-radius: 2px;
		cursor: pointer;
	}
	.xdc{
		width: 300px;
		height: 200px;
	}
	.h1{
		width: 100%;
		text-align: center;
		font-size: 30px;
		color: #22313F;
		margin: 0px auto;
	}
	.h2{
		width: 100%;
		text-align: center;
		font-size: 20px;
		color: #22313F;
		font-weight: normal;
		margin: 0px auto;	
	}
	.p{
		width: 100%;
		text-align: center;
		color: #22313F;
		font-weight: bolder;
	}
	.table{
		width: 100%;
	}
	.table th{
		height: 40px;
		background-color: #6553db;
		color: #fff;
	}
	.table td{
		border: 1px solid #ddd;
		height: 30px;
		font-size: 13px;
	}
</style>
<!DOCTYPE html>
<html>
<head>
	<title>Rekap Nilai</title>
</head>
<body>
	<?php
		$nis = $this->uri->segment(3);
		$id_mapel = $this->uri->segment(4);
		$siswa = $this->model_admin->qw("siswa,rombel","WHERE siswa.nis='$nis' AND siswa.id_rombel = rombel.id_rombel")->row_array();
		$mapel =$this->model_admin->qw("mapel","WHERE id_mapel = '$id_mapel'")->row_array();
	?>
	<h1 class="h1">Rekap Nilai</h1>
	<h2 class="h2"><?php echo $mapel['mapel'];?></h2>
	<hr size="1" color="#ddd">
	<table cellspacing="10px">
		<tr>
			<td>NIS</td>
			<td>:</td>
			<td><?php echo $siswa['nis'];?></td>
		</tr>
		<tr>
			<td>Nama</td>
			<td>:</td>
			<td><?php echo $siswa['nama'];?></td>
		</tr>
		<tr>
			<td>Rombel</td>
			<td>:</td>
			<td><?php echo $siswa['rombel'];?></td>
		</tr>
	</table>
	<table width="100%" border="0" cellpadding="10px" class="table">
			<th align="left" width="60px">No</th>
			<th width="750px">Soal</th>
			<th>Kunci</th>
			<th>Jawaban Siswa</th>
			<th>Status</th>
		</tr>
		<?php
			$x=0;
			if($soal == ""){

			}else{
			foreach ($soal as $datsol) {
			$x++;
		?>
		<tr>
			<td valign="top"><p class="p"><?php echo $x;?></p></td>
			<td>
				<?php
					if(empty($datsol->listening)){
						$list = "";
					}else{
						$list = base_url()."../assets/listening/".$datsol->listening;
				?>
				<audio class="audioDemo" controls preload="none"> 
				   <source src="<?php echo $list;?>" type="audio/mp3">
				</audio>
				<?php } ?>
				<br>
				<?php
					if(!empty($datsol->gambar)){
						$fg = base_url()."../assets/listening/".$datsol->gambar;
						echo "<img src='$fg' class='xdc'>";
					}else{

					}
			 		echo $datsol->soal;
			 	?>
			</td>
			<td><h1 class="h1"><?php echo $datsol->kunci;?></h1></td>
			<td><h1 class="h1"><?php echo $datsol->jawaban;?></h1></td>
			<td>
				<h1>
				<?php
					if($datsol->kunci == $datsol->jawaban){
						echo "Benar";
					}else{
						echo "Salah";
					}
				?>
				</h1>
			</td>
		</tr>
		<?php } ?>
		<?php } ?>
	</table>
</body>
</html>