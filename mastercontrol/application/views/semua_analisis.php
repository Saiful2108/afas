<style type="text/css">
	*{
		font-family: Segoe UI;
	}
	select{
		width: 200px;
		padding: 7px 0px;
		border:1px solid #ddd;
		text-indent: 5px;
		font-size: 16px;
		border-radius: 2px;
	}
	input[type="submit"]{
		background-color: #4b4ba3;
		border: 1px solid #4b4ba3;
		color: #fff;
		padding: 10px 25px;
		border-radius: 2px;
		cursor: pointer;
	}
	.xdc{
		width: 300px;
		height: 200px;
	}
	.h1{
		width: 100%;
		text-align: center;
		font-size: 30px;
		color: #22313F;
		margin: 0px auto;
	}
	.p{
		width: 100%;
		text-align: center;
		color: #22313F;
		font-weight: bolder;
	}
	.table{
		float: left;
		margin-bottom: 50px;
	}
	.table th{
		height: 40px;
		background-color: #6553db;
		border: 1px solid #5c51ad;
		font-size: 14px;
		color: #fff;
	}
	.table td{
		border: 1px solid #ddd;
		height: 30px;
		font-size: 13px;
	}
</style>
<!DOCTYPE html>
<html>
<head>
	<title>Lihat Soal</title>
</head>
<body>
	<?php echo form_open("");?>
		<table cellspacing="10">
			<tr>
				<td><p>Mata Pelajaran :</p></td>
				<td><p>Jenis Tes :</p></td>
				<td><p>Kelas :</p></td>
			</tr>
			<tr>
				<td>
					<select name="pelajaran">
						<?php
							foreach ($var as $dtd) {
						?>
						<option value="<?php echo $dtd->id_mapel;?>" <?php if($this->input->post("pelajaran") == $dtd->id_mapel){echo "selected";};?>><?php echo $dtd->mapel;?></option>

						<?php } ?>
					</select>
				</td>
				<td>
					<select name="jen">
						<?php
							foreach ($jns as $dtd) {
						?>
						<option value="<?php echo $dtd->id_tes;?>" <?php if($this->input->post("jen") == $dtd->id_tes){echo "selected";};?>><?php echo $dtd->jenis_tes;?></option>

						<?php } ?>
					</select>
				</td>
				<td>
					<select name="kelas">
						<option value="X" <?php if($this->input->post('kelas') == "X"){ echo "selected";};?>>X</option>
						<option value="XI" <?php if($this->input->post('kelas') == "XI"){ echo "selected";};?>>XI</option>
						<option value="XII" <?php if($this->input->post('kelas') == "XII"){ echo "selected";};?>>XII</option>

					</select>
				</td>
				<td>
					<select name="rbl">
						<?php
							foreach ($rbl as $dbl) {
						?>
						<option value="<?php echo $dbl->id_rombel;?>" <?php if($this->input->post("rbl") == $dbl->id_rombel){echo "selected";};?>><?php echo $dbl->rombel;?></option>

						<?php } ?>
					</select>
				</td>
				<td>
					<input type="submit" name="cari" value="CARI">
				</td>
			</tr>
		</table>
	<?php echo form_close();?>
	<?php
		if(isset($_POST['pelajaran'])){
			$pl = $this->input->post("pelajaran");
			$jn = $this->input->post("jen");
			$kl = $this->input->post("kelas");
			$rm = $this->input->post("rbl");
	?>
	<table cellspacing="0" class="table" width="100%">
	<?php		
		$this->db->select("id_soal,kunci");
		$this->db->where(array("id_mapel"=>$pl,"jenis_tes"=>$jn,"kelas"=>$kl));
		$ik = $this->db->get("soal");
		$jmlsol = $ik->num_rows();
	?>
		<tr>
			<th colspan="<?php echo $jmlsol+2;?>">Analisis Semua Soal</th>
		</tr>
		<tr>
			<th rowspan="2">Nama</th>
			<?php
				$ix= $ik->result();
				$x=0;
				foreach ($ix as $dbt) { $x++;
			?>
			<th><?php echo $x;?></th>
			<?php } ?>
			<th rowspan="2">Nilai</th>	
		</tr>
		<tr>
			<?php
				$ix= $ik->result();
				$x=0;
				foreach ($ix as $dbt) { $x++;
			?>
			<th><?php echo $dbt->kunci;?></th>
			<?php } ?>
		</tr>
		<?php
			$this->db->select("nis,nama");
			$this->db->where(array("kelas"=>$kl,"id_rombel"=>$rm));
			$sl = $this->db->get("siswa")->result();
			foreach ($sl as $dsiswa) {
		?>
		<tr>
			<td width="250px"><?php echo $dsiswa->nama;?></td>
			<?php
				foreach ($ix as $dsl) {
					$this->db->select("jawaban");
					$this->db->where(array("nis"=>$dsiswa->nis,"id_mapel"=>$pl,"id_soal"=>$dsl->id_soal));
					$jwbw = $this->db->get("jawaban2")->row_array();
					if($dsl->kunci == $jwbw['jawaban']){
						$bh="#5fed38";
					}else{
						$bh="#c92e2e";
					}
			?>
				<td bgcolor="<?php echo $bh;?>" align="center"><?php echo $jwbw['jawaban'];?></td>
			<?php } ?>
			<?php
				$this->db->select("nilai");
				$this->db->where(array("nis"=>$dsiswa->nis,"id_mapel"=>$pl,"id_jenis"=>$jn,"kelas"=>$kl));
				$nly = $this->db->get("nilai")->row_array();
				$this->db->select("kkm1,kkm2,kkm3");
				$this->db->where(array("id_mapel"=>$pl));
				$nxx = $this->db->get("mapel")->row_array();
				if($kl == "X"){
					$kkm = $nxx['kkm1'];
				}elseif($kl = "XI"){
					$kkm = $nxx['kkm2'];
				}else{
					$kkm = $nxx['kkm3'];
				}
				if($nly['nilai'] < $kkm){
					$bt = "#c92e2e";
				}else{
					$bt = "";
				}
			?>
			<td align="center" bgcolor="<?php echo $bt;?>"><?php echo $nly['nilai'];?></td>
		</tr>
		<?php } ?>
		<tr>
			<td align="right"><b>Jumlah Mengerjakan</b></td>
			<?php
				foreach ($ix as $dslx) {
					$id_soal = $dslx->id_soal;
					$jmljwbw = $this->db->query("SELECT jawaban2.jawaban FROM `jawaban2`,`siswa` WHERE jawaban2.nis = siswa.nis AND jawaban2.id_soal = '$id_soal' AND siswa.id_rombel = '$rm'")->num_rows();
			?>
			<td align="center"><?php echo $jmljwbw;?></td>
			<?php } ?>
			<td></td>
		</tr>
		<tr>
			<td align="right"><b>Jumlah Salah</b></td>
			<?php
				foreach ($ix as $dslx) {
					$id_soal = $dslx->id_soal;
					$jmljwbws = $this->db->query("SELECT jawaban2.jawaban FROM `jawaban2`,`siswa` WHERE jawaban2.nis = siswa.nis AND jawaban2.id_soal = '$id_soal' AND siswa.id_rombel = '$rm' AND poin = 0")->num_rows();
			?>
			<td align="center"><?php echo $jmljwbws;?></td>
			<?php } ?>
			<td></td>
		</tr>
		<tr>
			<td align="right"><b>Kategori Soal</b></td>
			<?php
				foreach ($ix as $dslxx) {
					$id_soal = $dslxx->id_soal;
					$jmljwbw = $this->db->query("SELECT jawaban2.jawaban FROM `jawaban2`,`siswa` WHERE jawaban2.nis = siswa.nis AND jawaban2.id_soal = '$id_soal' AND siswa.id_rombel = '$rm'")->num_rows();
					$jmljwbws = $this->db->query("SELECT jawaban2.jawaban FROM `jawaban2`,`siswa` WHERE jawaban2.nis = siswa.nis AND jawaban2.id_soal = '$id_soal' AND siswa.id_rombel = '$rm' AND poin = 0")->num_rows();
					$jben = $jmljwbw-$jmljwbws;
					$prs = ($jben/$jmljwbw)*100;
					if($prs <= 30){
						$st = "A";
						$bpr = "#e53939";
					}elseif($prs <= 70 && $prs > 30){
						$st = "B";
						$bpr = "#f9f968";
					}else{
						$bpr = "#3df75c";
						$st = "C";
					}
			?>
			<td align="center" bgcolor="<?php echo $bpr;?>"><?php echo $st;?></td>
			<?php } ?>
			<td></td>
		</tr>
	</table>
	<table cellspacing="0" class="table" width="400px">
		<tr>
			<th colspan="2">Keterangan</th>
		</tr>
		<tr>
			<td bgcolor="#e53939" align="center">A</td><td>Sulit</td>
		</tr>
		<tr>
			<td bgcolor="#f9f968" align="center">B</td><td>Sedang</td>
		</tr>
		<tr>
			<td bgcolor="#3df75c" align="center">C</td><td>Mudah</td>
		</tr>
	</table>
	<?php
		}else{
				
		}
	?>
</body>
</html>