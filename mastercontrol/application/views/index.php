<?php
	if($this->session->userdata('level') == "Guru"){
		$gur = "";
		$ad  = "hidden";
		$a="hidden";
		$pen = "hidden";
		$id_gur = $this->session->userdata("id");
		$qwyi = $this->model_admin->qw("tryout_guru","WHERE id_guru = '$id_gur'")->num_rows();
		if($qwyi == 0){
			$maptry = "hidden";
		}else{
			$maptry="";
		}
	}elseif($this->session->userdata('level') == "Pengawas"){
		$gur = "hidden";
		$ad  = "hidden";
		$a="hidden";
		$pen = "";
		$maptry = "hidden";
	}else{
		$gur = "hidden";
		$ad  = "";
		$a="";
		$pen = "hidden";
		$maptry = "hidden";
	}
?>
<?php
	$tyu = $this->model_admin->qw("pengaturan","WHERE jenis = 'Rekomendasi'")->row_array();
	if($tyu['status'] == "off"){
		$rekom = "hidden";
	}else{
		$rekom = "";
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title;?></title>
</head>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>../assets/css/admin/style.css">
<body>
<div id="header">
	<div id="kanan">
		<p><?php echo $this->session->userdata("nama");?></p>
		<a href="<?php echo site_url('login/keluar');?>" title="Keluar">
			<img src="<?php echo base_url();?>../assets/icon/power.svg">
		</a>
	</div>
</div>
<div id="menu">
	<div id="logo">
		<img src="<?php echo base_url();?>../assets/icon/logo-afas.png">
	</div>
	<div id="userlogin">
		<img src="<?php echo base_url();?>../assets/icon/user.svg">
		<h2><?php echo $this->session->userdata('nama');?></h2>
		<p><?php echo $this->session->userdata('level');?></p>
	</div>
	<ul>
		<a href="<?php echo base_url();?>admin/halad/beranda" title="Beranda"><li>Beranda</li></a>
		<a href="<?php echo base_url();?>admin/halad/identitas"><li <?php echo $ad;?>>Identitas Sekolah</li></a>
		<a href="<?php echo base_url();?>admin/halad/edit_guru/<?php echo $this->session->userdata('id');?>"><li <?php echo $gur;?>>Edit Profil</li></a>
		<a href="<?php echo base_url();?>admin/halad/siswa"><li <?php echo $ad;?>>Data Siswa</li></a>
		<a href="<?php echo base_url();?>admin/halad/guru"><li <?php echo $ad;?>>Data Guru</li></a>
		<a href="<?php echo base_url();?>admin/halad/guru_try"><li <?php echo $ad;?>>Data Guru Tryout</li></a>
		<a href="<?php echo base_url();?>admin/halad/jenis_tes"><li <?php echo $ad;?>>Jenis Tes</li></a>
		<a href="<?php echo base_url();?>admin/halad/jurusan"><li <?php echo $ad;?>>Jurusan</li></a>
		<a href="<?php echo base_url();?>admin/halad/rombel"><li <?php echo $ad;?>>Rombel</li></a>
		<a href="<?php echo base_url();?>admin/halad/mapel"><li <?php echo $ad;?>>Mata Pelajaran</li></a>
		<a href="<?php echo base_url();?>admin/halad/mapel_tryout"><li <?php echo $ad;?>>Mata Pelajaran Tryout</li></a>
		<a href="<?php echo base_url();?>admin/halad/soal"><li <?php echo $gur;?>>Input Soal</li></a>
		<a href="<?php echo base_url();?>admin/lihat_soal" target="_blank"><li <?php echo $gur;?>>Lihat Soal</li></a>
		<a href="<?php echo base_url();?>admin/lihat_soala" target="_blank"><li <?php echo $a;?>>Lihat Soal</li></a>
		<a href="<?php echo base_url();?>admin/lihat_nilai" target="_blank"><li <?php echo $gur;?>>Cetak Nilai</li></a>
		<a href="<?php echo base_url();?>admin/halad/p_mapel"><li <?php echo $gur;?>>Peringkat Mapel</li></a>
		<a href="<?php echo base_url();?>admin/halad/k_nil"><li <?php echo $gur;?>>Kelola Nilai</li></a>
		<a href="<?php echo base_url();?>admin/halad/soal_try"><li <?php echo $maptry;?>>Input Soal Tryout</li></a>
		<a href="<?php echo base_url();?>admin/lihat_soal_tryout" target="_blank"><li <?php echo $maptry;?>>Lihat Soal Tryout</li></a>
		<a href="<?php echo base_url();?>admin/halad/nilai_tryout"><li <?php echo $maptry;?>>Nilai Tryout</li></a>
		<a href="<?php echo base_url();?>admin/halad/absen"><li <?php echo $pen;?>>Absensi</li></a>
		<a href="<?php echo base_url();?>admin/halad/rekomen"><li <?php echo $gur;?> <?php echo $rekom;?>>Rekomendasi</li></a>
		<a href="<?php echo base_url();?>admin/halad/cek_rekom"><li <?php echo $pen;?> <?php echo $rekom;?>>Cek Rekomendasi</li></a>
		<a href="<?php echo base_url();?>admin/halad/status"><li>Cek Status</li></a>
		<a href="<?php echo base_url();?>admin/halad/pengaturan"><li <?php echo $ad;?>>Pengaturan</li></a>
		<a href="<?php echo base_url();?>../proses/proses/halad/proses_nilai"><li <?php echo $ad;?>>Backup Nilai</li></a>
	</ul>

</div>
<div id="content">
	<?php
		include('content/'.$lod.'.php');
	?>
</div>
<script type="text/javascript" src="<?php echo base_url();?>../assets/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>../assets/ckeditor/ckeditor.js"></script>
<?php include "../assets/js/function.php";?>
</body>
</html>
<script type="text/javascript">
	CKEDITOR.replace('soal',{
		filebrowserBrowseUrl : '<?php echo base_url();?>../assets/ckeditor/plugins/fileman/index.html',
		filebrowserImageBrowseUrl : "<?php echo base_url();?>../assets/ckeditor/plugins/fileman/index.html?type=image"
	})
</script>
<script language=JavaScript>
<!--

//Disable right mouse click Script
//By Maximus (maximus@nsimail.com) w/ mods by DynamicDrive
//For full source code, visit http://www.dynamicdrive.com

var message="Function Disabled!";

///////////////////////////////////
function clickIE4(){
if (event.button==2){
alert(message);
return false;
}
}

function clickNS4(e){
if (document.layers||document.getElementById&&!document.all){
if (e.which==2||e.which==3){
alert(message);
return false;
}
}
}

if (document.layers){
document.captureEvents(Event.MOUSEDOWN);
document.onmousedown=clickNS4;
}
else if (document.all&&!document.getElementById){
document.onmousedown=clickIE4;
}

document.oncontextmenu=new Function("return false")

// -->

shortcut={all_shortcuts:{},add:function(a,b,c){var d={type:"keydown",propagate:!1,disable_in_input:!1,target:document,keycode:!1};if(c)for(var e in d)"undefined"==typeof c[e]&&(c[e]=d[e]);else c=d;d=c.target,"string"==typeof c.target&&(d=document.getElementById(c.target)),a=a.toLowerCase(),e=function(d){d=d||window.event;if(c.disable_in_input){var e;d.target?e=d.target:d.srcElement&&(e=d.srcElement),3==e.nodeType&&(e=e.parentNode);if("INPUT"==e.tagName||"TEXTAREA"==e.tagName)return}d.keyCode?code=d.keyCode:d.which&&(code=d.which),e=String.fromCharCode(code).toLowerCase(),188==code&&(e=","),190==code&&(e=".");var f=a.split("+"),g=0,h={"`":"~",1:"!",2:"@",3:"#",4:"$",5:"%",6:"^",7:"&",8:"*",9:"(",0:")","-":"_","=":"+",";":":","'":'"',",":"<",".":">","/":"?","\\":"|"},i={esc:27,escape:27,tab:9,space:32,"return":13,enter:13,backspace:8,scrolllock:145,scroll_lock:145,scroll:145,capslock:20,caps_lock:20,caps:20,numlock:144,num_lock:144,num:144,pause:19,"break":19,insert:45,home:36,"delete":46,end:35,pageup:33,page_up:33,pu:33,pagedown:34,page_down:34,pd:34,left:37,up:38,right:39,down:40,f1:112,f2:113,f3:114,f4:115,f5:116,f6:117,f7:118,f8:119,f9:120,f10:121,f11:122,f12:123},j=!1,l=!1,m=!1,n=!1,o=!1,p=!1,q=!1,r=!1;d.ctrlKey&&(n=!0),d.shiftKey&&(l=!0),d.altKey&&(p=!0),d.metaKey&&(r=!0);for(var s=0;k=f[s],s<f.length;s++)"ctrl"==k||"control"==k?(g++,m=!0):"shift"==k?(g++,j=!0):"alt"==k?(g++,o=!0):"meta"==k?(g++,q=!0):1<k.length?i[k]==code&&g++:c.keycode?c.keycode==code&&g++:e==k?g++:h[e]&&d.shiftKey&&(e=h[e],e==k&&g++);if(g==f.length&&n==m&&l==j&&p==o&&r==q&&(b(d),!c.propagate))return d.cancelBubble=!0,d.returnValue=!1,d.stopPropagation&&(d.stopPropagation(),d.preventDefault()),!1},this.all_shortcuts[a]={callback:e,target:d,event:c.type},d.addEventListener?d.addEventListener(c.type,e,!1):d.attachEvent?d.attachEvent("on"+c.type,e):d["on"+c.type]=e},remove:function(a){var a=a.toLowerCase(),b=this.all_shortcuts[a];delete this.all_shortcuts[a];if(b){var a=b.event,c=b.target,b=b.callback;c.detachEvent?c.detachEvent("on"+a,b):c.removeEventListener?c.removeEventListener(a,b,!1):c["on"+a]=!1}}},shortcut.add("Ctrl+U",function(){alert('Jangan Tekan Ctrl+U :)');});

function disableSelection(e){if(typeof e.onselectstart!="undefined")e.onselectstart=function(){return false};else if(typeof e.style.MozUserSelect!="undefined")e.style.MozUserSelect="none";else e.onmousedown=function(){return false};e.style.cursor="default"}window.onload=function(){disableSelection(document.body)}

</script>